cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

init_project ()

project (
	ITAConvolution
	VERSION 2024.0
	LANGUAGES CXX C
)

# ---Options---
option (ITA_CONVOLUTION_UPCONV_WITH_POWER_SAVER
		"Use power saving mode for uniform partitioned convolution (do not convolve blocks with zeros)" ON
)
option (ITA_CONVOLUTION_WITH_NUPCONV "Build with non-uniform partitioned convolution engine" OFF)
option (ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER
		"Include performance logger for non-uniform partitioned convolution" OFF
)
option (ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
		"Include XML read/write file format for non-uniform partitioned convolution filter segmentation" OFF
)
option (ITA_CONVOLUTION_WITH_BENCHMARKS "Build the benchmarks for the library" OFF)
option (ITA_CONVOLUTION_WITH_PROFILERS "Build the profiling application for the library" OFF)
option (ITA_CONVOLUTION_WITH_APPS "Build the applications of the library" OFF)
option (ITA_CONVOLUTION_WITH_TESTS "Build the tests for the library" OFF)

if (ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER OR ITA_CONVOLUTION_NUPCONV_WITH_XML_READER)
	message (FATAL_ERROR "Performance logger is deprecated, as the XML library cannot be found.")
endif ()
# ---END: Options---

# External libs
add_subdirectory (external_libs)

# Library
ihta_add_library (
	NAME ${PROJECT_NAME}
	SOURCES include/ITAUPConvolution.h
			include/ITAUPFilter.h
			include/ITAUPFilterPool.h
			include/ITAUPTrigger.h
			include/ITADirectConvolution.h
			include/ITAConvolutionDefinitions.h
			src/ITAUPConvolution.cpp
			src/ITAUPFilter.cpp
			src/ITAUPFilterPool.cpp
			src/ITADirectConvolution.cpp
			src/ITADirectConvolutionImpl.cpp
			src/ITADirectConvolutionImpl.h
	NAMESPACE ${PROJECT_NAME}
	INSTALL_INCLUDE_DIR ITAConvolution
	IDE_FOLDER "ITACoreLibs"
	OUT_LIB LIB_TARGET
)

# Additional source files
if (ITA_CONVOLUTION_WITH_NUPCONV)
	target_sources (
		${LIB_TARGET}
		PRIVATE include/ITANUPConvolution.h
				include/ITANUPFilter.h
				src/ITADirectConvolution.cpp
				src/ITADirectConvolutionImpl.cpp
				src/ITADirectConvolutionImpl.h
				src/ITANUPCEventLog.cpp
				src/ITANUPCEventLog.h
				src/ITANUPCFade.cpp
				src/ITANUPCFade.h
				src/ITANUPCFilterSegmentation.cpp
				src/ITANUPCFilterSegmentation.h
				src/ITANUPCHelpers.cpp
				src/ITANUPCHelpers.h
				src/ITANUPCInputBuffer1.cpp
				src/ITANUPCInputBuffer1.h
				src/ITANUPCOutputBuffer.cpp
				src/ITANUPCOutputBuffer.h
				src/ITANUPCPerformanceProfile.cpp
				src/ITANUPCPerformanceProfile.h
				src/ITANUPCStage.cpp
				src/ITANUPCStage.h
				src/ITANUPCStageInfo.h
				src/ITANUPCStageStatistic.h
				src/ITANUPCTask.cpp
				src/ITANUPCTask.h
				src/ITANUPCTaskQueue.cpp
				src/ITANUPCTaskQueue.h
				src/ITANUPCUFilter.cpp
				src/ITANUPCUFilter.h
				src/ITANUPCUFilterPool.cpp
				src/ITANUPCUFilterPool.h
				src/ITANUPCUtils.cpp
				src/ITANUPCUtils.h
				src/ITANUPConvolution.cpp
				src/ITANUPConvolutionImpl.cpp
				src/ITANUPConvolutionImpl.h
				src/ITANUPFilterComponent.h
				src/ITANUPFilterImpl.h
				src/ITANUPartitioningScheme.cpp
				src/ITANUPartitioningScheme.h
				src/ITAUPConvolution.cpp
				src/ITAUPFilter.cpp
				src/ITAUPFilterPool.cpp
	)

	if (ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER)
		target_compile_definitions (${LIB_TARGET} PRIVATE ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER)
	endif ()

	if (ITA_CONVOLUTION_NUPCONV_WITH_XML_READER)
		target_compile_definitions (${LIB_TARGET} PRIVATE ITA_CONVOLUTION_NUPCONV_WITH_XML_READER)
	endif ()
endif ()

# Linking
target_link_libraries (${LIB_TARGET} PUBLIC ITABase::ITABase ITAFFT::ITAFFT TBB::tbb)

# Definitions for Shared/Static
target_compile_definitions (
	${LIB_TARGET} PUBLIC $<IF:$<BOOL:${BUILD_SHARED_LIBS}>,ITA_CONVOLUTION_EXPORT,ITA_CONVOLUTION_STATIC>
)

# Install & export
packageProject (
	NAME ${PROJECT_NAME}
	VERSION ${PROJECT_VERSION}
	BINARY_DIR ${PROJECT_BINARY_DIR}
	INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include
	INCLUDE_DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/ITAConvolution
	DEPENDENCIES "ITABase;ITAFFT;TBB"
	COMPATIBILITY ExactVersion
	NAMESPACE ${PROJECT_NAME}
	DISABLE_VERSION_SUFFIX YES
)

# Benchmarks
if (ITA_CONVOLUTION_WITH_BENCHMARKS)
	add_subdirectory (benchmarks)
endif ()

# Profiler
if (ITA_CONVOLUTION_WITH_PROFILERS
	AND ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER
	AND ITA_CONVOLUTION_WITH_NUPCONV
)
	add_subdirectory (profilers)
endif ()

# Apps
if (ITA_CONVOLUTION_WITH_APPS)
	add_subdirectory (apps)
endif ()

# Tests
if (ITA_CONVOLUTION_WITH_TESTS)
	add_subdirectory (tests)
endif ()
