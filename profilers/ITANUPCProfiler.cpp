#include <conio.h>
#include <stdio.h>

#define USE_FFTW3
//#define USE_MKL7_FFT

// ITAToolkit includes
#include <ITAFastMath.h>
#include <ITAHPT.h>
#include <ITAStopWatch.h>

// STL includes
#include "../benchmarks/NUPCFilterComponentBenchmark.h"
#include "../src/ITANUPCPerformanceProfile.cpp"

#include <iostream>

#ifdef USE_FFTW3
#	include <fftw3.h>
#endif

#ifdef USE_MKL7_FFT
#	include <MKL_FFT.h>
#endif

using namespace std;

void print_header( const char* pszFunc )
{
	printf( "%s\n\n", pszFunc );
	printf( "%-8s  %+12s  %+10s  %+10s  %+8s\n", "Size", "Time", "CPU-Cycl.", "Time/Value", "Cycl./Value" );
	printf( "-----------------------------------------------------------\n" );
}

void print_result( unsigned int s, double t )
{
	char size[255], time[255], cyc[255], vtime[255], vcyc[255];
	sprintf( size, "%d", s );
	sprintf( time, "%0.3f us", t * 1000000 );
	sprintf( cyc, "%0.1f", t / ITAHPT_resolution( ) );
	sprintf( vtime, "%0.3f ns", t / s * 1000000000 );
	sprintf( vcyc, "%0.1f", t / s / ITAHPT_resolution( ) );

	printf( "%-8s  %+12s  %+10s  %+10s  %+8s\n", size, time, cyc, vtime, vcyc );
}

void print_footer( )
{
	printf( "-----------------------------------------------------------\n\n" );
}

unsigned int minExp, maxExp;
unsigned int minSize, maxSize;
PerformanceProfile* profile;

void fastmath( )
{
	ITAStopWatch sw;

	const int cycles = 100;

	fm_init( );
	printf( "FastMath flags: %s\n", fm_flags_str( ).c_str( ) );

	float* a = fm_falloc( maxSize );
	float* b = fm_falloc( maxSize );
	float* c = fm_falloc( maxSize );

	print_header( "fm_add" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		unsigned int size = 1 << x;

		// Trockene Messung
		sw.start( );
		fm_add( a, b, size );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			fm_add( a, b, size );
			sw.stop( );
		}

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::VADD, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	print_header( "fm_mul" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		unsigned int size = 1 << x;

		// Trockene Messung
		sw.start( );
		fm_mul( a, 1.234F, size );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			fm_mul( a, 1.234F, size );
			sw.stop( );
		}

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::SMUL, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	print_header( "fm_cmulx (Size = Anzahl Flie�kommazahlen = Anzahl komplexe Zahlen * 2!)" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		unsigned int size = 1 << x;

		// Trockene Messung
		sw.start( );
		fm_cmul_x( a, b, c, size / 2 );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			fm_cmul_x( a, b, c, size / 2 );
			sw.stop( );
		}

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::CMUL, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	fm_free( a );
	fm_free( b );
	fm_free( c );
}

#ifdef USE_FFTW3
void fftw3( )
{
	ITAStopWatch sw;

	const int cycles = 100;

	fm_init( );
	float* a = fm_falloc( maxSize );
	float* b = fm_falloc( 2 * maxSize + 2 );

	print_header( "fftw3: dft_r2c_1d, FFTW_MEASURE" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		unsigned int size = 1 << x;

		// fftw-Plan erzeugen
		fftwf_plan p = fftwf_plan_dft_r2c_1d( size, a, (fftwf_complex*)b, FFTW_MEASURE );

		// Trockene Messung
		sw.start( );
		fftwf_execute( p );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			fftwf_execute( p );
			sw.stop( );
		}

		fftwf_destroy_plan( p );

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::FFT, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	print_header( "fftw3: dft_c2r_1d, FFTW_MEASURE" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		unsigned int size = 1 << x;

		// fftw-Plan erzeugen
		fftwf_plan p = fftwf_plan_dft_c2r_1d( size, (fftwf_complex*)b, a, FFTW_MEASURE );

		// Trockene Messung
		sw.start( );
		fftwf_execute( p );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			fftwf_execute( p );
			sw.stop( );
		}

		fftwf_destroy_plan( p );

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::IFFT, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	fm_free( a );
	fm_free( b );
}

// void fftw3_c2c() {
//	ITAStopWatch sw;
//
//	const int cycles = 100;
//
//	fm_init();
//	float* a = fm_falloc(2*maxSize);
//	float* b = fm_falloc(2*maxSize);
//
//	print_header("fftw3: dft_c2c_1d, FFTW_MEASURE");
//	for (unsigned int x=minExp; x<=maxExp; x++) {
//		unsigned int size = 1<<x;
//
//		// fftw-Plan erzeugen
//		fftwf_plan p = fftwf_plan_dft_1d(size, (fftwf_complex*) a, (fftwf_complex*) b, FFTW_FORWARD, FFTW_MEASURE);
//
//		// Trockene Messung
//		sw.start();
//		fftwf_execute(p);
//		sw.stop();
//		sw.reset();
//
//		for (int i=0; i<cycles; i++) {
//			sw.start();
//			fftwf_execute(p);
//			sw.stop();
//		}
//
//		fftwf_destroy_plan(p);
//
//		double t = sw.mean();
//		if (profile) profile->getOMT()->setRuntime(OperationMeasurementTable::FFT, x, (float) t);
//		print_result(size, t);
//	}
//	print_footer();
//
//	print_header("fftw3: dft_c2c_1d, FFTW_MEASURE");
//	for (unsigned int x=minExp; x<=maxExp; x++) {
//		unsigned int size = 1<<x;
//
//		// fftw-Plan erzeugen
//		fftwf_plan p = fftwf_plan_dft_1d(size, (fftwf_complex*) a, (fftwf_complex*) b, FFTW_BACKWARD, FFTW_MEASURE);
//
//		// Trockene Messung
//		sw.start();
//		fftwf_execute(p);
//		sw.stop();
//		sw.reset();
//
//		for (int i=0; i<cycles; i++) {
//			sw.start();
//			fftwf_execute(p);
//			sw.stop();
//		}
//
//		fftwf_destroy_plan(p);
//
//		double t = sw.mean();
//		if (profile) profile->getOMT()->setRuntime(OperationMeasurementTable::IFFT, x, (float) t);
//		print_result(size, t);
//	}
//	print_footer();
//
//	fm_free(a);
//	fm_free(b);
//}
#endif

// ------------------------------------------------------------------------

#ifdef USE_MKL7_FFT

void MKL7_FFT( )
{
	ITAStopWatch sw;

	const int cycles = 100;

	fm_init( );
	float* a = fm_falloc( 2 * maxSize + 4 );
	float* b = fm_falloc( 2 * maxSize + 4 );

	print_header( "MKL7: scfft1dc(a, size, 1, b)" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		int size = 1 << x;

		// Planen
		scfft1dc( a, size, 0, b );

		// Trockene Messung
		sw.start( );
		scfft1dc( a, size, 1, b );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			scfft1dc( a, size, 1, b );
			sw.stop( );
		}

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::FFT, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	print_header( "MKL7: scfft1dc(a, size, 1, b)" );
	for( unsigned int x = minExp; x <= maxExp; x++ )
	{
		int size = 1 << x;

		// fftw-Plan erzeugen
		csfft1dc( a, size, 0, b );

		// Trockene Messung
		sw.start( );
		csfft1dc( a, size, 1, b );
		sw.stop( );
		sw.reset( );

		for( int i = 0; i < cycles; i++ )
		{
			sw.start( );
			csfft1dc( a, size, 1, b );
			sw.stop( );
		}

		double t = sw.mean( );
		if( profile )
			profile->getOMT( )->setRuntime( OperationMeasurementTable::IFFT, x, (float)t );
		print_result( size, t );
	}
	print_footer( );

	fm_free( a );
	fm_free( b );
}

#endif

// ------------------------------------------------------------------------

/* To reactivate...
void stage() {
std::vector<unsigned int> v;
for (unsigned int bs=128; bs<=65536; bs<<=1) {
v.clear();
for (unsigned int m=0; m<1; m++) {
// Zerlegungsschema erzeugen
v.push_back(bs);
PartitionScheme P(v);

// Stufe erzeugen
Stage S(&P, 0);
}
}
}
*/

int main( int argc, char* argv[] )
{
	// benchmarkFilterComponentCreation();
	// benchmarkFilterComponentLoad();
	// benchmarkFilterComponentSegLoad();

	minExp  = 6;
	maxExp  = 16;
	minSize = 1 << minExp;
	maxSize = 1 << maxExp;

	if( SetPriorityClass( GetCurrentProcess( ), REALTIME_PRIORITY_CLASS ) == 0 )
		fprintf( stderr, "Warnung: Setzen der Prozess-Priorit�tsklasse schlug fehl!\n" );
	if( SetThreadPriority( GetCurrentThread( ), THREAD_PRIORITY_TIME_CRITICAL ) == 0 )
		fprintf( stderr, "Warnung: Setzen der Thread-Priorit�t schlug fehl!\n" );

	ITAHPT_init( );

	printf( "Selftest:  %0.3f ns\n", ITAStopWatch::selftest( ) * 1000000000 );

	profile = new PerformanceProfile( );
	profile->createOMT( minExp, maxExp );

	fastmath( );

#ifdef USE_FFTW3
	fftw3( );
	// fftw3_c2c();
#endif

#ifdef USE_MKL7_FFT
	MKL7_FFT( );
#endif

	// stage();

	profile->store( "out.pp" );
	cout << profile->toString( );

	delete profile;

	// getch();

	return 0;
}