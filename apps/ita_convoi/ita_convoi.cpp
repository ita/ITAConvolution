/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */
#include <ITAAudiofileReader.h>
#include <ITADataSourceRealization.h>
#include <ITAException.h>
#include <ITAFileDataSource.h>
#include <ITAPortaudioInterface.h>
#include <ITASampleFrame.h>
#include <ITAStreamMultiplier1N.h>
#include <ITAStreamPatchBay.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

#ifdef WIN32
#	include <conio.h>
#else
#	include <ncurses.h>
#endif

#include <VistaTools/VistaFileSystemFile.h>


class ITAStreamConvolver
    : public ITADatasourceRealizationEventHandler
    , public ITAUPConvolution
{
public:
	inline ITAStreamConvolver( const double dSampleRate, const int iBlockLength, const int iFilterLength )
	    : ITAUPConvolution( iBlockLength, iFilterLength )
	    , pdsInput( NULL )
	{
		m_pdsOutput = new ITADatasourceRealization( 1, dSampleRate, iBlockLength );
		m_pdsOutput->SetStreamEventHandler( this );
	};

	inline void HandleProcessStream( ITADatasourceRealization*, const ITAStreamInfo* pStreamInfo )
	{
		if( !pdsInput )
			ITA_EXCEPT1( MODAL_EXCEPTION, "Input data source not defined yet" );

		const float* pfInputData = pdsInput->GetBlockPointer( 0, pStreamInfo );
		Process( pfInputData, GetBlocklength( ), m_pdsOutput->GetWritePointer( 0 ), GetBlocklength( ), ITABase::MixingMethod::OVERWRITE );
		m_pdsOutput->IncrementWritePointer( );
	};

	void HandlePostIncrementBlockPointer( ITADatasourceRealization* ) { pdsInput->IncrementBlockPointer( ); };

	ITADatasource* GetOutputDataSource( ) { return m_pdsOutput; };

	ITADatasource* pdsInput;

private:
	ITADatasourceRealization* m_pdsOutput;
};

std::string ita_convoi_syntax( );
std::string ita_convoi_commands( );
void ita_convio_exchange_channel( ITAStreamConvolver*, const ITASampleFrame&, const int iChannelIndex );
void ita_convio_dirac_channel( ITAStreamConvolver* );

int main( int argc, char* argv[] )
{
	if( argc < 3 )
	{
		std::cout << ita_convoi_syntax( ) << std::endl;
		return 255;
	}

	// Input wav file
	std::string sInputFilePath = std::string( argv[1] );
	VistaFileSystemFile oInputFile( sInputFilePath );
	if( !oInputFile.Exists( ) || !oInputFile.IsFile( ) )
	{
		std::cerr << "Input file '" << oInputFile.GetName( ) << "' not found or invalid" << std::endl;
		return 255;
	}

	ITAAudiofileReader* pSample = NULL;
	try
	{
		pSample = ITAAudiofileReader::create( oInputFile.GetName( ) );
	}
	catch( ITAException& e )
	{
		std::cerr << "Could not read input file: " << e << std::endl;
		return 255;
	}

	double dSamplingRate = pSample->getSamplerate( );
	delete pSample;

	// IR filter file
	std::string sIRFilePath = std::string( argv[2] );
	VistaFileSystemFile oIRFile( sIRFilePath );
	if( !oIRFile.Exists( ) || !oIRFile.IsFile( ) )
	{
		std::cerr << "IR filter file '" << oIRFile.GetName( ) << "' not found or invalid" << std::endl;
		return 255;
	}

	ITAAudiofileReader* pIRAudioFile = NULL;
	try
	{
		pIRAudioFile = ITAAudiofileReader::create( oIRFile.GetName( ) );
	}
	catch( ITAException& e )
	{
		std::cerr << "Could not read IR filter file: " << e << std::endl;
		return 255;
	}
	double dSamplingRateFilter = pIRAudioFile->getSamplerate( );
	int iFilterChannels        = pIRAudioFile->getNumberOfChannels( );
	int iFilterLength          = pIRAudioFile->getLength( );
	delete pIRAudioFile;

	// Require matching sampling rates
	if( dSamplingRate != dSamplingRateFilter )
	{
		std::cerr << "Sampling rate of input file (" << dSamplingRate << ") and IR filter (" << dSamplingRateFilter << ") does not match" << std::endl;
		return 255;
	}

	int iBlockLength = ITAPortaudioInterface::GetPreferredBufferSize( );
	if( argc > 3 )
	{
		iBlockLength = atoi( argv[3] );
	}

	// Start audio streaming
	ITAPortaudioInterface::ITA_PA_ERRORCODE iError;
	ITAPortaudioInterface oITAPA( dSamplingRate, iBlockLength );
	int iPortaudioDeviceID = oITAPA.GetDefaultOutputDevice( );
	if( argc > 4 )
		iPortaudioDeviceID = atoi( argv[4] );

	std::cout << "Attempting to initialize Portaudio device with ID " << iPortaudioDeviceID << std::endl;
	if( ( iError = oITAPA.Initialize( iPortaudioDeviceID ) ) != ITAPortaudioInterface::ITA_PA_NO_ERROR )
	{
		std::cerr << "Could not initialize Portaudio, encountered error: " << ITAPortaudioInterface::GetErrorCodeString( iError ) << std::endl;
		return 255;
	}

	std::cout << "Starting audio streaming on device '" << oITAPA.GetDeviceName( iPortaudioDeviceID ) << "'"
	          << " with " << dSamplingRate / 1.0e3 << " kHz and a block size of " << iBlockLength << " samples on " << oITAPA.GetNumOutputChannels( iPortaudioDeviceID )
	          << " output channels." << std::endl;
	std::cout << "Your FIR filter has " << iFilterChannels << " channel" << ( iFilterChannels > 1 ? "s" : "" ) << " and a length of " << iFilterLength
	          << " taps / samples"
	          << " (" << double( iFilterLength ) / dSamplingRate << " seconds)." << std::endl;

	// Streaming chaing
	ITAStreamConvolver oConvolver( dSamplingRate, iBlockLength, iFilterLength );

	ITAFileDatasource oSample( oInputFile.GetName( ), iBlockLength, true );
	if( oSample.GetNumberOfChannels( ) != 1 )
	{
		std::cout << "Warning ... input file has multiple channels, mixing to mono." << std::endl;
	}

	ITAStreamPatchbay oPatchBay( dSamplingRate, iBlockLength );
	int iInputID        = oPatchBay.AddInput( &oSample );
	int iOutputID       = oPatchBay.AddOutput( 1 );
	oConvolver.pdsInput = oPatchBay.GetOutputDatasource( iOutputID );

	for( int n = 0; n < int( oSample.GetNumberOfChannels( ) ); n++ ) // Downmix
		oPatchBay.ConnectChannels( iInputID, n, iOutputID, 0 );

	ITAStreamMultiplier1N oMultiplier( oConvolver.GetOutputDataSource( ), oITAPA.GetNumOutputChannels( iPortaudioDeviceID ) );

	if( ( iError = oITAPA.SetPlaybackDatasource( &oMultiplier ) ) != ITAPortaudioInterface::ITA_PA_NO_ERROR )
	{
		std::cerr << "Could not set streaming playback for Portaudio, encountered error: " << ITAPortaudioInterface::GetErrorCodeString( iError ) << std::endl;
		return 255;
	}

	if( ( iError = oITAPA.Open( ) ) != ITAPortaudioInterface::ITA_PA_NO_ERROR )
	{
		std::cerr << "Could not open streaming over Portaudio, encountered error: " << ITAPortaudioInterface::GetErrorCodeString( iError ) << std::endl;
		return 255;
	}

	if( ( iError = oITAPA.Start( ) ) != ITAPortaudioInterface::ITA_PA_NO_ERROR )
	{
		std::cerr << "Could not start streaming, encountered error: " << ITAPortaudioInterface::GetErrorCodeString( iError ) << std::endl;
		return 255;
	}


	int iCurrentIRChannelIndex = 0;
	ITASampleFrame sfIR( oIRFile.GetName( ) );
	ita_convio_exchange_channel( &oConvolver, sfIR, iCurrentIRChannelIndex );

	// Start user interaction
	std::cout << ita_convoi_commands( ) << std::endl;
	int iKey;
	while( ( iKey = _getch( ) ) != 'q' )
	{
		switch( iKey )
		{
			case( 'm' ):
			{
				// Toggle
				oMultiplier.SetMuted( !oMultiplier.IsMuted( ) );
				if( oMultiplier.IsMuted( ) )
					std::cout << "Output is now muted" << std::endl;
				else
					std::cout << "Output is loud" << std::endl;
				break;
			}
			case( 'n' ):
			{
				// Switch to next (or only) channel
				iCurrentIRChannelIndex = ( iCurrentIRChannelIndex + 1 ) % iFilterChannels;
				ita_convio_exchange_channel( &oConvolver, sfIR, iCurrentIRChannelIndex );
				break;
			}
			case( 'd' ):
			{
				ita_convio_dirac_channel( &oConvolver );
				break;
			}
			case( 48 + 1 ):
			case( 48 + 2 ):
			case( 48 + 3 ):
			case( 48 + 4 ):
			case( 48 + 5 ):
			case( 48 + 6 ):
			case( 48 + 7 ):
			case( 48 + 8 ):
			case( 48 + 9 ):
			{
				// Switch to next channel
				int iRequestedChannelIndex = iKey - 1 - 48;
				if( iFilterChannels > iRequestedChannelIndex )
				{
					iCurrentIRChannelIndex = iRequestedChannelIndex;
					ita_convio_exchange_channel( &oConvolver, sfIR, iCurrentIRChannelIndex );
				}
				else
				{
					std::cerr << "Requested channel is out of range, maximum IR channel number is " << iFilterChannels << std::endl;
				}
				break;
			}
			default:
			{
				std::cerr << "Unrecognized key command '" << char( iKey ) << "'" << std::endl;
				std::cout << ita_convoi_commands( ) << std::endl;
				break;
			}
		}
	}

	oITAPA.Stop( );
	oITAPA.Close( );
	oITAPA.Finalize( );

	return 0;
}

std::string ita_convoi_syntax( )
{
	std::stringstream ss;
	ss << "Syntax: ita_convoi SAMPLE_INPUT_WAV_MONO IR_FILTER_WAV_MULTICHANNEL [BLOCKLENGTH] [PORTAUDIO_DEVICE]" << std::endl;
	return ss.str( );
}

std::string ita_convoi_commands( )
{
	std::stringstream ss;
	ss << "Commands:\t'q' quit" << std::endl;
	ss << "\t\t'm' toggle mute" << std::endl;
	ss << "\t\t'n' switch to next IR channel" << std::endl;
	ss << "\t\t'd' exchange a Dirac filter" << std::endl;
	ss << "\t\t1-9 exchange IR filter to given channel, if available" << std::endl;
	return ss.str( );
}

void ita_convio_exchange_channel( ITAStreamConvolver* pSC, const ITASampleFrame& sfIR, const int iChannelIndex )
{
	assert( sfIR.channels( ) > iChannelIndex );
	ITAUPFilter* pFilter = pSC->RequestFilter( );
	pFilter->Load( sfIR[iChannelIndex].GetData( ), sfIR.GetLength( ) );
	pSC->ExchangeFilter( pFilter );
	pSC->ReleaseFilter( pFilter );
	std::cout << "Exchanged filter, now streaming channel " << iChannelIndex + 1 << std::endl;
}


void ita_convio_dirac_channel( ITAStreamConvolver* pSC )
{
	ITAUPFilter* pFilter = pSC->RequestFilter( );
	pFilter->identity( );
	pSC->ExchangeFilter( pFilter );
	pSC->ReleaseFilter( pFilter );
	std::cout << "Exchanged Dirac filter" << std::endl;
}
