## ita_convoi

ita_convoi is an exemplary command line application that uses a streaming block convolution from ITAConvolution.
It basically takes an input WAV file and an input IR file as a filter and convolves these two signals as a running conolution
using overlapp-save method. For playback, Portaudio is used either over requested interface number or using default output.
Input should be mono, IR filter can be of arbitrary channels, but sampling rates have to match.
Switching between IR filter channels can be triggered by command keys, and also a Dirac switch is available.

### License

See [LICENSE](LICENSE.md) file.

