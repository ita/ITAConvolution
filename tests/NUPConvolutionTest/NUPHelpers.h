#ifndef IW_ITANUP_TES_HELPERS
#define IW_ITANUP_TES_HELPERS

#include <vector>

// Ein float-Array aus einer Audiodatei laden
// (Erzeugt Array und setzt Parameter via Call-by-Reference)
float* load( char* pszFilename, float*& pfData, unsigned int& uiLength );

// Ein float-Array in eine Audiodatei schreiben
float* store( char* pszFilename, float* pfData, unsigned int uiLength );

// Wertes eines (reellen) float Arrays ausgeben
void show( float* in, unsigned int n );

// Wertes eines Arrays von komplexen Zahlen ausgeben
// (Koeffizienten getrennt nach Real- und Imagin�rteil)
void show( float* ri, float* ii, unsigned int n );

// Arrays komplexer Zahlen multiplizieren
inline void cmulf( float* dest, const float* src, unsigned int n );

/*
    Hilfsfunktionen f�r Datei Ein- und Ausgabe
    */

// Einen unsigned int als unsigned char in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_uchar( FILE* file, unsigned int i );

// Einen unsigned int als unsigned short in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_ushort( FILE* file, unsigned int i );

// Einen unsigned int in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_uint( FILE* file, unsigned int i );

// Einen float in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_float( FILE* file, float x );

// Einen Null-terminierten String in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_sz( FILE* file, char* s );

// Einen std::string als Null-terminierten String in einen Stream schreiben
// (R�ckgabewert: 0 -> kein Fehler, 1 sonst)
int fwrite_sz( FILE* file, std::string s );

// Felder eines float-Arrays auf der Konsole ausgeben
void TRACE_BUFFER( char* name, float* src, unsigned int count, unsigned int digits = 3 );

// Felder eines double-Arrays auf der Konsole ausgeben
void TRACE_BUFFER( char* name, double* src, unsigned int count, unsigned int digits = 3 );

// Felder eines float-Arrays von komplexen Zahlen auf der Konsole ausgeben (dim = Anzahl der komplexen Koeff.)
void TRACE_BUFFER_COMPLEX( char* name, float* src, unsigned int dim, unsigned int digits = 3 );


#endif // IW_ITANUP_TES_HELPERS