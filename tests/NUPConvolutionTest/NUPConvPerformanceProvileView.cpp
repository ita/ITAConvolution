#include "../../src/NUPCPerformanceProfile.h"

#include <ITAException.h>
#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
	if( argc != 2 )
	{
		cerr << "Syntax: PPView DATEINAME" << endl;
		return 255;
	}

	try
	{
		PerformanceProfile PP( argv[1] );
		cout << endl << PP.toString( ) << endl;
	}
	catch( ITAException& e )
	{
		cerr << "Fehler: " << e.toString( ) << endl;
		return 255;
	}

	return 0;
}
