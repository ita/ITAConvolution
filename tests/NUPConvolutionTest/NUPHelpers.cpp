#include "Helpers.h"

#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>

float* load( char* pszFilename, float*& pfData, unsigned int& uiLength )
{
	pfData   = 0;
	uiLength = 0;

	ITAAudiofileProperties props;
	sneakAudiofile( pszFilename, props );
	if( ( props.iLength == 0 ) || ( props.iChannels != 1 ) || ( props.eDomain != ITADomain::ITA_TIME_DOMAIN ) )
		return 0;

	// Hinweis: Wegen der Abfrage (s.o.) k�nnen wir sicher sein, das
	//          vpfData nur ein Element enth�lt.
	std::vector<float*> vpfData = readAudiofile( pszFilename, props );
	uiLength                    = props.iLength;
	pfData                      = vpfData.front( );
	return pfData;
}

// Ein float-Array in eine Audiodatei schreiben
float* store( char* pszFilename, float* pfData, unsigned int uiLength )
{
	ITAAudiofileProperties props;
	props.iLength       = uiLength;
	props.iChannels     = 1;
	props.dSampleRate   = 44100;
	props.eDomain       = ITADomain::ITA_TIME_DOMAIN;
	props.eQuantization = ITAQuantization::ITA_INT16;
	std::vector<float*> vpfData;
	vpfData.push_back( pfData );
	writeAudiofile( pszFilename, props, vpfData );
	return pfData;
}

void show( float* in, unsigned int n )
{
	for( unsigned int i = 0; i < n; i++ )
		printf( "%0.6f\n", in[i] );
}

void show( float* ri, float* ii, unsigned int n )
{
	for( unsigned int i = 0; i < n; i++ )
		printf( "%0.6f + %0.6fi\n", ri[i], ii[i] );
}

inline void cmulf( float* dest, const float* src, unsigned int n )
{
	float re;
	for( unsigned int i = 0; i < n; i++ )
	{
		// re1 = re1*re2 - im1*im2
		// im1 = re1*im2 + re2*im1

		// Aktuellen Realteil in dest sichern
		re                   = dest[( i << 1 )];
		dest[( i << 1 )]     = dest[( i << 1 )] * src[( i << 1 )] - dest[( i << 1 ) + 1] * src[( i << 1 ) + 1];
		dest[( i << 1 ) + 1] = re * src[( i << 1 ) + 1] + dest[( i << 1 ) + 1] * src[( i << 1 )];
	}
}

int fwrite_uchar( FILE* file, unsigned int i )
{
	unsigned char j = (unsigned char)i;
	return ( fwrite( &j, 1, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_ushort( FILE* file, unsigned int i )
{
	unsigned short j = (unsigned short)i;
	return ( fwrite( &j, 2, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_uint( FILE* file, unsigned int i )
{
	return ( fwrite( &i, 4, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_float( FILE* file, float x )
{
	return ( fwrite( &x, 4, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_sz( FILE* file, char* s )
{
	return ( fwrite( s, strlen( s ) + 1, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_sz( FILE* file, std::string s )
{
	return ( fwrite( s.c_str( ), s.length( ) + 1, 1, file ) == 1 ) ? 0 : 1;
}

void TRACE_BUFFER( char* name, float* src, unsigned int count, unsigned int digits )
{
	char mask[32];
	/*	Ausgabe in einer Zeile
	    sprintf(mask, "%%s%%0.%df", digits);
	    printf("%s = [", name);
	    for (unsigned int i=0; i<count; i++)
	    printf(mask, (i>0 ? ", " : ""), src[i]);
	    printf("]\n");
	    */
	sprintf( mask, "%%0.%df\n", digits );
	printf( "%s:\n\n", name );
	for( unsigned int i = 0; i < count; i++ )
		printf( mask, src[i] );
	printf( "\n" );
}

void TRACE_BUFFER( char* name, double* src, unsigned int count, unsigned int digits )
{
	char mask[32];
	sprintf( mask, "%%0.%df\n", digits );
	printf( "%s:\n\n", name );
	for( unsigned int i = 0; i < count; i++ )
		printf( mask, src[i] );
	printf( "\n" );
}

void TRACE_BUFFER_COMPLEX( char* name, float* src, unsigned int dim, unsigned int digits )
{
	char pmask[32];
	char nmask[32];
	sprintf( pmask, "%%0.%df + %%0.%dfi\n", digits, digits );
	sprintf( nmask, "%%0.%df - %%0.%dfi\n", digits, digits );
	printf( "%s:\n\n", name );
	for( unsigned int i = 0; i < dim; i++ )
		if( src[2 * i + 1] >= 0 )
			printf( pmask, src[2 * i], src[2 * i + 1] );
		else
			printf( nmask, src[2 * i], -src[2 * i + 1] );
	printf( "\n" );
}