#include "../../src/ITANUPCFilterSegmentation.cpp"
#include "../../src/ITANUPCFilterSegmentation.h"

#include <ITAException.h>
#include <stdio.h>

using namespace std;

int main( int argc, char** )
{
	try
	{
		// Creation of an FSC
		FSegCollection fsc;
		FILTERSEGMENTATION fs;
		fs.push_back( fseg_make( 128, 3 ) );
		fs.push_back( fseg_make( 256, 4 ) );
		fs.push_back( fseg_make( 1024, 5 ) );
		fs.push_back( fseg_make( 2048, 6 ) );
		fs.push_back( fseg_make( 16384, 7 ) );
		fsc.addSegmentation( fs, 44100 );

		fs.clear( );
		fs.push_back( fseg_make( 128, 2 ) );
		fs.push_back( fseg_make( 256, 1 ) );
		fs.push_back( fseg_make( 512, 11 ) );
		fsc.addSegmentation( fs, 48000 );

		printf( fsc.toString( ).c_str( ) );

		// Try to save it
		fsc.store( "NUPFSegTest_FilterSegmentation.fsc" );

		// Try to reload it
		FSegCollection fsc2( "test.fsc" );
		printf( fsc.toString( ).c_str( ) );
	}
	catch( ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}

	return 0;
}