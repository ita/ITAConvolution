#include "../../src/ITANUPCPerformanceProfile.h"

#include <ITAException.h>
#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
	if( argc != 2 )
	{
		cerr << "Syntax: PPTest DATEINAME" << endl;
		return 255;
	}

	try
	{
		PerformanceProfile PP( argv[1] );
		cout << endl << PP.toString( ) << endl;

		ITADynaBuffer db( 65536 );
		PP.write( db );

		db.seek( 0 );
		PerformanceProfile PP2( db );
		cout << endl << "Deserialized Profile:" << endl << endl << PP2.toString( ) << endl;
	}
	catch( ITAException& e )
	{
		cerr << "Fehler: " << e.ToString( ) << endl;
		return 255;
	}

	return 0;
}