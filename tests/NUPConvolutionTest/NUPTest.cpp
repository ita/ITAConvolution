#include "../../src/ITANUPCEventLog.cpp"
#include "../../src/ITANUPCFade.cpp"
#include "../../src/ITANUPCFilterSegmentation.cpp"
#include "../../src/ITANUPCInputBuffer1.cpp"
#include "../../src/ITANUPCOutputBuffer.cpp"
#include "../../src/ITANUPCStage.cpp"
#include "../../src/ITANUPCTask.cpp"
#include "../../src/ITANUPCTask.h"
#include "../../src/ITANUPCTaskQueue.cpp"
#include "../../src/ITANUPCUFilter.cpp"
#include "../../src/ITANUPCUFilterPool.cpp"
#include "../../src/ITANUPCUtils.cpp"
#include "../../src/ITANUPConvolutionImpl.cpp"
#include "../../src/ITANUPartitioningScheme.cpp"

#include <ITAException.h>
#include <cassert>
#include <cstdlib>
#include <iostream>

using namespace std;

void TestPartitions( )
{
	vector<int> v;
	v.push_back( 128 );
	v.push_back( 128 );
	v.push_back( 128 );
	v.push_back( 256 );
	v.push_back( 256 );
	v.push_back( 256 );
	v.push_back( 256 );
	v.push_back( 1024 );
	v.push_back( 1024 );
	v.push_back( 2048 );

	NUPartition p( v );
	cout << p.ToString( );
}

void testUFilter( )
{
	CUPartition p( 0, 4, 2 );

	CUFilter* f = new CUFilter( &p, nullptr );
	assert( f->IsInUse( ) == false );

	f->Load( nullptr, 0, nullptr, 0 );

	// float h1[ 8 ] = { 0, 0, 0, 1, 1, 0, 0, 0 };
	float h2[8] = { 0, 0, 0, 0, 1, 0, 0, 0 };
	// float h3[ 8 ] = { 0, 0, 0, 0, 0, 0, 0, 0 };

	f->Load( h2, 8, nullptr, 0 );

	delete f;
}

void TestUFilterPool( )
{
	CUPartition p( 0, 4, 2 );

	UFilterPool x( &p, 1 );
	CUFilter* f1 = x.RequestFilter( );
	CUFilter* f2 = x.RequestFilter( );
	assert( f1 && f2 );
}

void TestStage( )
{
	const int B         = 4;
	const int M         = 2;
	const float data0[] = { 1, 2, 3, 4 };
	const float data1[] = { 5, 6, 7, 8 };


	CUPartition p( 0, B, M );
	CStage* s = new CStage( &p );

	// Eingangspuffer
	CNUPCInputImpl* in = new CNUPCInputImpl( 0, B, B );


	UFilterPool x( &p, 1 );
	CUFilter* f1 = x.RequestFilter( );
	f1->Identity( );
	s->ExchangeFilter( f1 );

	// Runde 1
	memcpy( in->GetBuffer( ), data0, B * sizeof( float ) );
	in->load( );
	Task t1( in, 0, 0, s, 0, INFINITE_TIME );
	assert( f1->IsInUse( ) );
	s->Compute( &t1, false, INFINITE_TIME );
	assert( f1->IsInUse( ) );
	// f->zero();

	float* out0 = s->pfLeftOutput;
	float* out1 = s->pfRightOutput;

	// Runde 2

	CUFilter* f2 = x.RequestFilter( );
	float h[]    = { 0.5 };
	f2->Load( h, 1, h, 1 );
	s->ExchangeFilter( f2 );

	assert( f1->IsInUse( ) );

	memcpy( in->GetBuffer( ), data1, B * sizeof( float ) );
	in->load( );
	Task t2( in, 0, 0, s, 0, INFINITE_TIME );
	s->Compute( &t2, false, INFINITE_TIME );

	assert( f1->IsInUse( ) );

	out0 = s->pfLeftOutput;
	out1 = s->pfRightOutput;

	delete s;
}

void TestNUPC( )
{
	const int B = 4;
	const int L = 32;

	ITANUPC::CConvolutionParameter params( 44100, B, L );
	params.eSegmentationScheme = ITANUPC::SegmentationScene::GARDNER;
	params.bSynchronous        = true;

	ITANUPC::IConvolution* conv = ITANUPC::IConvolution::Create( params );

	ITANUPC::IInput* input = conv->AddInput( );
	std::vector<ITANUPC::IOutput*> outputs;
	conv->GetOutputs( outputs );

	/*
	const float s0[] = {1, 2, 3, 4};
	const float s1[] = {5, 6, 7, 8};
	const float s2[] = {0, 0, 0, 0};
	*/
	const float s[] = { 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };

	// const float h0[] = { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	const float h1[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0 };
	float g[]        = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


	ITANUPC::IFilter* filter0 = conv->RequestFilter( );
	// filter0->identity();
	filter0->Load( h1, 8, h1, 8 );
	conv->ExchangeFilter( input, filter0 );

	input->Put( s );
	conv->Process( );
	outputs[0]->Get( g );

	input->Put( s + B );
	conv->Process( );
	outputs[0]->Get( g + B );

	input->Put( s + 2 * B );
	conv->Process( );
	outputs[0]->Get( g + 2 * B );

	delete conv;
}

int main( int, char** )
{
	try
	{
		// TestPartitions();
		// TestUFilter();
		// TestUFilterPool();
		// TestStage();
		TestNUPC( );

		return 0;
	}
	catch( ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}
}