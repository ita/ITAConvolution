#include "NUPHelpers.h"

#include <ITAAudiofileReader.h>
#include <ITADataSourceUtils.h>
#include <ITAException.h>
#include <ITAFileDataSource.h>
#include <ITANUPConvolution.h>
#include <ITANumericUtils.h>
#include <stdio.h>
#include <vector>

using namespace std;

int main( int argc, char* argv[] )
{
	if( ( argc < 5 ) || ( argc > 6 ) )
	{
		printf( "Syntax:       NUPConvFileTest INFILE IRFILE OUTFILE BUFFERSIZE [OUTPUTGAIN]\n\n" );
		printf( "Beschreibung: Faltet INFILE mit IRFILE und speichert das Ergebnis in OUTFILE\n" );
		printf( "              (Dabei wird BUFFERSIZE als Puffergroesse verwendet,\n" );
		printf( "               OUTPUTGAIN (Optional) Ausgabeverstärkungsfaktor)\n" );
		return 255;
	}

	ITANUPC::IConvolution* pConv = NULL;
	std::string sInfile          = argv[1];
	std::string sIRFile          = argv[2];
	std::string sOutfile         = argv[3];
	unsigned int uiBufferSize    = atoi( argv[4] );
	double dGain                 = 1.0;

	if( argc == 6 )
		dGain = atof( argv[5] );

	// try {
	// Dateiquelle erzeugen
	ITAFileDatasource source( sInfile, uiBufferSize );
	unsigned int uiChannels = source.GetNumberOfChannels( );
	double dSamplerate      = source.GetSampleRate( );

	if( uiChannels != 1 )
	{
		fprintf( stderr, "Eingabedatei muß Mono sein!\n" );
		return 255;
	}

	// Impulsantwort laden
	unsigned int uiIRLength;
	ITAAudiofileProperties props;
	sneakAudiofile( sIRFile, props );
	if( ( props.iChannels != 1 ) && ( props.iChannels != 2 ) )
	{
		fprintf( stderr, "Impulsantwort muß Mono oder Stereo sein!\n" );
		return 255;
	}

	//! Audiodatei in Puffer laden (Puffer werden erzeugt)
	vector<float*> vpfData = readAudiofile( sIRFile, props );
	uiIRLength             = props.iLength;

	// Convolution creation
	ITANUPC::CConvolutionParameter oParams;
	oParams.dSamplerate  = dSamplerate;
	oParams.iBlocklength = uiBufferSize;
	oParams.iIRLength    = uiIRLength;
	pConv                = ITANUPC::IConvolution::create( oParams );
	unsigned int uiCCID  = pConv->requestConvolutionChannel( );

	/*
	        // Zwischentest: getSegment
	        for (unsigned int i=0; i< pConv->getMaxIRLength(); i++)
	        printf("Segment[%d] = %d\n", i, pConv->getSegment(i));
	        */

	// Create filter object
	ITANUPC::CFilterComponent* pFC = pConv->requestFilterComponent( );
	if( props.uiChannels == 1 )
		pFC->load( vpfData[0], uiIRLength, vpfData[0], uiIRLength );
	else
		pFC->load( vpfData[0], uiIRLength, vpfData[1], uiIRLength );

	pConv->setInputDatasource( uiCCID, &source );
	pConv->setFilterComponent( uiCCID, pFC );
	// pConv->setInputDatasource(2, &source);
	// pConv->setFilterComponent(2, pFC);
	// pConv->setOutputGain(0.3);

	// Anzahl der Durchläufe berechnen
	unsigned int l = uprmul( source.GetFileCapacity( ) + uiIRLength - 1, uiBufferSize );

	// Infos ausgeben
	printf( "\nEingabedatei:    \"%s\" (%d Kanaele, %0.3f kHz)\n", sInfile.c_str( ), uiChannels, dSamplerate / 1000 );
	printf( "Impulsantwort:   \"%s\" (%d Samples)\n", argv[2], uiIRLength );
	printf( "Ausgabedatei:    \"%s\"\n", sOutfile.c_str( ) );
	printf( "Puffergroesse:   %d Samples\n\n", uiBufferSize );

	// Falten...
	printf( "\nBerechne %d Samples ...\n", l );
	WriteFromDatasourceToFile( pConv->getOutputDatasource( ), sOutfile, l, dGain, true, true );
	// WriteFromDatasourceToFile(&source, sOutfile, source.GetFileCapacity(), dGain, true, true);
	printf( "\nFertig!\n" );

	delete pConv;
	/*	} catch (ITAException& e) {
	        delete pConv;
	        fprintf(stderr, "Fehler %s\n", e.toString().c_str());
	        } catch (ITAException& e) {
	        delete pConv;
	        fprintf(stderr, "Fehler %s\n", e.toString().c_str());
	        } */

	printf( "Fertig.\n" );
	return EXIT_SUCCESS;
}
