#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITADirectConvolution.h>
#include <ITAStringUtils.h>
#include <cstring>
#include <stdio.h>

using namespace std;


void test1( )
{
	// Einfaches Durchfalten. Kein Austausch
	const int m = 7;
	const int n = 5;
	int o       = m + n - 1;

	float* s = new float[m];
	float* h = new float[n];
	float* g = new float[o];

	memset( s, 0, m * sizeof( float ) );
	memset( h, 0, n * sizeof( float ) );
	memset( g, 0, o * sizeof( float ) );

	for( int i = 0; i < ( m >> 1 ); i++ )
		s[i] = 1;
	for( int i = 0; i < ( n >> 1 ); i++ )
		h[i] = 1;
	// for (int i=0; i<n; i++)
	//  h[i] = 1;

	printf( "s = %s\n", FloatArrayToString( s, m ).c_str( ) );
	printf( "h = %s\n", FloatArrayToString( h, n ).c_str( ) );

	ITADirectConvolution conv( m, n );
	conv.Convolve( s, m, h, n, g, o );

	printf( "g = %s\n", FloatArrayToString( g, o ).c_str( ) );

	delete[] s;
	delete[] h;
	delete[] g;
}

int main( int, char** )
{
	test1( );
	return 0;
}
