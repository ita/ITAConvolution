#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITAFunctors.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

ITAAudiofileReader *pInputReader = NULL, *pFilterReader = NULL;
ITAAudiofileWriter* pOutputWriter = NULL;
vector<float*> vpfInputData;
vector<float*> vpfOutputData;
vector<float*> vpfFilterData;

void test1( )
{
	// Einfaches Durchfalten. Kein Austausch
	const int m = 16;
	const int n = 8;
	const int b = 4;
	int o       = uprmul( m + n - 1, b );

	float* s = new float[m];
	float* h = new float[n];
	float* g = new float[o];

	memset( s, 0, m * sizeof( float ) );
	memset( h, 0, n * sizeof( float ) );
	memset( g, 0, o * sizeof( float ) );

	for( int i = 0; i < ( m >> 1 ); i++ )
		s[i] = 1;
	for( int i = 0; i < ( n >> 1 ); i++ )
		h[i] = 1;

	ITAUPConvolution* conv = new ITAUPConvolution( b, n );

	ITAUPFilter* filter1 = new ITAUPFilter( b, n );
	filter1->Load( h, n );

	conv->ExchangeFilter( filter1 );

	int j = 0;
	for( int i = 0; i < uprdiv( o, b ); i++ )
	{
		// for (int i=0; i<1; i++) {
		conv->Process( s + j, g + j );
		j += b;
	}

	delete conv;
	delete filter1;

	delete[] s;
	delete[] h;
	delete[] g;
}

void test2( )
{
	// Einfaches Durchfalten. Einblenden
	const int m = 16;
	const int n = 8;
	const int b = 4;
	int o       = uprmul( m + n - 1, b );

	float* s = new float[m];
	float* h = new float[n];
	float* g = new float[o];

	memset( s, 0, m * sizeof( float ) );
	memset( h, 0, n * sizeof( float ) );
	memset( g, 0, o * sizeof( float ) );

	for( int i = 0; i < ( m >> 1 ); i++ )
		s[i] = 1;
	for( int i = 0; i < ( n >> 1 ); i++ )
		h[i] = 1;

	ITAUPConvolution* conv = new ITAUPConvolution( b, n );
	conv->SetFilterExchangeFadingFunction( ITABase::FadingFunction::LINEAR );
	conv->SetFilterCrossfadeLength( 3 );

	ITAUPFilter* filter1 = new ITAUPFilter( b, n );
	// conv->destroyFilter(filter1);
	filter1->Load( h, n );

	conv->ExchangeFilter( filter1 );

	int j = 0;
	for( int i = 0; i < uprdiv( o, b ); i++ )
	{
		// for (int i=0; i<1; i++) {
		conv->Process( s, g );
		j += b;
	}

	delete conv;
	delete filter1;

	delete[] s;
	delete[] h;
	delete[] g;
}

void test3( )
{
	// Einfaches Durchfalten. Einblenden
	const int m = 16;
	const int n = 8;
	const int b = 4;
	int o       = uprmul( m + n - 1, b );

	float* s  = new float[m];
	float* h1 = new float[n];
	float* h2 = new float[n];
	float* g  = new float[o];

	memset( s, 0, m * sizeof( float ) );
	memset( h1, 0, n * sizeof( float ) );
	memset( h2, 0, n * sizeof( float ) );
	memset( g, 0, o * sizeof( float ) );

	for( int i = 0; i < ( m >> 1 ); i++ )
		s[i] = 1;
	for( int i = 0; i < ( n >> 1 ); i++ )
		h1[i] = 1;
	h2[n - 1] = 1;

	ITAUPConvolution* conv = new ITAUPConvolution( b, n );
	conv->SetFilterExchangeFadingFunction( ITABase::FadingFunction::LINEAR );
	conv->SetFilterCrossfadeLength( 3 );

	ITAUPFilterPool* pool = new ITAUPFilterPool( b, n, 0 );
	ITAUPFilter* filter1  = pool->RequestFilter( );
	ITAUPFilter* filter2  = pool->RequestFilter( );
	// conv->destroyFilter(filter1);
	filter1->Load( h1, n );
	filter2->Load( h2, n );

	conv->ExchangeFilter( filter1 );

	int j = 0;
	for( int i = 0; i < uprdiv( o, b ); i++ )
	{
		// for (int i=0; i<1; i++) {
		if( i == 1 )
			conv->ExchangeFilter( filter2 );
		//                               vvv f�r Bereichspr�fung auf s
		conv->Process( s, g );
		j += b;
	}

	// writeAudiofile("out.wav", g, o, 44100);
	printf( "s = (%s)\n", FloatArrayToString( s, m, 1 ).c_str( ) );
	printf( "h1 = (%s)\n", FloatArrayToString( h1, n, 1 ).c_str( ) );
	printf( "h2 = (%s)\n", FloatArrayToString( h2, n, 1 ).c_str( ) );
	printf( "g = (%s)\n", FloatArrayToString( g, m + n - 1, 1 ).c_str( ) );


	delete conv;
	delete pool;

	delete[] s;
	delete[] h1;
	delete[] h2;
	delete[] g;
}

void test_filterpool( )
{
	// Einfaches Durchfalten. Einblenden
	const int b = 8;
	const int n = 8;
	float* s    = new float[n];

	ITAUPConvolution* conv = new ITAUPConvolution( b, n );
	ITAUPFilterPool* pool  = new ITAUPFilterPool( b, n, 0 );

	ITAUPFilter* f1 = pool->RequestFilter( );
	conv->ExchangeFilter( f1 );
	conv->Process( s, s );
	f1->Release( );

	ITAUPFilter* f2 = pool->RequestFilter( );
	conv->ExchangeFilter( f2 );
	conv->Process( s, s );

	ITAUPFilter* f3 = pool->RequestFilter( );
	conv->ExchangeFilter( f2 );
	conv->Process( s, s );

	delete conv;
	delete pool;

	delete[] s;
}

int main( int argc, char* argv[] )
{
	test1( );
	// test2();
	// test3();
	// test_filterpool();
	return 0;
}
