#ifndef __STAGE_H__
#define __STAGE_H__

#include <ITAAtomicPrimitives.h>
#include <ITACriticalSection.h>
#include <ITAFFT.h>
#include <ITAStopWatch.h>
#include <limits>
#include <string>
#include <tbb/concurrent_queue.h>
#include <vector>

// Vorw�rtsdeklarationen
class CInputBuffer1;
class CUFilter;
class CUPartition;
class Task;

// Debug-Schalter um die Ausgabe jeder Stufe in eine Datei zu schreiben
//#define STAGE_RECORD_OUTPUT

/*
    TODO-Liste:

    - _pfTemp wird einmal erzeugt und passt sich der �berblendl�nge nicht an.
    Dadurch steigt der Speicherplatzbedarf!

    */

// Konstante f�r unbegrenzte Zeiten
const int64_t INFINITE_TIME = INT64_MAX; // <- gr��ter int64_t Wert

/*
   Die Klasse Stage realisiert Faltungsstufen.
   Stufen nehmen Eingangsdaten entgegen und Falten diese mit einer
   spezifischen IR. Diese IR ist meist ein Teil der kompletten IR des
   Falters selbst. Stufen haben eine L�nge und eine Vielfachheit (multiplicity).
   Die Stufe hat als Eingabepuffer ein Schieberegister, indem
   sie neue Eingabedaten ablegt und �ltere eine gewissen Zyklus vorh�lt
   (wegen der Vielfachheit der Stufe), sowie einen Ausgabepuffer.
   Die insgesamt gesehenen IR die sie falten, haben eine L�nge des
   Produktes aus Stufenl�nge und Stufenvielfachheit. Stufen werden von
   Instanzen der Implementierungsklasse erzeugt und verwaltet.

   Die zentrale Idee einer Stufe ist die F�higkeit die Rechenzeiten f�r
   FFT/IFFT und spektrale Faltung in ein balanciertes Verh�ltnis zu bringen,
   durch mehrere IR-Teile gleicher L�nge. Denn: F�r jeden Block Eingabedaten
   mu� eine Stufe nur eine FFT und eine IFFT berechnen (gleiche L�nge der Teile).

   o Stufen arbeiten Tasks (Faltungs-Aufgaben) ab.
   o Stufen sind strikt kanal-gebunden
   o Jeder Task ist genau einer Stufe zugeordnet
   o Eine Stufe kann zu einem Zeitpunkt immer nur
   von einem Thread und/oder einer CPU/Core ausgef�hrt werden.

   */

class CStage
{
public:
	// Task-Status (R�ckgabewerte von Calc und CDI)
	enum
	{
		TASK_CALCULATION_COMPLETED = 0,
		TASK_QUANTUM_EXCEEDED      = 1,
		TASK_DEADLINE_EXCEEDED     = 2
	};

	const CUPartition* m_pPartition; // Uniforme Zerlegung
	const int& iPartLength;          // Teill�nge    [# Filterkoeffizienten]
	const int& iMultiplicity;        // Vielfachheit [# Filterteile]

	float* pfLeftOutput; // Ausgabepuffer (Zeitbereich)
	float* pfRightOutput;

	std::string sTag; // DEBUG: Hilfstag

	// ITANUFilterComponentImpl* pFC;		// Benutzte Filterkomponente
	// (Wird von aussen neu gesetzt mit Mutex)

	ITAAtomicInt iCrossfadeLength; // L�nge der �berblendung
	ITAAtomicInt iCrossfadeFunc;   // �berblendfunktion

	ITAStopWatch swSingleFFT, swSingleConvolution, swSingleIFFT, swCompleteIFFT;
	// ITAStopWatch swIFFT, swConvolution, swMix, swTotal;

	// Konstruktor: Konstruktiert eine Stufe anhand des Partitionierungsschemas.
	// Level ist der Index der Stufe innerhalb des Partitionierungsschemas.
	CStage( const CUPartition* pPartition );

	// Destruktor
	~CStage( );

	// Stufe zur�cksetzen
	void Reset( );

	// ID des aktuellen Task zur�ckgeben (0 falls kein Task)
	Task* GetCurrentTask( ) const;

	// N�chste Filterkomponente zuweisen
	void ExchangeFilter( CUFilter* pFilter );

	// Gesch�tzte Mindestberechnungsdauer eines neuen Task in Sekunden zur�ckgeben
	double EstimatedInitalQuantum( );

	// Einen Task berechnen bzw. dessen Berechnung fortsetzen
	// (R�ckgabewert: true, falls Task gerechnet,
	//                false wenn zwischenzeitlich abgebrochen
	int Compute( Task* pTask, bool bInterruptable, int64_t iRDTSC_EndOfQuantum );

	// Innere Klasse welche Position im Ablaufgraphen der Faltungs-Berechnung speichert
	class CSequencePointer
	{
	public:
		int iMain; // Punkt im Ablauf der gesamten Berechnung ("compute")
		int iCDI;  // Punkt im Ablauf Convolve/Downmix/IFFT

		enum
		{
			PREPARE = 0, // FFT der Eingabedaten durchgef�hren

			// 1. Berechnungspfad: Kein Austausch der IR
			PATH1 = 10,

			// 2. Berechnungspfad: IR-Austausch: Neue IR != 0, Alte IR = 0
			PATH2_LOAD = 20,
			PATH2_CDI  = 21,

			// 3. Berechnungspfad: IR-Austausch: Neue IR = 0, Alte IR != 0
			PATH3 = 30,

			// 4. Berechnungspfad: IR-Austausch: Neue IR != 0, Alte IR != 0
			PATH4_CDI_OLD = 40,
			PATH4_CDI_NEW = 41,

			COMPLETED = 100 // Berechnung abgeschlossen
		};

		static const int CDI_COMPLETED = INT_MAX;

		inline CSequencePointer( ) : iMain( PREPARE ), iCDI( 0 ) { };
	};

private:
	CUFilter* pCurFilter;                         // Aktuelles Filter
	CUFilter* pNewFilter;                         // Neues Filter
	tbb::concurrent_queue<CUFilter*> qNewFilters; // Queue f�r die neu zugewiesene Filter [lock-free]

	int nDFTCoeffs; // Anzahl der symmetrischen komplex-konjugierten DFT-Koeffzienten

	float* pfInput;    // Interner Eingabepuffer (Zeitbereich)
	float* pfLeftTemp; // Tempor�re Puffer f�r die �berblendung
	float* pfRightTemp;

	// Variablennamen: "_F" = Frequenzbereich
	std::vector<float*> vpfInput_F;    // Arrays der Eingabedaten (Frequenzbereich)
	std::vector<float*> vpfOutput_F;   // Arrays der Ausgabedaten (Frequenzbereich)
	std::vector<float*> vpfIR_F;       // Teile der Impulsantwort (Spektralbereich)
	std::vector<float*> vpfLeftMix_F;  // Zeiger auf alle zu mischenden spektral Daten
	std::vector<float*> vpfRightMix_F; // Zeiger auf alle zu mischenden spektral Daten

	// DEBUG:
#ifdef STAGE_RECORD_OUTPUT
	ITAAudiofileWriter* pafwDebug;
	std::vector<float*> vpfDebugChannels;
	bool bDebugFirst;
#endif

	// -= Berechnungs-interne Variablen =-----------------------------------------

	// Hinweis: Diese Variablen sind im Klassen-Scope definiert, wg. der
	//          Unterbrechbarkeit der Berechnungsmethode calc

	// bool bExchangeFilter;

	ITAFFT fft, ifft;
	ITAAtomicPtr<Task> pCurTask;

	// Alle Faltungen durchf�hren, die Ergebnisse im Spektralbereich mischen
	// und das Misch-Ergebnis in den Zeitbereich transformieren (in pfOutput)
	//
	// Deadline = 0 -> Keine Breakpoints
	// R�ckgabewerte: true   Berechnung vollst�ndig durchgef�hrt
	//                false  Abbruch durch Breakpoint
	// bDestOutput = true -> Faltungsergebnis geht nach Left/Right-Output
	//               sonst -> "" Left/Right-Temp
	inline int convolveDownmixIFFT( Task* pTask, bool bInterruptable, int64_t iRDTSC_EndOfQuantum, bool bDestOutput = true );

	// Einmaliges Ausmessen der dominanten Rechenoperationen
	void measure( );

	void printSomething( );

	// Handler: Arbeit an einem Task beenden (vollst�ndig|unvollst�ndig|versp�tet)
	int updateTask( int iResult );
};

#endif // __STAGE_H__
