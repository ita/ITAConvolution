#ifndef __UFilterPOOL_H__
#define __UFilterPOOL_H__

#include "ITANUPartitioningScheme.h"

#include <ITACriticalSection.h>
#include <list>

// Vorw�rtsdeklarationen
class CUFilter;

// TODO: Lock-free Implementierung?

class UFilterPool
{
public:
	// Konstruktor
	UFilterPool( const CUPartition* pPartition, int iInitialSize );

	// Destruktor
	virtual ~UFilterPool( );

	// Anzahl freier Filter zur�ckgeben
	int GetNumFreeFilters( ) const;

	// Anzahl freier Filter zur�ckgeben
	int GetNumUsedFilters( ) const;

	// Anzahl freier Filter zur�ckgeben
	int GetNumTotalFilters( ) const;

	// Neue freie Filter erzeugen
	void Grow( int iDelta );

	// Freies Filter anfordern
	CUFilter* RequestFilter( );

	// Filter wieder zur anderweitigen Benutzung freischalten
	void ReleaseFilter( CUFilter* pFilter );

private:
	const CUPartition* m_pPartition;
	std::list<CUFilter*> m_lpFreeFilters; // Unbenutzte (freie) Filter im Pool
	std::list<CUFilter*> m_lpUsedFilters; // Benutzte Filter im Pool
	std::list<CUFilter*> m_lpAutoFilters; // Filter welche nicht mehr seitens den Benutzers
	// gebraucht werden, aber noch in einem Falter benutzt werden
	ITACriticalSection m_csFilters; // Exklusiver Zugriff auf die Listen

	// Creator f�r neue Filter
	CUFilter* CreateFilter( );
};

#endif // __UFilterPOOL_H__
