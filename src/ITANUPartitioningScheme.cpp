#include "ITANUPartitioningScheme.h"

#include <ITAASCIITable.h>
#include <cassert>
#include <sstream>

NUPartition::NUPartition( const std::vector<int>& vParts )
{
	iLength = iNumSegments = 0;
	int nParts             = (int)vParts.size( );
	if( nParts == 0 )
		return;

	int k = 0; // L�nge des vorhergehenden Teiles
	for( int i = 0; i < nParts; i++ )
	{
		// Im ersten Durchgang nur die Stufen z�hlen:
		if( vParts[i] != k )
		{
			k = vParts[i];
			iNumSegments++;
		}
	}

	voSegments.resize( iNumSegments );

	// --= Segmente zusammenstellen =--

	k     = 0;
	int s = 0;         // Segmentz�hler
	int b = vParts[0]; // Teill�nge

	for( int i = 0; i < nParts; i++ )
	{
		assert( vParts[i] > 0 );

		if( vParts[i] != k )
		{
			k = vParts[i];

			// TODO: �ber UPartition Konstruktor regeln
			// Informationen speichern
			voSegments[s].iOffset       = iLength;
			voSegments[s].iPartLength   = k;
			voSegments[s].iMultiplicity = 1;
			voSegments[s].iLength       = voSegments[s].iPartLength * voSegments[s].iMultiplicity;
			voSegments[s].iLevel        = s;

			/* Zyklusnummer (Hauptzeitz�hler) indem soviele Eingabedaten
			   bereitstehen, wie die Teill�nge der Stufe ist.
			   Hier kann die Berechnung der Stufe starten.
			   Hinweis: Im n-ten Takt stehen n+1 Blockl�ngen Eingabedaten bereit!
			   */
			int tReady = ( k / b ) - 1;

			/* Zyklusnummer (Hauptzeitz�hler) indem die Berechnung der Stufe
			   abgeschlossen werden muss (Deadline) */
			int tDeadline = ( iLength <= b ? 0 : ( iLength / b ) - 1 );

			voSegments[s].iClearance = tDeadline - tReady;
			assert( voSegments[s].iClearance >= 0 );
			s++;
		}
		else
		{
			// Vielfachheit inkrementieren
			voSegments[s - 1].iMultiplicity++;
			voSegments[s - 1].iLength = voSegments[s - 1].iPartLength * voSegments[s - 1].iMultiplicity;
		}

		iLength += vParts[i];
	}
}

std::string NUPartition::ToString( ) const
{
	if( iNumSegments == 0 )
		return "Empty partition scheme of length 0";

	ITAASCIITable t( iNumSegments, 5 );
	t.setColumnTitle( 0, "Stage" );
	t.setColumnTitle( 1, "Partlength" );
	t.setColumnTitle( 2, "Multiplicity" );
	t.setColumnTitle( 3, "Offset" );
	t.setColumnTitle( 4, "Clearance" );

	for( int i = 0; i < iNumSegments; i++ )
	{
		t.setContent( i, 0, i );
		t.setContent( i, 1, voSegments[i].iPartLength );
		t.setContent( i, 2, voSegments[i].iMultiplicity );
		t.setContent( i, 3, voSegments[i].iOffset );
		t.setContent( i, 4, voSegments[i].iClearance );
	}

	std::stringstream ss;
	ss << "Filter partition of length " << iLength << std::endl << std::endl << t.toString( ) << std::endl;
	return ss.str( );
}
