#include "ITANUPCStage.h"

#include "ITANUPCFade.h"
#include "ITANUPCHelpers.h"
#include "ITANUPCInputBuffer1.h"
#include "ITANUPCTask.h"
#include "ITANUPCUFilter.h"
#include "ITANUPCUtils.h"
#include "ITANUPConvolutionImpl.h"
#include "ITANUPartitioningScheme.h"

#include <ITAASCIITable.h>
#include <ITADebug.h>
#include <ITAFastMath.h>
#include <ITAHPT.h>
#include <ITANUPConvolution.h>
#include <algorithm>
#include <iostream>
#include <string>

#define WITH_SEPARATED_IFFT

#define USE_BREAKPOINTS
//#define WITH_PROFILING

// Diese Pr�prozessordefinition "einkommentieren" um die Unterbrechung der Berechnung in den Stufen zu unterbinden
// (Nur f�r Debug-Zwecke. Die Performance wird dann wesentlich schlechter!)
//#define DISABLE_CALCULATION_INTERRUPTION

CStage::CStage( const CUPartition* pPartition )
    // Standardgr��e f�r die Queue = 16 Elemente (das sollte reichen)
    : m_pPartition( pPartition )
    , iPartLength( pPartition->iPartLength )
    , iMultiplicity( pPartition->iMultiplicity )
    ,

    pfInput( nullptr )
    , pfLeftTemp( nullptr )
    , pfRightTemp( nullptr )
    , pfLeftOutput( nullptr )
    , pfRightOutput( nullptr )
    ,

    pCurFilter( nullptr )
{
	assert( pPartition->iLevel >= 0 );

	// DEBUG: printf("Created Stage: Level %d, Index %d\n", uiLevel, iStartIndex);

	// Felder initialisieren
	pCurTask         = nullptr;
	pCurFilter       = nullptr;
	pNewFilter       = nullptr;
	iCrossfadeLength = 0;
	iCrossfadeFunc   = ITANUPC::LINEAR;

	// Anzahl der komplexen Koeffizienten
	// (um 1 erweitert um f�r SIMD-Instruktionen die 16-Byte Ausrichtung zu erhalten!)
	assert( iPartLength % 2 == 0 );
	nDFTCoeffs = iPartLength + 2;

	// Puffer und Arrays allozieren
	// TODO: Fehler bei der Speicherallozierung abfangen
	pfInput       = fm_falloc( 2 * iPartLength, true );
	pfLeftTemp    = fm_falloc( 2 * iPartLength, true );
	pfRightTemp   = fm_falloc( 2 * iPartLength, true );
	pfLeftOutput  = fm_falloc( 2 * iPartLength, true );
	pfRightOutput = fm_falloc( 2 * iPartLength, true );

	vpfInput_F.resize( iMultiplicity, nullptr );
	for( int i = 0; i < iMultiplicity; i++ )
		// Allozieren und mit Nullsamples f�llen
		vpfInput_F[i] = fm_falloc( 2 * nDFTCoeffs, true );

	// fwe: Ausgabe ist 2-Kanalig! Deshalb *2
	vpfOutput_F.resize( 2 * iMultiplicity, nullptr );
	for( int i = 0; i < 2 * iMultiplicity; i++ )
		vpfOutput_F[i] = fm_falloc( 2 * nDFTCoeffs, true );

	vpfLeftMix_F.reserve( iMultiplicity );
	vpfRightMix_F.reserve( iMultiplicity );

	vpfIR_F.resize( 2 * iMultiplicity, nullptr );

	fft.plan( ITAFFT::FFT_R2C, 2 * iPartLength, pfInput, vpfInput_F[0] );
	ifft.plan( ITAFFT::IFFT_C2R, 2 * iPartLength, vpfOutput_F[0], pfLeftOutput );

#ifdef STAGE_RECORD_OUTPUT
	bDebugFirst = true;
	pafwDebug   = nullptr;
#endif

	// Stufe einmessen
	measure( );
}

CStage::~CStage( )
{
#ifdef STAGE_RECORD_OUTPUT
	delete pafwDebug;
#endif

	fm_free( pfInput );
	fm_free( pfLeftTemp );
	fm_free( pfRightTemp );
	fm_free( pfLeftOutput );
	fm_free( pfRightOutput );

	std::for_each( vpfInput_F.begin( ), vpfInput_F.end( ), fm_free );
	std::for_each( vpfOutput_F.begin( ), vpfOutput_F.end( ), fm_free );

	printSomething( );
}

void CStage::Reset( )
{
	// Alle Puffer mit Nullen f�llen
	fm_zero( pfInput, 2 * iPartLength );

	for( int i = 0; i < iMultiplicity; i++ )
		fm_zero( vpfInput_F[i], 2 * nDFTCoeffs );

	fm_zero( pfLeftOutput, 2 * iPartLength );
	fm_zero( pfRightOutput, 2 * iPartLength );

	// TODO: Clear Task?
	pCurTask = nullptr;
}

double CStage::EstimatedInitalQuantum( )
{
	// Dominate Berechung beim Einstieg: Die Eingabe FFT
	double t = swSingleFFT.mean( );
	return ( t <= 0 ? 0 : swSingleFFT.mean( ) );
}

Task* CStage::GetCurrentTask( ) const
{
	return pCurTask;
}

void CStage::ExchangeFilter( CUFilter* pFilter )
{
	// DEBUG_PRINTF("[Stage 0x%08Xh] Exchanging to filter 0x%08Xh\n", this, pNewFilter);

	// Platzierung des Filter vermerken
	if( pFilter )
		pFilter->m_oState.addPrep( );

	// N�chstes Filter in die Austausch-Queue einf�gen
	qNewFilters.push( pFilter );
}

int CStage::Compute( Task* pTask, bool bInterruptable, int64_t iRDTSC_EndOfQuantum )
{
#ifdef STAGE_RECORD_OUTPUT
	if( bDebugFirst )
	{
		ITAAudiofileProperties props = { ITA_INT16, ITA_TIME_DOMAIN, 2, 44100, 0, "Up on stage, buddy!" };
		pafwDebug                    = ITAAudiofileWriter::create( sTag + "_out.wav", props );
		vpfDebugChannels.push_back( pfLeftOutput );
		vpfDebugChannels.push_back( pfRightOutput );
		bDebugFirst = false;
	}
#endif

#ifdef WITH_PROFILING
	// swTotal.start();
#endif

	assert( pTask );
	assert( iRDTSC_EndOfQuantum > 0 );

	int& main_sp = pTask->SSP.iMain; // Main seq. pointer
	int& cdi_sp  = pTask->SSP.iCDI;  // CDI seq. pointer

	// Sicherheitschecks
	// TEST: Attempt to use locked stage with a different task
	assert( !pCurTask || ( ( pTask == pCurTask ) && ( pTask->pStage == this ) ) );

	if( main_sp == CStage::CSequencePointer::PREPARE )
	{
		/*
		    Pr�fen, ob die rechen-intensivste Operation in dieser Sektion, die FFT,
		    noch innerhalb der Deadline gerechnet werden kann. Falls nicht, so wird
		    der Task erst gar nicht angefangen
		*/
		bool bContinue = false;

		double dEstimatedDuration = swSingleFFT.mean( );
		if( dEstimatedDuration <= 0 )
			// Kein bisheriger Messwert vorhanden -> Auf jedenfall rechnen...
			bContinue = true;
		else if( ( ITAHPT_now( ) + toTimerTicks( dEstimatedDuration ) ) < iRDTSC_EndOfQuantum )
			bContinue = true;

		if( !bInterruptable )
			bContinue = true;

		if( bContinue )
		{
			// Stufe sperren!
			pCurTask      = pTask;
			pTask->pStage = this;

			/* +-----------------------------------------------------------------------+
			   | Die gesch�tze Laufzeit f�r diese Sektion liegt innerhalb des Quantums |
			   | Berechnung durchf�hren...                                             |
			   +-----------------------------------------------------------------------+ */

			// Startzeit vermerken
			pTask->iRDTSCStart = ITAHPT_now( );

			/* 1. Vorherigen Datenblock in die linke Pufferh�lfte und
			      neue Daten in die rechte Pufferh�lfte kopieren */

			fm_copy( pfInput, pfInput + iPartLength, iPartLength );
			pTask->pInput->getInputBuffer( )->get( pfInput + iPartLength, pTask->iInputOffset, iPartLength );

			/* 2. In den Frequenzbereich transformierte Eingangsdaten vorheriger
			      Berechnungen im Pufferarray zyklisch weiterschieben (shiften)

			      Alle bereits transformierten Eingabesignale im Spektralbereich
			      jeweils an den n�chsth�heren Blockfalter weitergeben und die des
			      rangh�chsten Falters als Ziel f�r die neuen Eingangsdaten im
			      Frequenz bereich verwenden. Stille-Flags werden mit vertauscht. */

			float* pfTemp = vpfInput_F[iMultiplicity - 1];
			for( unsigned i = iMultiplicity - 1; i > 0; i-- )
				vpfInput_F[i] = vpfInput_F[i - 1];
			vpfInput_F[0] = pfTemp;

			/* 3. FFT der neuen Eingangsdaten berechnen

			      Hinweis: Durch der durchschieben der Pufferzeiger ist das Ziel f�r die
			               FFT nun nicht mehr das vorauf die FFT geplant wurde.
			               Deshalb muss sie unter Zuhilfenahme des fftw3-Guru-Interface
			               auf ein anderes Zielarray ausgef�hrt werden */

			// Powersaver: FFT nur rechnen, wenn keine Stille vorhanden

			swSingleFFT.start( );
			fft.execute( pfInput, vpfInput_F[0] );
			swSingleFFT.stop( );

			/* 4. Weiteren Berechnungspfad bestimmen */

			// Neuste gew�nschte Filterkomponente holen...
			// Wichtig: Bei allen die '�berschrieben' wurden,
			//          muss der Placement-RC zur�ckgesetzt werden.

			// TODO: Dieser Code muss noch getestet werden!

			// Test auf neue Filter nur dann durchf�hren, wenn der Trigger signalisiert wurde
			// oder gar kein Trigger zuordnet ist (dies wird in der TriggerWatch realisiert).
			// if (m_oTriggerWatch.fire())
			{
				CUFilter* pTempFilter = nullptr;
				CUFilter* pPrevFilter = nullptr;
				bool bNewFilters      = false;

				// Konsumiere alle bis auf die Letzte.
				while( qNewFilters.try_pop( pTempFilter ) )
				{
					bNewFilters = true;

					DEBUG_PRINTF( "[Stage 0x%08Xh] Popped next filter 0x%08Xh\n", this, pTempFilter );
					if( pPrevFilter )
						pPrevFilter->m_oState.removePrep( );
					pPrevFilter = pTempFilter;
				}

				if( bNewFilters )
				{
					pNewFilter = pPrevFilter;
				}
			}

			if( pNewFilter == pCurFilter )
			{
				main_sp               = CStage::CSequencePointer::PATH1;
				pTask->dEstMinQuantum = ( pNewFilter ? swSingleConvolution.mean( ) : 0 );
			}
			else
			{
				if( !pCurFilter && pNewFilter )
					main_sp = CStage::CSequencePointer::PATH2_LOAD;
				if( pCurFilter && !pNewFilter )
					main_sp = CStage::CSequencePointer::PATH3;
				if( pCurFilter && pNewFilter )
					main_sp = CStage::CSequencePointer::PATH4_CDI_OLD;
				pTask->dEstMinQuantum = swSingleConvolution.mean( );
			}

			// CDI-Sequence-Pointer des Task initialisieren
			cdi_sp = 0;

			// TODO: Hier muss der Spezialfall rein
			// Gesch�tzte n�chste Bearbeitungszeit: Faltung
			assert( pTask->dEstMinQuantum >= 0 );
			// if (pTask->dEstMinQuantum < 0) pTask->dEstMinQuantum = 0;
		}
		else
		{
			// Selbst die eing�ngliche FFT passt nicht ins Zeitbudget
			// No run. Gesch�tzte n�chste Bearbeitungszeit: FFT
			pTask->dEstMinQuantum = swSingleFFT.mean( );
			assert( pTask->dEstMinQuantum >= 0 );
			// if (pTask->dEstMinQuantum < 0) pTask->dEstMinQuantum = 0;
		}
	}

	/* +------------------------------------------------------+
	   | 1. Berechnungspfad: Kein Austausch der Impulsantwort |
	   +------------------------------------------------------+ */

	if( main_sp == CStage::CSequencePointer::PATH1 )
	{
		if( !pCurFilter )
		{
			// Direkt Nullen in die Ausgabe schreiben
			fm_zero( pfLeftOutput, iPartLength );
			fm_zero( pfRightOutput, iPartLength );

			// Berechnung dieses Tasks ist abgeschlossen!
			return updateTask( TASK_CALCULATION_COMPLETED );
		}
		else
		{
			int iResult = convolveDownmixIFFT( pTask, bInterruptable, iRDTSC_EndOfQuantum );
			return updateTask( iResult );
			/*
			if (iResult == TASK_DEADLINE_EXCEEDED) {
			    return TASK_DEADLINE_EXCEEDED;
			}

			if (uiResult == TASK_CALCULATION_COMPLETED) {
			    // DEBUG: Ergebnis schreiben...
#ifdef STAGE_RECORD_OUTPUT
			    pafwDebug->write(iPartLength, vpfDebugChannels);
#endif

			    // Berechnung dieses Tasks ist abgeschlossen!
			    pTask->iRDTSCEnd = ITAHPT_now();	// Zeit der Fertigstellung vermerken
			    iCurrentTaskID = 0;
			    main_sp = Stage::SequencePointer::COMPLETED;
			    return TASK_CALCULATION_COMPLETED;
			}
			*/
		}
	}

	/* +-------------------------------------------------------------+
	   | 2. Berechnungspfad: IR-Austausch: Neue IR != 0, Alte IR = 0 |
	   +-------------------------------------------------------------+ */

	if( main_sp == CStage::CSequencePointer::PATH2_LOAD )
	{
		// Neues Filter sperren
		assert( pNewFilter );
		pNewFilter->m_oState.xchangePrep2Use( );

		// Zugriffszeiger die Teile der neuen IR laden
		for( int i = 0; i < 2 * iMultiplicity; i++ )
			vpfIR_F[i] = pNewFilter->vpfPartSpecData[i];

		assert( !pCurFilter );
		pCurFilter = pNewFilter;
		main_sp    = CStage::CSequencePointer::PATH2_CDI;
	}

	if( main_sp == CStage::CSequencePointer::PATH2_CDI )
	{
		// Nur mit der neuen Impulsantwort Falten, spektral Mischen
		// und R�cktransformieren // (Ergebnis in pfLeftOutput/pfRightOutput speichern)
		int iResult = convolveDownmixIFFT( pTask, bInterruptable, iRDTSC_EndOfQuantum );

		if( iResult == TASK_CALCULATION_COMPLETED )
		{
			// Faltungsergebnis einblenden (falls �berblendl�nge > 0)
			if( iCrossfadeLength > 0 )
			{
				fade_in( pfLeftOutput, iCrossfadeLength, iCrossfadeFunc );
				fade_in( pfRightOutput, iCrossfadeLength, iCrossfadeFunc );
			}

			// DEBUG: Ergebnis schreiben...
#ifdef STAGE_RECORD_OUTPUT
			pafwDebug->write( iPartLength, vpfDebugChannels );
#endif
		}

		return updateTask( iResult );
	}

	/* +-------------------------------------------------------------+
	   | 3. Berechnungspfad: IR-Austausch: Neue IR = 0, Alte IR != 0 |
	   +-------------------------------------------------------------+ */

	if( main_sp == CStage::CSequencePointer::PATH3 )
	{
		// Alte Impulsantwort Falten, spektral Mischen, R�cktransformieren
		// (Ergebnis in pfLeftOutput/pfRightOutput speichern)
		int iResult = convolveDownmixIFFT( pTask, bInterruptable, iRDTSC_EndOfQuantum );

		// if (iResult == TASK_DEADLINE_EXCEEDED) return TASK_DEADLINE_EXCEEDED;

		/* Wozu?
		if (iResult == TASK_DEADLINE_EXCEEDED) {
		    // Zugriffszeiger die Teile der neuen IR setzen
		    for (int i=0; i<2*iMultiplicity; i++) vpfIR_F[i] = 0;

		    // Altes Filter entsperren
		    pCurFilter->m_oState.removeUse();

		    // Impulsantwort aktualisieren
		    assert( !pNewFilter );
		    pCurFilter = pNewFilter;

		    return TASK_DEADLINE_EXCEEDED;
		}
		*/

		if( iResult == TASK_CALCULATION_COMPLETED )
		{
			// Faltungsergebnis ausblenden (falls �berblendl�nge > 0)
			if( iCrossfadeLength > 0 )
			{
				fade_out( pfLeftOutput, iCrossfadeLength, iCrossfadeFunc );
				fade_out( pfRightOutput, iCrossfadeLength, iCrossfadeFunc );
			}

			// Zugriffszeiger die Teile der neuen IR setzen
			for( int i = 0; i < 2 * iMultiplicity; i++ )
				vpfIR_F[i] = 0;

			// Altes Filter entsperren
			pCurFilter->m_oState.removeUse( );

			// Impulsantwort aktualisieren
			assert( !pNewFilter );
			pCurFilter = pNewFilter;

			// DEBUG: Ergebnis schreiben...
#ifdef STAGE_RECORD_OUTPUT
			pafwDebug->write( iPartLength, vpfDebugChannels );
#endif
		}

		return updateTask( iResult );
	}

	/* +--------------------------------------------------------------+
	   | 4. Berechnungspfad: IR-Austausch: Neue IR != 0, Alte IR != 0 |
	   +--------------------------------------------------------------+ */

	if( main_sp == CStage::CSequencePointer::PATH4_CDI_OLD )
	{
		if( iCrossfadeLength == 0 )
		{
			/* Wichtig: Folgender Spezialfall ist zu beachten:
			            �berblendl�nge == 0. In diesem Falle wird das
			            Faltungsergebnis mit der alten IR nicht ben�tigt. */

			// Neues Filter sperren
			pNewFilter->m_oState.xchangePrep2Use( );

			// Zugriffszeiger die Teile der neuen IR laden
			for( int i = 0; i < 2 * iMultiplicity; i++ )
				vpfIR_F[i] = pNewFilter->vpfPartSpecData[i];

			// Altes Filter entsperren
			pCurFilter->m_oState.removeUse( );

			// Impulsantwort aktualisieren
			assert( pNewFilter );
			pCurFilter = pNewFilter;

			main_sp = CStage::CSequencePointer::PATH4_CDI_NEW;
		}
		else
		{
			// Alte Impulsantwort Falten, spektral Mischen, R�cktransformieren
			// (Ergebnis in _pfLeftTemp/_pfRightTemp speichern)
			int iResult = convolveDownmixIFFT( pTask, bInterruptable, iRDTSC_EndOfQuantum, false );

			if( iResult == TASK_DEADLINE_EXCEEDED )
				return TASK_DEADLINE_EXCEEDED;

			// Gesch�tzte n�chste Bearbeitungszeit vermerken
			// (Muss hier geschehen, da CDI nach diesem Punkt nochmals aufgerufen wird)
			pTask->dEstMinQuantum = swSingleConvolution.mean( );
			if( pTask->dEstMinQuantum < 0 )
				pTask->dEstMinQuantum = 0;

			/* Wozu?
			if (iResult == TASK_DEADLINE_EXCEEDED) {
			    // Wichtig: Noch den Austausch der IRs vollziehen!

			    // Zugriffszeiger die Teile der neuen IR laden
			    for (int i=0; i<2*iMultiplicity; i++)
			        vpfIR_F[i] = pNewFilter->vpfPartSpecData[i];

			    // Altes Filter entsperren
			    --pCurFilter->rcUseRefCount;

			    // Impulsantwort aktualisieren
			    pCurFilter = pNewFilter;

			    return TASK_DEADLINE_EXCEEDED;
			}
			*/

			if( iResult == TASK_CALCULATION_COMPLETED )
			{
				// Neues Filter sperren
				assert( pNewFilter );
				pNewFilter->m_oState.xchangePrep2Use( );

				// Zugriffszeiger die Teile der neuen IR laden
				for( int i = 0; i < 2 * iMultiplicity; i++ )
					vpfIR_F[i] = pNewFilter->vpfPartSpecData[i];

				// Altes Filter entsperren
				pCurFilter->m_oState.removeUse( );

				// Impulsantwort aktualisieren
				pCurFilter = pNewFilter;

				// N�chster Schritt: Mit neuer Impulsantwort falten
				// (CDI-Sequence-Pointer des Task zur�cksetzen)
				cdi_sp  = 0;
				main_sp = CStage::CSequencePointer::PATH4_CDI_NEW;
			}
		}
	}

	if( main_sp == CStage::CSequencePointer::PATH4_CDI_NEW )
	{
		// Alte Impulsantwort Falten, spektral Mischen, R�cktransformieren
		// (Ergebnis in pfLeftOutput/pfRightOutput speichern)
		int iResult = convolveDownmixIFFT( pTask, bInterruptable, iRDTSC_EndOfQuantum );

		// if (iResult == TASK_DEADLINE_EXCEEDED) return TASK_DEADLINE_EXCEEDED;

		if( iResult == TASK_CALCULATION_COMPLETED )
		{
			if( iCrossfadeLength > 0 )
			{
				// Beide Faltungsergebnisse ineinander �berblenden
				crossfade( pfLeftOutput, pfLeftTemp, iCrossfadeLength, iCrossfadeFunc );
				crossfade( pfRightOutput, pfRightTemp, iCrossfadeLength, iCrossfadeFunc );
			}

			// DEBUG: Ergebnis schreiben...
#ifdef STAGE_RECORD_OUTPUT
			pafwDebug->write( iPartLength, vpfDebugChannels );
#endif
		}

		return updateTask( iResult );
	}

	// Task konnte nicht fertiggestellt werden. Z�hler der Unterbrechungen erh�hen
	++pTask->nInterruptions;

	return TASK_QUANTUM_EXCEEDED;
}

inline int CStage::convolveDownmixIFFT( Task* pTask, bool bInterruptable, int64_t iRDTSC_EndOfQuantum, bool bDestOutput )
{
	/* Wichtig:

	   Hier ist ein Power-Saver eingebaut:

	   - Nullzeiger-Impulsantwortteile werden nicht gefaltet
	   - Es wird nur das gemischt, was berechnet wurde

	*/

	assert( iRDTSC_EndOfQuantum > 0 );

	const int MIX  = 2 * iMultiplicity;
	const int IFFT = MIX + 1;
	ITATimerTicks ttNow;
	// bool bContinue;
	int& sp = pTask->SSP.iCDI; // Sequence-pointer Referenz

	// -= Test auf Deadlock-Abbruch =------------------------------
	ttNow = ITAHPT_now( );
	if( ttNow >= pTask->iRDTSCDeadline )
		return TASK_DEADLINE_EXCEEDED;
	// ------------------------------------------------------------

	if( sp < MIX )
	{
		// Im Spektralbereich falten
		if( sp == 0 )
		{
			vpfLeftMix_F.clear( );
			vpfRightMix_F.clear( );
		}

		for( ; sp < 2 * iMultiplicity; sp++ )
		{
			// Alle zu Zeiger auf zu einmischende Puffer in _ppfMix_F sammeln

			// Falls der Zugriffszeiger auf die spektralen Daten der
			// Nullzeiger ist, so werden die spektralen Daten als Null
			// angenommen. Es mu� nicht gefaltet werden.

			// Powersaver: Komplexe Faltung �berspringen, falls Stille vorliegt.
			// TODO-Powersaver: if ((_vpfIR_F[sp] != 0) && (!_vbInputSilence_F[sp])) {
			if( vpfIR_F[sp] != 0 )
			{
				// Zeitquantum �berpr�fen
				bool bRun = false;

				double dEstimatedDuration = swSingleConvolution.mean( );
				if( dEstimatedDuration <= 0 )
					// Kein bisheriger Messwert vorhanden -> Auf jedenfall rechnen...
					bRun = true;
				else if( ( ITAHPT_now( ) + toTimerTicks( dEstimatedDuration ) ) < iRDTSC_EndOfQuantum )
					bRun = true;

				if( !bInterruptable )
					bRun = true;

				if( !bRun )
					return TASK_QUANTUM_EXCEEDED;

				swSingleConvolution.start( );
				fm_cmul_x( vpfOutput_F[sp], vpfInput_F[sp / 2], vpfIR_F[sp], nDFTCoeffs );
				swSingleConvolution.stop( );

				if( ( sp % 2 ) == 0 ) // Linker Kanal?
					vpfLeftMix_F.push_back( vpfOutput_F[sp] );
				else
					vpfRightMix_F.push_back( vpfOutput_F[sp] );
			}
			else
			    // Teil ist Null
			    if( sp < 2 )
			{
				// _vpfOutput_F[0] ist immer dabei (n�tig wegen IFFT)
				fm_zero( vpfOutput_F[sp], nDFTCoeffs * 2 );

				if( ( sp % 2 ) == 0 ) // Linker Kanal?
					vpfLeftMix_F.push_back( vpfOutput_F[sp] );
				else
					vpfRightMix_F.push_back( vpfOutput_F[sp] );
			}
		}


		// N�chste Aufgabe: Spektral zusammenmischen
		// Wichtig: Kein sp++! Dies geschah schon in der for-Schleife!
	}

	// Gesch�tzte n�chste Bearbeitungszeit: IFFT
#ifdef WITH_SEPARATED_IFFT
	pTask->dEstMinQuantum = swSingleIFFT.mean( );
	if( pTask->dEstMinQuantum < 0 )
		pTask->dEstMinQuantum = 0;
#else
	pTask->dEstMinQuantum = swCompleteIFFT.mean( );
	if( pTask->dEstMinQuantum < 0 )
		pTask->dEstMinQuantum = 0;
#endif

	// -= Test auf Deadlock-Abbruch =------------------------------
	ttNow = ITAHPT_now( );
	if( ttNow >= pTask->iRDTSCDeadline )
		return TASK_DEADLINE_EXCEEDED;
	// ------------------------------------------------------------

	if( sp == MIX )
	{
		// Faltungsergebnisse im Spektralbereich mischen
		// Hinweis: Hier keine Unterbrechbarkeit, da die Mischoperation sehr kurz ist

		ITANUPCFVMix( vpfLeftMix_F, nDFTCoeffs * 2 );
		ITANUPCFVMix( vpfRightMix_F, nDFTCoeffs * 2 );

		// N�chste Aufgabe: Spektral zusammenmischen
		sp++;
	}

	// -= Test auf Deadlock-Abbruch =------------------------------
	ttNow = ITAHPT_now( );
	if( ttNow >= pTask->iRDTSCDeadline )
		return TASK_DEADLINE_EXCEEDED;
		// ------------------------------------------------------------

#ifdef WITH_SEPARATED_IFFT

	// Neuer Code: Separierte Ausgabe-IFFTs
	if( sp == IFFT )
	{
		// Zeitquantum �berpr�fen
		bool bRun = false;

		double dEstimatedDuration = swSingleIFFT.mean( );
		if( dEstimatedDuration <= 0 )
			// Kein bisheriger Messwert vorhanden -> Auf jedenfall rechnen...
			bRun = true;
		else if( ( ITAHPT_now( ) + toTimerTicks( dEstimatedDuration ) ) < iRDTSC_EndOfQuantum )
			bRun = true;

		if( !bInterruptable )
			bRun = true;

		if( !bRun )
			return TASK_QUANTUM_EXCEEDED;

		// Ergebnisse durch die L�nge teilen (siehe Theorie!)
		// Hinweis: Korrekt am 12.2.2005: Der Faktor war falsch. Es mu�
		//          hier 1/(2*iPartLength) lauten! Trotzdem nochmal die Theorie pr�fen
		fm_mul( vpfOutput_F[0], 1 / ( 2 * (float)iPartLength ), nDFTCoeffs * 2 );

		swSingleIFFT.start( );
		// IFFT des linken Kanals berechnen und Ergebnis im Ausgabepuffer speichern
		if( bDestOutput )
		{
			ifft.execute( vpfOutput_F[0], pfLeftOutput );
		}
		else
		{
			// Ausgabe in die tempor�ren Puffer, mittels des fftw3-Guru-Interface:
			ifft.execute( vpfOutput_F[0], pfLeftTemp );
		}
		swSingleIFFT.stop( );

		// N�chster Punkt: Ausgabe f�r rechten Kanal
		sp++;
	}

	if( sp == IFFT + 1 )
	{
		// Zeitquantum �berpr�fen
		bool bRun = false;

		double dEstimatedDuration = swSingleIFFT.mean( );
		if( dEstimatedDuration <= 0 )
			// Kein bisheriger Messwert vorhanden -> Auf jedenfall rechnen...
			bRun = true;
		else if( ( ITAHPT_now( ) + toTimerTicks( dEstimatedDuration ) ) < iRDTSC_EndOfQuantum )
			bRun = true;

		if( !bInterruptable )
			bRun = true;

		if( !bRun )
			return TASK_QUANTUM_EXCEEDED;

		// Ergebnisse durch die L�nge teilen (siehe Theorie!)
		// Hinweis: Korrekt am 12.2.2005: Der Faktor war falsch. Es mu�
		//          hier 1/(2*iPartLength) lauten! Trotzdem nochmal die Theorie pr�fen
		fm_mul( vpfOutput_F[1], 1 / ( 2 * (float)iPartLength ), nDFTCoeffs * 2 );

		swSingleIFFT.start( );
		// IFFT des linken Kanals berechnen und Ergebnis im Ausgabepuffer speichern
		if( bDestOutput )
		{
			// fftwf_execute(_right_ifft);
#	ifdef USE_FFTBROKER
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[1], pfRightOutput );
#	endif
#	ifdef USE_ITAFFT
			ifft.execute( vpfOutput_F[1], pfRightOutput );
#	endif
		}
		else
		{
			// Ausgabe in die tempor�ren Puffer, mittels des fftw3-Guru-Interface:
#	ifdef USE_FFTBROKER
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[1], pfRightTemp );
#	endif
#	ifdef USE_ITAFFT
			ifft.execute( vpfOutput_F[1], pfRightTemp );
#	endif
		}
		swSingleIFFT.stop( );

		// Berechnung abgeschlossen!
		sp = CStage::CSequencePointer::CDI_COMPLETED;
		return TASK_CALCULATION_COMPLETED;
	}

#else

	// Alter Code:

	if( sp == IFFT )
	{
		// Zeitquantum �berpr�fen
		bool bRun = false;

		double dEstimatedDuration = swCompleteIFFT.mean( );
		if( dEstimatedDuration <= 0 )
			// Kein bisheriger Messwert vorhanden -> Auf jedenfall rechnen...
			bRun = true;
		else if( ( ITAHPT_now( ) + toTimerTicks( dEstimatedDuration ) ) < iRDTSC_EndOfQuantum )
			bRun = true;

		if( !bInterruptable )
			bRun = true;


		if( !bRun )
			return TASK_QUANTUM_EXCEEDED;

		// Ergebnisse durch die L�nge teilen (siehe Theorie!)
		// Hinweis: Korrekt am 12.2.2005: Der Faktor war falsch. Es mu�
		//          hier 1/(2*iPartLength) lauten! Trotzdem nochmal die Theorie pr�fen
		fm_mul( vpfOutput_F[0], 1 / ( 2 * (float)iPartLength ), nDFTCoeffs * 2 );
		fm_mul( vpfOutput_F[1], 1 / ( 2 * (float)iPartLength ), nDFTCoeffs * 2 );

		swCompleteIFFT.start( );

		// IFFT berechnen und Ergebnis im Ausgabepuffer speichern
		if( bDestOutput )
		{
			// fftwf_execute(_left_ifft);
			// fftwf_execute(_right_ifft);
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[0], pfLeftOutput );
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[1], pfRightOutput );
		}
		else
		{
			// Ausgabe in die tempor�ren Puffer, mittels des fftw3-Guru-Interface:
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[0], pfLeftTemp );
			fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[1], pfRightTemp );
		}

		swCompleteIFFT.stop( );

		// TRACEN
		// TRACE_BUFFER("pfLeftOutput", pfLeftOutput, iPartLength, 3);
		// TRACE_BUFFER("pfRightOutput", pfRightOutput, iPartLength, 3);

		sp = CStage::CSequencePointer::CDI_COMPLETED;
		return TASK_CALCULATION_COMPLETED;
	}

#endif // WITH_SEPARATED_IFFT

	return TASK_QUANTUM_EXCEEDED;
}

void CStage::measure( )
{
	const int iCycles = 5;

	// FFT
	for( int i = 0; i < iCycles; i++ )
	{
		swSingleFFT.start( );
#ifdef USE_FFTBROKER
		fftwf_execute_dft_r2c( fft, pfInput, (fftwf_complex*)vpfInput_F[0] );
#endif
#ifdef USE_ITAFFT
		fft.execute( pfInput, vpfInput_F[0] );
#endif
		swSingleFFT.stop( );
	}

	// Faltung
	for( int i = 0; i < iCycles; i++ )
	{
		swSingleConvolution.start( );
		// Hinweis: Da noch keine IR zugeordnet ist, wird hier Input mit Input gefaltet!
		fm_cmul_x( vpfOutput_F[0], vpfInput_F[0], vpfInput_F[0], nDFTCoeffs );
		swSingleConvolution.stop( );
	}

	// IFFT
	for( int i = 0; i < iCycles; i++ )
	{
#ifdef WITH_SEPARATED_IFFT

		swSingleIFFT.start( );
#	ifdef USE_FFTBROKER
		fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[0], pfLeftOutput );
#	endif
#	ifdef USE_ITAFFT
		ifft.execute( vpfOutput_F[0], pfLeftOutput );
#	endif
		swSingleIFFT.stop( );

#else
		swCompleteIFFT.start( );
		fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[0], pfLeftOutput );
		fftwf_execute_dft_c2r( ifft, (fftwf_complex*)vpfOutput_F[1], pfRightOutput );
		swCompleteIFFT.stop( );
#endif
	}

	/*
	    printf("Stage %d^%d estimated runtimes: FFT = %0.3f, Conv = %0.3f, IFFT = %0.3f\n",
	#ifdef WITH_SEPARATED_IFFT
	        iPartLength, iMultiplicity, swSingleFFT.mean() * 1000000, swSingleConvolution.mean() * 1000000, swSingleIFFT.mean() * 1000000);
	#else
	        iPartLength, iMultiplicity, swSingleFFT.mean() * 1000000, swSingleConvolution.mean() * 1000000, swCompleteIFFT.mean() * 1000000);
	#endif
	*/
}


void CStage::printSomething( )
{
	// Profiling-Informationen ausgeben
#ifdef WITH_PROFILING
	char buf[1024];
	sprintf( buf, "\nStufe \"%s\", Partlength %d, Multiplicity %d:\n\n", sTag.c_str( ), iPartLength, iMultiplicity );
	// OutputDebugString(buf);
	printf( buf );
	sprintf( buf, "\t\tMin\tAvg\tMax\tVar\n" );
	// OutputDebugString(buf);
	printf( buf );
	sprintf( buf, "\tFFT\t%0.3f ns\t%0.3f ns\t%0.3f ns\t%0.8f\n", swFFT.minimum( ) * 1000000, swFFT.mean( ) * 1000000, swFFT.maximum( ) * 1000000, swFFT.variance( ) );
	// OutputDebugString(buf);
	printf( buf );

	sprintf( buf, "\tConv\t%0.3f ns\t%0.3f ns\t%0.3f ns\t%0.8f\n", swConvolution.minimum( ) * 1000000, swConvolution.mean( ) * 1000000,
	         swConvolution.maximum( ) * 1000000, swConvolution.variance( ) );
	// OutputDebugString(buf);
	printf( buf );

	sprintf( buf, "\tMix\t%0.3f ns\t%0.3f ns\t%0.3f ns\t%0.8f\n", swMix.minimum( ) * 1000000, swMix.mean( ) * 1000000, swMix.maximum( ) * 1000000, swMix.variance( ) );
	// OutputDebugString(buf);
	printf( buf );

	sprintf( buf, "\tIFFT\t%0.3f ns\t%0.3f ns\t%0.3f ns\t%0.8f\n\n", swIFFT.minimum( ) * 1000000, swIFFT.mean( ) * 1000000, swIFFT.maximum( ) * 1000000,
	         swIFFT.variance( ) );
	// OutputDebugString(buf);
	printf( buf );

	sprintf( buf, "\tTotal\t%0.3f ns\t%0.3f ns\t%0.3f ns\t%0.8f\n\n\n", swTotal.minimum( ) * 1000000, swTotal.mean( ) * 1000000, swTotal.maximum( ) * 1000000,
	         swTotal.variance( ) );
	// OutputDebugString(buf);
	printf( buf );
#endif

	/*
	    printf("\tSingle input FFT:  \t%0.3f us (Stddrv. %0.3f us)\n", swSingleFFT.mean() * 1000000, swSingleFFT.std_deviation() * 1000000);
	    printf("\tSingle convolution:\t%0.3f us (Stddrv. %0.3f us)\n", swSingleConvolution.mean() * 1000000, swSingleConvolution.std_deviation() * 1000000);
	#ifdef WITH_SEPARATED_IFFT
	    printf("\tSingle output IFFT:\t%0.3f us (Stddrv. %0.3f us)\n", swSingleIFFT.mean() * 1000000, swSingleIFFT.std_deviation() * 1000000);
	#else
	    printf("\tDual output IFFT:  \t%0.3f us (Stddrv. %0.3f us)\n", swCompleteIFFT.mean() * 1000000, swCompleteIFFT.std_deviation() * 1000000);
	#endif
	    printf("\n\n");
	*/

	/*
	    printf("Stage-%d^%d:\n\n", iPartLength, iMultiplicity);


	    ITAASCIITable T(3, 5);
	    T.setColumnTitle(0, "Operation");
	    T.setColumnTitle(1, "min [us]");
	    T.setColumnTitle(2, "avg [us]");
	    T.setColumnTitle(3, "max [us]");
	    T.setColumnTitle(4, "std [us]");

	    T.setContent(0, 0, "Single input FFT");
	    T.setContent(0, 1, DoubleToString(swSingleFFT.minimum() * 1000000, 3));
	    T.setContent(0, 2, DoubleToString(swSingleFFT.mean() * 1000000, 3));
	    T.setContent(0, 3, DoubleToString(swSingleFFT.maximum() * 1000000, 3));
	    T.setContent(0, 4, DoubleToString(swSingleFFT.std_deviation() * 1000000, 3));

	    T.setContent(1, 0, "Single convolution");
	    T.setContent(1, 1, DoubleToString(swSingleConvolution.minimum() * 1000000, 3));
	    T.setContent(1, 2, DoubleToString(swSingleConvolution.mean() * 1000000, 3));
	    T.setContent(1, 3, DoubleToString(swSingleConvolution.maximum() * 1000000, 3));
	    T.setContent(1, 4, DoubleToString(swSingleConvolution.std_deviation() * 1000000, 3));

	#ifdef WITH_SEPARATED_IFFT
	    T.setContent(2, 0, "Single output IFFT");
	    T.setContent(2, 1, DoubleToString(swSingleIFFT.minimum() * 1000000, 3));
	    T.setContent(2, 2, DoubleToString(swSingleIFFT.mean() * 1000000, 3));
	    T.setContent(2, 3, DoubleToString(swSingleIFFT.maximum() * 1000000, 3));
	    T.setContent(2, 4, DoubleToString(swSingleIFFT.std_deviation() * 1000000, 3));
	#else
	    T.setContent(2, 0, "Dual output IFFT");
	    T.setContent(2, 1, DoubleToString(swCompleteIFFT.minimum() * 1000000, 3));
	    T.setContent(2, 2, DoubleToString(swCompleteIFFT.mean() * 1000000, 3));
	    T.setContent(2, 3, DoubleToString(swCompleteIFFT.maximum() * 1000000, 3));
	    T.setContent(2, 4, DoubleToString(swCompleteIFFT.std_deviation() * 1000000, 3));
	#endif

	    std::cout << T.toString() << std::endl;
	*/
}

int CStage::updateTask( int iResult )
{
	switch( iResult )
	{
		case TASK_CALCULATION_COMPLETED:
		{
			pCurTask->iRDTSCEnd = ITAHPT_now( ); // Zeit der Fertigstellung vermerken
			pCurTask->SSP.iMain = CStage::CSequencePointer::COMPLETED;
			pCurTask            = nullptr; // Stufe wieder frei
		}
		break;


		case TASK_QUANTUM_EXCEEDED:
		{
			// Task bleibt auf der Stufe
		}
		break;

		case TASK_DEADLINE_EXCEEDED:
		{
			// Task verwerfen
			pCurTask->iRDTSCEnd = ITAHPT_now( ); // Zeit des Abbruchs vermerken
			pCurTask->SSP.iMain = CStage::CSequencePointer::COMPLETED;
			pCurTask            = nullptr; // Stufe wieder frei

			// Ausgabe Nullen?
		}
		break;

		default:
			assert( false );
	}

	return iResult;
}