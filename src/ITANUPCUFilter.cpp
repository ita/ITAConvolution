#include "ITANUPCUFilter.h"

#include "ITANUPCHelpers.h"
#include "ITANUPCUFilterPool.h"
#include "ITANUPCUtils.h"
#include "ITANUPartitioningScheme.h"

#include <ITAException.h>
#include <ITAFastMath.h>
#include <cassert>

CUFilter::CUFilter( const CUPartition* pPartition, UFilterPool* pParentPool )
    : m_pPartition( pPartition )
    , m_pParentPool( pParentPool )
    , m_pfDataBuf( nullptr )
    , m_iDataBufSize( 0 )
{
	assert( pPartition );
	assert( pPartition->iPartLength * pPartition->iMultiplicity > 0 );

	m_oState.m_pParent = this;

	/*
	   Erkl�rung: Berechnung der Gesamtl�nge des Datenpuffers

	   Ein Teil von k Filterkoeffizienten korrespondiert mit einer
	   zyklischen Faltung mit einer Impulsantwort der L�nge 2k
	   Filterkoffizienten (k Nullen + k Filterkoeffizienten).
	   Jeder Teil der L�nge 2*k hat im Frequenzbereich floor(2*k/2)+1 = k+1
	   komplexe Fourierkoeffizienten. Das sind 2k+2 float-Werte
	   (je einer Real- und Imagin�rteil). k ist eine 2^n-Potenz.
	   Ein float hat 4 Byte. Also ist 4*(2*k+4) = 8k+16 sicher 16-Byte
	   ausgerichtet f�r SIMD-Instruktionen.

	*/

	// fwe: Nun doppelte Gr��e, weil 2 Kan�le
	m_iDataBufSize += 2 * m_pPartition->iMultiplicity * ( 2 * m_pPartition->iPartLength + 4 );

	vpfPartSpecData.resize( 2 * m_pPartition->iMultiplicity, nullptr );
	vpfPartSpecBuf.resize( 2 * m_pPartition->iMultiplicity, nullptr );

	// Alle Filterteile in einem zusammenh�ngenden Puffer ablegen
	m_pfDataBuf = (float*)fm_falloc( m_iDataBufSize, true );

	// Zugriffszeiger bestimmen und merken
	int k = 0;
	int o = 0;
	for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
	{
		// Linker und rechter Kanal
		vpfPartSpecBuf[k++] = m_pfDataBuf + o;
		assert( ITANUPCIsAligned16Byte( m_pfDataBuf + o ) );
		o += ( 2 * m_pPartition->iPartLength + 4 );
		vpfPartSpecBuf[k++] = m_pfDataBuf + o;
		o += ( 2 * m_pPartition->iPartLength + 4 );
		assert( ITANUPCIsAligned16Byte( m_pfDataBuf + o ) );
	}

	// FFT planen
	m_fft.plan( ITAFFT::FFT_R2C, 2 * m_pPartition->iPartLength, vpfPartSpecBuf[0], vpfPartSpecBuf[0] );
}

CUFilter::~CUFilter( )
{
	fm_free( m_pfDataBuf );

	// DEBUG:
	/*	char buf[2048];
	    sprintf(buf, "load f�r Filterkomponente (0x%08Xh): min %0.3f us, avg %0.3f us\n", this, sw.minimum()*1000000, sw.mean()*1000000);
	    OutputDebugString(buf); */
}

bool CUFilter::IsInUse( ) const
{
	return m_oState.isInUse( );
}

void CUFilter::Release( )
{
	assert( m_pParentPool );
	m_pParentPool->ReleaseFilter( this );
}

void CUFilter::Load( const float* pfLeftData, int iLeftDataLength, const float* pfRightData, int iRightDataLength )
{
	// Keine Reentrance
	ITACriticalSectionLock oLock( m_csReentrance );

	assert( !IsInUse( ) );
	if( IsInUse( ) )
		ITA_EXCEPT1( UNKNOWN, "Internal error E4283" );

	/*	DEBUG:
	    char buf[255];
	    sprintf(buf, "load-%08d-%02d-left.wav", this, j);
	    store(buf, (float*) pfLeftData, uiLeftDataLength);
	    sprintf(buf, "load-%08d-%02d-right.wav", this, j);
	    store(buf, (float*) pfLeftData, uiLeftDataLength);
	    store(buf, (float*) pfRightData, uiRightDataLength);
	*/
	//	sw.start();

	// Datenpuffer mit Nullen initialisieren
	// TODO: Performance kann verbessert werden, wenn nach Bedarf initialisiert wird
	fm_zero( m_pfDataBuf, m_iDataBufSize );

	/*
	 *  Zeitbereichsdaten in den Datenbereich kopieren
	 *
	 *  Hinweis: F�r die Blockfaltung mu� der jeder Teil der IR links mit der
	 *           gleichen Anzahl Nullen erg�nzt werden, wie er lang ist.
	 */

	int n;

	// Linker Kanal:
	if( pfLeftData )
	{
		n = 0; // <- Hier die zugeteilte L�nge
		for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
		{
			// IR-Daten kopieren (falls noch welche �brig sind)
			if( n < iLeftDataLength )
			{
				int r = iLeftDataLength - n;
				r     = ( std::min )( r, m_pPartition->iPartLength );

				if( ITANUPCFCheckZero( pfLeftData + n, r ) )
				{
					// Filterdaten sind nur Nullen => Nullzeiger setzen
					vpfPartSpecData[2 * i] = nullptr;
				}
				else
				{
					vpfPartSpecData[2 * i] = vpfPartSpecBuf[2 * i];
					fm_copy( vpfPartSpecData[2 * i] + m_pPartition->iPartLength, pfLeftData + n, r );
				}

				n += r; // Positionszeiger inkrementieren
			}
			else
			{
				// Keine Filterkoeffizienten => Nullzeiger setzen
				vpfPartSpecData[2 * i] = nullptr;
			}
		}
	}
	else
	{
		// Keine Filterkoeffizienten => Nullzeiger setzen
		for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
			vpfPartSpecData[2 * i] = nullptr;
	}

	// Rechter Kanal
	if( pfRightData )
	{
		n = 0; // <- Hier die zugeteilte L�nge
		for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
		{
			// IR-Daten kopieren (falls noch welche �brig sind)
			if( n < iRightDataLength )
			{
				int r = iRightDataLength - n;
				r     = ( std::min )( r, m_pPartition->iPartLength );

				if( ITANUPCFCheckZero( pfRightData + n, r ) )
				{
					// Filterdaten sind nur Nullen => Nullzeiger setzen
					vpfPartSpecData[2 * i + 1] = nullptr;
				}
				else
				{
					vpfPartSpecData[2 * i + 1] = vpfPartSpecBuf[2 * i + 1];
					fm_copy( vpfPartSpecData[2 * i + 1] + m_pPartition->iPartLength, pfRightData + n, r );
				}

				n += r; // Positionszeiger inkrementieren
			}
			else
			{
				// Keine Filterkoeffizienten => Nullzeiger setzen
				vpfPartSpecData[2 * i + 1] = nullptr;
			}
		}
	}
	else
	{
		// Keine Filterkoeffizienten => Nullzeiger setzen
		for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
			vpfPartSpecData[2 * i + 1] = nullptr;
	}

	/* DEBUG:
	std::string fname = std::string("FCI_Data_Spatial_") + IntToString(i) + ".wav";
	store((char*) fname.c_str(), m_pfDataBuf, _uiPartSpecSize);
	TRACE_BUFFER("PIR_Data", m_pfDataBuf, n);
	*/

	for( int i = 0; i < m_pPartition->iMultiplicity; i++ )
	{
		// FFTs durchf�hren (linker und rechter Kanal)
		if( vpfPartSpecData[2 * i] )
			m_fft.execute( vpfPartSpecData[2 * i], vpfPartSpecData[2 * i] );
		if( vpfPartSpecData[2 * i + 1] )
			m_fft.execute( vpfPartSpecData[2 * i + 1], vpfPartSpecData[2 * i + 1] );
	}

	// fname = std::string("FCI_Data_Frequency_") + IntToString(i) + ".wav";
	// store((char*) fname.c_str(), m_pfDataBuf, _uiPartSpecSize);

	/*	double t = sw.stop();
	    char buf[255];
	    sprintf(buf, "load (0x%08Xh): took %0.12f s\n", t);
	    OutputDebugString(buf); */
}

void CUFilter::Zero( )
{
	Load( nullptr, 0, nullptr, 0 );
}

void CUFilter::Identity( )
{
	const float one[1] = { 1 };
	Load( one, 1, one, 1 );
}

// --= State class =--

CUFilter::State::State( ) : m_pParent( nullptr )
{
	m_oState.iPrepRefCount = 0;
	m_oState.iUseRefCount  = 0;
}

bool CUFilter::State::isInUse( ) const
{
	// Trick: 32-Bit als int auswerten. Wert > 0 dann in Benutzung...
	return atomic_read_int( (volatile int*)&m_oState ) != 0;
}

void CUFilter::State::addPrep( )
{
	modifyState( 1, 0 );
}

void CUFilter::State::removePrep( )
{
	modifyState( -1, 0 );
}

void CUFilter::State::xchangePrep2Use( )
{
	modifyState( -1, +1 );
}

void CUFilter::State::removeUse( )
{
	modifyState( 0, -1 );
}

void CUFilter::State::modifyState( int16_t iPrepDelta, int16_t iUseDelta )
{
	StateStruct oGivenState, oNewState;
	do
	{
		// Read
		atomic_read32( &m_oState, &oGivenState );

		// Modify
		oNewState.iPrepRefCount = oGivenState.iPrepRefCount + iPrepDelta;
		oNewState.iUseRefCount  = oGivenState.iUseRefCount + iUseDelta;

		// Try-write
	} while( !atomic_cas32( &m_oState, &oGivenState, &oNewState ) );

	/*
	DEBUG_PRINTF("[UFilter 0x%08Xh] Changing state (%d, %d) => (%d, %d)\n",
	             m_pParent, oGivenState.iPrepRefCount, oGivenState.iUseRefCount,
	             oNewState.iPrepRefCount, oNewState.iUseRefCount);
	*/

	// Post-condition: Valid states
	assert( oNewState.iPrepRefCount >= 0 );
	assert( oNewState.iUseRefCount >= 0 );
}
