#include "ITANUPCOutputBuffer.h"

#include <ITAFastMath.h>
#include <ITANUPConvolution.h>
#include <cassert>

COutputBuffer* COutputBuffer::pInstance = 0;

COutputBuffer::COutputBuffer( unsigned int uiChannels, unsigned int uiBlocklength, unsigned int uiSize )
{
	this->uiChannels    = uiChannels;
	this->uiBlocklength = uiBlocklength;
	this->uiSize        = uiSize;

	uiCursor = 0;
	ppfData  = 0;

	ppfData = new float*[uiChannels];
	memset( ppfData, 0, uiChannels * sizeof( float* ) );
	for( unsigned int c = 0; c < uiChannels; c++ )
		ppfData[c] = fm_falloc( uiSize, true );

	pInstance = this;
}

COutputBuffer::~COutputBuffer( )
{
	tidyup( );
	pInstance = 0;
}

void COutputBuffer::Reset( )
{
	// TODO: Ist das Schreiben von Nullen wirklich notwendig?
	uiCursor = 0;
	for( unsigned int c = 0; c < uiChannels; c++ )
		fm_zero( ppfData[c], uiSize );
}

void COutputBuffer::Get( float* pfDest, unsigned int uiChannel, float fGain )
{
	csData.enter( );
#ifdef _OUTPUTBUFFER_DEBUG_MESSAGES
	printf( "[OutputBuffer::get] Cursor = %d\n", uiCursor );
#endif
	fm_copy( pfDest, ppfData[uiChannel] + uiCursor, uiBlocklength );
	if( fGain != 1.0 )
		fm_mul( pfDest, fGain, uiBlocklength );

	fm_zero( ppfData[uiChannel] + uiCursor, uiBlocklength );
	csData.leave( );
}

void COutputBuffer::PutAtCursor( const float* pfSource, unsigned int uiChannel, unsigned int uiLength )
{
	Put( pfSource, uiChannel, uiCursor, uiLength );
}

void COutputBuffer::Put( const float* pfSource, unsigned int uiChannel, unsigned int uiOffset, unsigned int uiLength )
{
	csData.enter( );
#ifdef _OUTPUTBUFFER_DEBUG_MESSAGES
	printf( "\n\n[OutputBuffer::put] Channel = %d, Offset = %d, Length = %d\n\n\n", uiChannel, uiOffset, uiLength );
	TRACE_BUFFER( "Data:", (float*)pfSource, uiLength );
#endif
	// Wichtig: Der �bergebene Offset kann durch die Addition der Clearance
	//          �ber das Ende des Puffers hinausragen. Daher muss er wieder
	//          in den Puffer hineingewrappt werden (modulo):
	uiOffset %= uiSize;
	assert( uiOffset < uiSize );

	unsigned int r = uiSize - uiOffset;
	if( uiLength <= r )
		// Addieren in einem Schritt
		fm_add( ppfData[uiChannel] + uiOffset, pfSource, uiLength );
	// for (unsigned int i=0; i<uiLength; i++)
	//	ppfData[uiChannel][uiOffset+i] = pfSource[i];
	else
	{
		// Addieren in zwei Schritten
		fm_add( ppfData[uiChannel] + uiOffset, pfSource, r );
		fm_add( ppfData[uiChannel], pfSource + r, uiLength - r );
		// for (unsigned int i=0; i<r; i++)
		//	ppfData[uiChannel][uiOffset+i] = pfSource[i];
		// for (i=0; i<uiLength-r; i++)
		//	ppfData[uiChannel][i] = pfSource[i+r];
	}

	csData.leave( );
}

void COutputBuffer::IncrementCursor( )
{
	uiCursor += uiBlocklength;
	uiCursor %= uiSize;
#ifdef _OUTPUTBUFFER_DEBUG_MESSAGES
	printf( "[OutputBuffer::incrementCursor] Cursor = %d\n", uiCursor );
#endif
}

void COutputBuffer::tidyup( )
{
	if( ppfData )
		for( unsigned int c = 0; c < uiChannels; c++ )
			fm_free( ppfData[c] );
	delete[] ppfData;
	ppfData = 0;
}
