#include <ITAException.h>
#include <ITAFFT.h>
#include <ITAFade.h>
#include <ITAFastMath.h>
#include <ITAFunctors.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <algorithm>
#include <cassert>
#include <cstring>

ITAUPConvolution::ITAUPConvolution( )
    : m_iBlocklength( 0 )
    , m_iMaxFilterlength( 0 )
    , m_iExchangeFadingFunction( ITABase::FadingFunction::SWITCH )
    , m_iCrossfadeLength( 0 )
    , m_pfTimeInputBuffer( NULL )
    , m_pfTimeOutputBuffer1( NULL )
    , m_pfTimeOutputBuffer2( NULL )
    , m_pfFreqAuxBuffer( NULL )
    , m_pfFreqMixdownBuffer( NULL )
    , m_pCurrentFilter( NULL )
    , m_pOwnPool( NULL )
    , m_pCurrentPool( NULL )
    , m_pFFT( NULL )
    , m_pIFFT( NULL )
{
}

ITAUPConvolution::ITAUPConvolution( const int iBlocklength, const int iMaxFilterlength, ITAUPFilterPool* pFilterPool )
    : m_iBlocklength( iBlocklength )
    , m_iMaxFilterlength( iMaxFilterlength )
    , m_iExchangeFadingFunction( ITABase::FadingFunction::SWITCH )
    , m_iCrossfadeLength( 0 )
    , m_pfTimeInputBuffer( NULL )
    , m_pfTimeOutputBuffer1( NULL )
    , m_pfTimeOutputBuffer2( NULL )
    , m_pfFreqAuxBuffer( NULL )
    , m_pfFreqMixdownBuffer( NULL )
    , m_pCurrentFilter( NULL )
    , m_pOwnPool( NULL )
    , m_pCurrentPool( pFilterPool )
    , m_pFFT( NULL )
    , m_pIFFT( NULL )
{
	Init( iBlocklength, iMaxFilterlength );
}

ITAUPConvolution::~ITAUPConvolution( )
{
	// Aktuelles Filter freigeben
	if( (ITAUPFilter*)m_pCurrentFilter )
		( *m_pCurrentFilter ).m_oState.RemoveUse( );

	// Nächste Filter in der Queue freigeben
	FilterUpdate oUpdate;
	while( m_qExchangeFilters.try_pop( oUpdate ) )
		if( oUpdate.pFilter )
			oUpdate.pFilter->m_oState.RemovePrep( );

	// Resourcen freigeben
	std::for_each( m_vpfFreqDelayLine.begin( ), m_vpfFreqDelayLine.end( ), fm_free );
	fm_free( m_pfTimeInputBuffer );
	fm_free( m_pfTimeOutputBuffer1 );
	fm_free( m_pfTimeOutputBuffer2 );
	fm_free( m_pfFreqAuxBuffer );
	fm_free( m_pfFreqMixdownBuffer );

	delete m_pOwnPool;
	delete m_pFFT;
	delete m_pIFFT;
}

void ITAUPConvolution::Init( const int iBlocklength, const int iMaxFilterlength )
{
	/*
	 *  Bemerkungen:
	 *
	 *  1. Die Grääe des Filterkoeffizienten-Array pfFilterData muss
	 *    nicht zwingend ein Vielfaches der Puffergrääe sein
	 *
	 *  2. Ferner kann seine Grääe kleiner als iBlocklength sein!
	 */

	// Anzahl der Filterteile bestimmen
	int n = iMaxFilterlength / iBlocklength;
	if( ( iMaxFilterlength % iBlocklength ) > 0 )
		n++;

	m_iBlocklength     = iBlocklength;
	m_iMaxFilterlength = iBlocklength * n;

	// Leere Impulsantwort? Dann haben wir fertig!
	if( n == 0 )
		return;

	/*
	 *  Speicher fär die DFT-Koeffizienten der Filterteile allozieren
	 *  und die Filterteile in den Frequenzbereich transformieren.
	 *  Blockfaltungsalgoritmus bedeutet: Linke Seiten der Filterteile
	 *  mässen mit Nullsamples gepadded werden!
	 */

	m_iFreqCoeffs         = iBlocklength + 1;
	m_pfTimeInputBuffer   = fm_falloc( 2 * iBlocklength, true );
	m_pfTimeOutputBuffer1 = fm_falloc( 2 * iBlocklength, true );
	m_pfTimeOutputBuffer2 = fm_falloc( 2 * iBlocklength, true );

	for( int i = 0; i < n; i++ )
		m_vpfFreqDelayLine.push_back( fm_falloc( 2 * m_iFreqCoeffs, true ) );

	// Frequenzbereichs-Mischpuffer allozieren und Rücktransformation planen
	m_pfFreqAuxBuffer     = fm_falloc( m_iFreqCoeffs * 2, false );
	m_pfFreqMixdownBuffer = fm_falloc( m_iFreqCoeffs * 2, false );

	// Transformationen planen
	m_pFFT  = new ITAFFT( ITAFFT::FFT_R2C, 2 * iBlocklength, m_pfTimeInputBuffer, m_vpfFreqDelayLine[0] );
	m_pIFFT = new ITAFFT( ITAFFT::IFFT_C2R, 2 * iBlocklength, m_pfFreqMixdownBuffer, m_pfTimeOutputBuffer1 );

	// Eigenen Filterpool erzeugen
	m_pOwnPool = new ITAUPFilterPool( m_iBlocklength, m_iMaxFilterlength, 0 );
	if( !m_pCurrentPool )
		m_pCurrentPool = m_pOwnPool;

	m_fCurrentGain = 1;
	m_fNewGain     = 1;
}

int ITAUPConvolution::GetBlocklength( ) const
{
	return m_iBlocklength;
}

int ITAUPConvolution::GetMaxFilterlength( ) const
{
	return m_iMaxFilterlength;
}

const ITAUPTrigger* ITAUPConvolution::GetFilterExchangeTrigger( ) const
{
	return m_oTriggerWatch.GetWatchedTrigger( );
}

void ITAUPConvolution::SetFilterExchangeTrigger( const ITAUPTrigger* pTrigger )
{
	m_oTriggerWatch.SetWatchedTrigger( pTrigger );
}

int ITAUPConvolution::GetFilterExchangeFadingFunction( )
{
	return m_iExchangeFadingFunction;
}

void ITAUPConvolution::SetFilterExchangeFadingFunction( const int iMode )
{
	m_iExchangeFadingFunction = iMode;
}

int ITAUPConvolution::GetFilterCrossfadeLength( )
{
	return m_iCrossfadeLength;
}

void ITAUPConvolution::SetFilterCrossfadeLength( const int iLength )
{
	m_iCrossfadeLength = iLength;
}

float ITAUPConvolution::GetGain( ) const
{
	return m_fCurrentGain;
}

void ITAUPConvolution::SetGain( const float fGain, const bool bSetImmediately )
{
	m_fNewGain = fGain;
	if( bSetImmediately )
		m_fCurrentGain = fGain;
}

ITAUPFilterPool* ITAUPConvolution::GetFilterPool( ) const
{
	m_csPool.enter( );
	ITAUPFilterPool* pResult = m_pCurrentPool;
	m_csPool.leave( );
	return pResult;
}

void ITAUPConvolution::SetFilterPool( ITAUPFilterPool* pFilterPool )
{
	if( m_iBlocklength == 0 )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Convolver instance not initialized yet" );

	// Parameter prüfen
	if( ( pFilterPool->GetBlocklength( ) != m_iBlocklength ) || ( pFilterPool->GetMaxFilterlength( ) != m_iMaxFilterlength ) )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Pool does not meet the properties of the convolver" );

	m_csPool.enter( );
	m_pCurrentPool = ( pFilterPool ? pFilterPool : m_pOwnPool );
	m_csPool.leave( );
}

ITAUPFilter* ITAUPConvolution::RequestFilter( )
{
	m_csPool.enter( );
	ITAUPFilter* pFilter = m_pCurrentPool->RequestFilter( );
	m_csPool.leave( );
	return pFilter;
}

void ITAUPConvolution::ReleaseFilter( ITAUPFilter* pFilter )
{
	m_csPool.enter( );
	m_pCurrentPool->ReleaseFilter( pFilter );
	m_csPool.leave( );
}

ITAUPFilter* ITAUPConvolution::GetActiveFilter( )
{
	return m_pCurrentFilter;
}

void ITAUPConvolution::ExchangeFilter( ITAUPFilter* pNewFilter, const int iExchangeFadingFunction, const int iCrossfadeLength )
{
	// DEBUG_PRINTF("[ITAUPConvolution 0x%08Xh] Exchanging to filter 0x%08Xh\n", this, pNewFilter);

	// Platzierung des Filter vermerken
	if( pNewFilter )
		pNewFilter->m_oState.AddPrep( );

	// Nächstes Filter in die Austausch-Queue einfügen
	FilterUpdate oUpdate;
	oUpdate.pFilter          = pNewFilter;
	oUpdate.iExchangeMode    = ( iExchangeFadingFunction == ITABase::FadingFunction::SWITCH ? (int)m_iExchangeFadingFunction : iExchangeFadingFunction );
	oUpdate.iCrossfadeLength = ( iCrossfadeLength == ITABase::FadingFunction::SWITCH ? (int)m_iCrossfadeLength : iCrossfadeLength );
	m_qExchangeFilters.push( oUpdate );
}

void ITAUPConvolution::clear( )
{
	fm_zero( m_pfTimeInputBuffer, 2 * m_iBlocklength );
	fm_zero( m_pfTimeOutputBuffer1, 2 * m_iBlocklength );
	fm_zero( m_pfTimeOutputBuffer2, 2 * m_iBlocklength );

	for( size_t i = 0; i < m_vpfFreqDelayLine.size( ); i++ )
		fm_zero( m_vpfFreqDelayLine[i], 2 * m_iFreqCoeffs );

	fm_zero( m_pfFreqAuxBuffer, 2 * m_iFreqCoeffs );
	fm_zero( m_pfFreqMixdownBuffer, 2 * m_iFreqCoeffs );

	if( (ITAUPFilter*)m_pCurrentFilter )
	{
		m_pCurrentPool->ReleaseFilter( m_pCurrentFilter );
		m_pCurrentFilter = NULL;
	}
}

void ITAUPConvolution::Process( const float* pfInputData, float* pfOutputData, const int iOutputMode )
{
	Process( pfInputData, m_iBlocklength, pfOutputData, m_iBlocklength, iOutputMode );
}

void ITAUPConvolution::Process( const float* pfInputData, const int iInputLength, float* pfOutputData, const int iOutputLength, const int iOutputMode )
{
	assert( iInputLength >= 0 );
	assert( iOutputLength >= 0 );

	// Rechte Seite des Hilfspuffers in die linke kopieren.
	// Danach die neuen Eingangsdaten in den rechten Hälfte plazieren.

	memcpy( m_pfTimeInputBuffer, m_pfTimeInputBuffer + m_iBlocklength, m_iBlocklength * sizeof( float ) );

	if( pfInputData > 0 && pfInputData != nullptr )
	{
		if( iInputLength >= m_iBlocklength )
		{
			// Eingangsdaten am Stück kopieren
			memcpy( m_pfTimeInputBuffer + m_iBlocklength, pfInputData, m_iBlocklength * sizeof( float ) );
		}
		else
		{
			// Eingabedaten kopieren und den Rest mit Nullen füllen
			memcpy( m_pfTimeInputBuffer + m_iBlocklength, pfInputData, iInputLength * sizeof( float ) );
			memset( m_pfTimeInputBuffer + m_iBlocklength + iInputLength, 0, ( m_iBlocklength - iInputLength ) * sizeof( float ) );
		}
	}
	else
	{
		// Nullen setzen.
		fm_zero( m_pfTimeInputBuffer + m_iBlocklength, m_iBlocklength );
	}

	// Elemente der Frequency-domain delay line (FDL) weiterschieben
	int n    = (int)m_vpfFreqDelayLine.size( );
	float* X = m_vpfFreqDelayLine.back( );
	for( int i = n - 1; i > 0; i-- )
		m_vpfFreqDelayLine[i] = m_vpfFreqDelayLine[i - 1];
	m_vpfFreqDelayLine[0] = X;

	// Eingangsdaten in den Frequenzbereich transformieren
	m_pFFT->execute( m_pfTimeInputBuffer, X );

	// Globale Austausch-Parameter
	int iFadingModeInternal = m_iExchangeFadingFunction; // AUTO = -1
	int iCrossfadeLength    = m_iCrossfadeLength;

	// Werte holen [atomar]
	FilterUpdate oUpdate;
	ITAUPFilter* C    = m_pCurrentFilter;
	ITAUPFilter* N    = NULL;
	ITAUPFilter* Z    = NULL;
	bool bNewFilters  = false;
	bool bForceSwitch = false;

	// Test auf neue Filter nur dann durchführen, wenn der Trigger signalisiert wurde
	// oder gar kein Trigger zuordnet ist (dies wird in der TriggerWatch realisiert).
	if( m_oTriggerWatch.Fire( ) )
	{
		// Alle bis auf das letzte Filter in der Austausch-Queue freigeben
		while( m_qExchangeFilters.try_pop( oUpdate ) )
		{
			bNewFilters = true;
			Z           = oUpdate.pFilter;

			// Lokale Parameter übernehmen
			iFadingModeInternal = oUpdate.iExchangeMode;
			iCrossfadeLength    = oUpdate.iCrossfadeLength;
			// Wichtig: Ein Switching bei den Updates ist dominant
			bForceSwitch |= ( iFadingModeInternal == ITABase::FadingFunction::SWITCH );

			// DEBUG_PRINTF("[ITAUPConvolution 0x%08Xh] Popped next filter 0x%08Xh\n", this, Z);
			if( N )
				N->m_oState.RemovePrep( );
			N = Z;
		}
		// Hinweis: N enthält jetzt das letzte Element der Queue (kann auch NULL sein)
	}

	// Keine Änderung
	if( !bNewFilters )
		N = C;

	if( bForceSwitch )
	{
		iFadingModeInternal = ITABase::FadingFunction::SWITCH;
		iCrossfadeLength    = 0;
	}

	// Im Fall von Umschalten das alte Filter sofort freigeben (wird nicht mehr benötigt)
	// Dies gilt auch falls keine Ausgabedaten ausgegeben werden sollen.
	if( ( iFadingModeInternal == ITABase::FadingFunction::SWITCH ) || ( !pfOutputData ) )
	{
		if( C != N )
		{
			if( C )
				C->m_oState.RemoveUse( );
			if( N )
				N->m_oState.ExchangePrep2Use( );
			C                = N;
			m_pCurrentFilter = N;
		}
	}

	if( !pfOutputData )
	{
		m_fCurrentGain = (float)m_fNewGain;
		return;
	}

	int iOutputCount = ( std::min )( m_iBlocklength, iOutputLength );
	if( !pfOutputData )
		iOutputCount = 0; // Legacy support

	/*
	 *  Austausch-Verfahrensweisen
	 *
	 *  Generelles: Bei diesem Code wurde mehr wert auf übersichlichtlich gelegt.
	 *              Deshalb werden die nur selten auftretenden Fälle leerer Filter
	 *              nicht optimiert. Grundsätzlich werden zwei Stränge berechnet:
	 *              Einer für das alte Filter und einer für das neue. Im Falle von
	 *              hartem Unschalten wird nur ein Strang bestimmt.
	 */

	if( C == N )
	{
		// Fall:    Keine Austausch des Filters oder Austausch durch hartes Umschalten (siehe oben)
		// Aufgabe: Nur einen Strang falten. Keine Überblendung.

		if( C != NULL )
		{
			// Gleiches Filter wie bisher. Normal falten.
			if( C->m_iNumEffectiveFilterParts > 0 )
			{
				for( int i = 0; i < C->m_iNumEffectiveFilterParts; i++ )
				{
					if( i == 0 )
					{
						// Ergebnis direkt in den Misch-Puffer speichern
						fm_cmul_x( m_pfFreqMixdownBuffer, m_vpfFreqDelayLine[i], C->m_vpfFreqData[i], m_iFreqCoeffs );
					}
					else
					{
						// Ergebnis zwischenpuffern, danach auf den Misch-Puffer aufaddieren
						fm_cmul_x( m_pfFreqAuxBuffer, m_vpfFreqDelayLine[i], C->m_vpfFreqData[i], m_iFreqCoeffs );
						fm_add( m_pfFreqMixdownBuffer, m_pfFreqAuxBuffer, m_iFreqCoeffs * 2 );
					}
				}

				// Rücktransformation durchführen
				m_pIFFT->execute( m_pfFreqMixdownBuffer, m_pfTimeOutputBuffer1 );

				// Verstärkungs-Envelope anwenden
				CopyOutputApplyGain1( pfOutputData, m_pfTimeOutputBuffer1, iOutputCount, iOutputMode );

				return;
			}
			else
			{
				// Keine effektiven Filterteile => Ausgabe ist Null
				if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
					memset( pfOutputData, 0, iOutputCount * sizeof( float ) );
				return;
			}
		}
		else
		{
			// Abkürzung: Leeres Filter -> Keine Ausgangsdaten
			if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
			{
				for( int i = 0; i < iOutputCount; i++ )
					pfOutputData[i] = 0;
			}

			m_fCurrentGain = (float)m_fNewGain;

			return;
		}
	}
	else
	{
		// Fall:    Austausch des Filters. Kein Umschalten. Eine Impulsantwort kann leer sein.
		// Aufgabe: Nur einen Strang falten. Keine Überblendung.

		if( C != NULL )
		{
			if( C->m_iNumEffectiveFilterParts > 0 )
			{
				// Falten mit aktuellen Filter
				for( int i = 0; i < C->m_iNumEffectiveFilterParts; i++ )
				{
					if( i == 0 )
					{
						// Ergebnis direkt in den Misch-Puffer speichern
						fm_cmul_x( m_pfFreqMixdownBuffer, m_vpfFreqDelayLine[i], C->m_vpfFreqData[i], m_iFreqCoeffs );
					}
					else
					{
						// Ergebnis zwischenpuffern, danach auf den Misch-Puffer aufaddieren
						fm_cmul_x( m_pfFreqAuxBuffer, m_vpfFreqDelayLine[i], C->m_vpfFreqData[i], m_iFreqCoeffs );
						fm_add( m_pfFreqMixdownBuffer, m_pfFreqAuxBuffer, m_iFreqCoeffs * 2 );
					}
				}

				m_pIFFT->execute( m_pfFreqMixdownBuffer, m_pfTimeOutputBuffer1 );

				const int iFadingSign = ITABase::FadingSign::FADE_OUT;
				const int iFadingFunction =
				    ( iFadingModeInternal == ITABase::FadingFunction::LINEAR ? ITABase::FadingFunction::LINEAR : ITABase::FadingFunction::COSINE_SQUARE );
				Fade( m_pfTimeOutputBuffer1, iCrossfadeLength, iFadingSign, iFadingFunction, 0, iCrossfadeLength );
			}
			else
			{
				// Keine effektiven Filterteile => Zwischenergebnis ist Null
				fm_zero( m_pfTimeOutputBuffer1, m_iBlocklength );
			}

			// Altes Filter freigeben
			C->m_oState.RemoveUse( );
		}
		else
		{
			fm_zero( m_pfTimeOutputBuffer1, m_iBlocklength );
		}

		// Falten mit dem neuen Filter
		if( N != NULL )
		{
			// Neues Filter sperren
			N->m_oState.ExchangePrep2Use( );

			if( N->m_iNumEffectiveFilterParts > 0 )
			{
				for( int i = 0; i < N->m_iNumEffectiveFilterParts; i++ )
				{
					if( i == 0 )
					{
						// Ergebnis direkt in den Misch-Puffer speichern
						fm_cmul_x( m_pfFreqMixdownBuffer, m_vpfFreqDelayLine[i], N->m_vpfFreqData[i], m_iFreqCoeffs );
					}
					else
					{
						// Ergebnis zwischenpuffern, danach auf den Misch-Puffer aufaddieren
						fm_cmul_x( m_pfFreqAuxBuffer, m_vpfFreqDelayLine[i], N->m_vpfFreqData[i], m_iFreqCoeffs );
						fm_add( m_pfFreqMixdownBuffer, m_pfFreqAuxBuffer, m_iFreqCoeffs * 2 );
					}
				}
				m_pIFFT->execute( m_pfFreqMixdownBuffer, m_pfTimeOutputBuffer2 );

				const int iFadingSign = ITABase::FadingSign::FADE_IN;
				const int iFadingFunction =
				    ( iFadingModeInternal == ITABase::FadingFunction::LINEAR ? ITABase::FadingFunction::LINEAR : ITABase::FadingFunction::COSINE_SQUARE );
				Fade( m_pfTimeOutputBuffer2, iCrossfadeLength, iFadingSign, iFadingFunction, 0, iCrossfadeLength );
			}
			else
			{
				// Keine effektiven Filterteile => Zwischenergebnis ist Null
				fm_zero( m_pfTimeOutputBuffer2, m_iBlocklength );
			}
		}
		else
		{
			fm_zero( m_pfTimeOutputBuffer2, m_iBlocklength );
		}

		// Ausgabe normalisieren und im Ausgabepuffer zusammenkopieren
		// Linke Seite von m_pfTimeOutputBuffer enthält die Nutzdaten.
		CopyOutputApplyGain2( pfOutputData, m_pfTimeOutputBuffer1, m_pfTimeOutputBuffer2, iOutputCount, iCrossfadeLength, iOutputMode );

		// Filteraustausch vermerken:
		m_pCurrentFilter = N;

		return;
	}
}

// Transferriert Samples vom den internen Puffern in die Ausgabe und wendet dabei die Gains an.
// Div ist ein Skalierungsfaktor (1/x) für die unnormalisierte inverse FFT.
void ITAUPConvolution::CopyOutputApplyGain1( float* pfDest, const float* pfSrc, const int iOutputLength, const int iOutputMode )
{
	// Atomares lesen in lokale Variablen
	float fGain1 = m_fCurrentGain;
	float fGain2 = m_fNewGain;

	if( ( fGain1 == fGain2 ) && ( fGain1 != 1 ) )
	{
		// Keine Änderung und neues Gain nicht 1 => Konstanter Gain
		if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
		{
			// Ausgabe Überschreiben
			for( int i = 0; i < iOutputLength; i++ )
				pfDest[i] = pfSrc[i] * fGain1;
		}
		else
		{
			// Ausgabe Aufaddieren
			for( int i = 0; i < iOutputLength; i++ )
				pfDest[i] += pfSrc[i] * fGain1;
		}
	}
	else
	{
		// Verstärkung ändert sich => Linear interpolierter Gain
		float c = ( fGain2 - fGain1 ) / (float)m_iBlocklength;

		if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
		{
			// Ausgabe Überschreiben
			for( int i = 0; i < iOutputLength; i++ )
				pfDest[i] = pfSrc[i] * ( fGain1 + i * c );
		}
		else
		{
			// Ausgabe Aufaddieren
			for( int i = 0; i < iOutputLength; i++ )
				pfDest[i] += pfSrc[i] * ( fGain1 + i * c );
		}

		m_fCurrentGain = fGain2;
	}
}

// Wie oben, nur mit zwei Quellpuffern die direkt addiert werden.
void ITAUPConvolution::CopyOutputApplyGain2( float* pfDest, const float* pfSrc1, const float* pfSrc2, const int iOutputLength, const int iCrossfadeLength,
                                             int iOutputMode )
{
	// Atomares lesen in lokale Variablen
	float fGain1 = m_fCurrentGain;
	float fGain2 = m_fNewGain;

	if( ( fGain1 == fGain2 ) && ( fGain1 != 1 ) )
	{
		// Keine Änderung und neues Gain nicht 1 => Konstanter Gain
		if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
		{
			// Ausgabe Überschreiben
			for( int i = 0; i < ( std::min )( iCrossfadeLength, iOutputLength ); i++ )
				pfDest[i] = ( pfSrc1[i] + pfSrc2[i] ) * fGain1;
			for( int i = iCrossfadeLength; i < ( std::min )( m_iBlocklength, iOutputLength ); i++ )
				pfDest[i] = pfSrc2[i] * fGain1;
		}
		else
		{
			// Ausgabe Aufaddieren
			for( int i = 0; i < ( std::min )( iCrossfadeLength, iOutputLength ); i++ )
				pfDest[i] += ( pfSrc1[i] + pfSrc2[i] ) * fGain1;
			for( int i = iCrossfadeLength; i < ( std::min )( m_iBlocklength, iOutputLength ); i++ )
				pfDest[i] += pfSrc2[i] * fGain1;
		}
	}
	else
	{
		// Verstärkung ändert sich => Linear interpolierter Gain
		float c = ( fGain2 - fGain1 ) / (float)m_iBlocklength;

		if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
		{
			// Ausgabe Überschreiben
			for( int i = 0; i < ( std::min )( iCrossfadeLength, iOutputLength ); i++ )
				pfDest[i] = ( pfSrc1[i] + pfSrc2[i] ) * ( fGain1 + i * c );
			for( int i = iCrossfadeLength; i < ( std::min )( m_iBlocklength, iOutputLength ); i++ )
				pfDest[i] = pfSrc2[i] * ( fGain1 + i * c );
		}
		else
		{
			// Ausgabe Aufaddieren
			for( int i = 0; i < ( std::min )( iCrossfadeLength, iOutputLength ); i++ )
				pfDest[i] += ( pfSrc1[i] + pfSrc2[i] ) * ( fGain1 + i * c );
			for( int i = iCrossfadeLength; i < ( std::min )( m_iBlocklength, iOutputLength ); i++ )
				pfDest[i] += pfSrc2[i] * ( fGain1 + i * c );
		}

		m_fCurrentGain = fGain2;
	}
}
