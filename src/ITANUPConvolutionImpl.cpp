#include "ITANUPConvolutionImpl.h"

#include "ITANUPCFilterSegmentation.h"
#include "ITANUPCHelpers.h"
#include "ITANUPCInputBuffer1.h"
#include "ITANUPCOutputBuffer.h"
#include "ITANUPCStage.h"
#include "ITANUPCUFilter.h"
#include "ITANUPCUFilterPool.h"
#include "ITANUPCUtils.h"
#include "ITANUPFilterImpl.h"
#include "ITANUPartitioningScheme.h"

#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITAFastMath.h>
#include <ITAFilesystemUtils.h>
#include <ITAFunctors.h>
#include <ITAHPT.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <functional>
#include <iostream>
#include <iterator>
#include <process.h>
#include <stdlib.h>

/**
 * Helper functions for threads.
 */

// Makro zur Umrechnung von FILETIME ins 4-Wort-Format
#define FILETIME2QUADWORD( X ) ( Int64ShllMod32( X.dwHighDateTime, 32 ) | X.dwLowDateTime )

typedef unsigned( __stdcall* PTHREAD_START )( void* );

using namespace ITANUPC;


typedef struct
{
	ITANUPConvolutionImpl* pTarget;             // Zielinstanz
	ITANUPConvolutionImpl::CalcThreadOpts opts; // Parameter
} ThreadWrapperArgs;

// Thread function wrapping implementation instance
void ThreadWrapper( void* param )
{
	/*  DEBUG:
	    char buf[255];
	    sprintf(buf, "[%s] Prozess-ID = %d, Thread-ID = %d\n",
	    __FUNCTION__, GetCurrentProcessId(), GetCurrentThreadId());
	    OutputDebugString(buf);
	    */
	ThreadWrapperArgs* pArgs = (ThreadWrapperArgs*)param;
	int iResult              = pArgs->pTarget->CalcThreadProc( pArgs->opts );
	_endthreadex( iResult );
}

ITANUPConvolutionImpl::ITANUPConvolutionImpl( const CConvolutionParameter& oParams ) : m_iInputIDCounter( 0 ), iNumDropouts( 0 )
{
	m_oParams = oParams;

	// Auch die Felder der durch Mehrfachvererbung geerbeten ITADatasource setzen
	// TODO: Diamond-Kette erfernen! ITADatasources reine Schnittstelle!
	// m_uiBlocklength = uiBlocklength;
	// ITANUPC::m_uiChannels = 2;
	// m_dSamplerate = dSamplerate;

	// Zeiger 0 setzen (Wichtig f�r tidyup() im Fehlerfall)
	m_pScheme     = 0;
	hL1StartEvent = hL2StartEvent = hIdleEvent = hTerminateEvent = hThreadReadyEvent = 0;
	phCalcThreads                                                                    = 0;
	pOutputBuffer                                                                    = 0;

	fGlobalOutputGain = 1.0F;

#ifdef WITH_RECORDING
	pfRecordLeft = pfRecordRight = 0;
#endif

	// Parameter �berpr�fen
	/* Wichtig: Damit die FastMath-Befehle funktionieren muss die Blockl�nge ein
	            Vielfaches von 4 sein. Mit der Bedingung das die Blockl�nge eine
	            Zweierpotenz sein muss (wegen FFT), ergibt sich: Blockl�nge >= 4! */
	if( ( oParams.dSampleRate <= 0 ) || ( ITANUPCGetExp2( oParams.iBlockLength ) == -1 ) || ( oParams.iBlockLength < 4 ) ||
	    ( oParams.iImpulseResponseFilterLength <= 0 ) )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// Hochgenaue Zeitmessung des ITAToolkit initialisieren
	ITAHPT_init( );
	iRDTSCCycleDuration = ( int64_t )( ITAHPT_frequency( ) * (double)oParams.iBlockLength / oParams.dSampleRate );

	bError = false;

	tCycleDuration  = ( (double)oParams.iBlockLength ) / oParams.dSampleRate;
	tSampleDuration = 1 / oParams.dSampleRate;
	iCurCycle       = 0;
	iUserCycles     = 0;
	iNumDropouts    = 0;

	// �berblendung-Standardeinstellungen:
	iGlobalCrossfadeLength = 0; // Keine �berblendung
	iGlobalCrossfadeFunc   = LINEAR;

	// Zerlegungsschema berechnen
	BuildPartitionScheme( );

	tqTasks.setup( m_pScheme->iNumSegments );
	vStageStats.resize( m_pScheme->iNumSegments );

	// Ausgabepuffer erzeugen (Immer zwei Ausg�ngskan�le)
	pOutputBuffer = new COutputBuffer( 2, oParams.iBlockLength, ITANUPCNextPow2( oParams.iImpulseResponseFilterLength ) * 2 );
	if( pOutputBuffer == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT0( OUT_OF_MEMORY );
	}

	// Filterpool erzeugen
	// TODO: Welche Initiale Anzahl Filter?
	const int UFILTERPOOL_INITIAL_SIZE = 16;
	for( int s = 0; s < m_pScheme->iNumSegments; s++ )
		m_vpUFilterPools.push_back( new UFilterPool( &m_pScheme->voSegments[s], UFILTERPOOL_INITIAL_SIZE ) );

	// Berechnungs-Thread und weitere Elemente der Thread-Synchronisation erzeugen
	// (Zun�chst die Events)

	// Startereignisse = Manueller Reset, Nicht signalisiert!
	if( ( hL1StartEvent = CreateEvent( nullptr, true, false, nullptr ) ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	if( ( hL2StartEvent = CreateEvent( nullptr, true, false, nullptr ) ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	// Leerlaufereignis = Manueller Reset, Signalisiert!
	if( ( hIdleEvent = CreateEvent( nullptr, true, true, nullptr ) ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	// Terminationsereignis = Manueller Reset, Nicht signalisiert!
	if( ( hTerminateEvent = CreateEvent( nullptr, true, false, nullptr ) ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	// Thread-Bereit-Event = Manueller Reset, Nicht signalisiert!
	if( ( hThreadReadyEvent = CreateEvent( nullptr, true, false, nullptr ) ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	// TODO: Woanders festlegen
	nCalcThreads = oParams.nThreads;
	if( nCalcThreads == ITANUPC::NUPC_AUTO )
	{
		// Automatischer Modus => 1 Hintergrundthread
		nCalcThreads = 1;
	}

	// Berechnungs-Threads erzeugen
	if( ( phCalcThreads = new HANDLE[nCalcThreads] ) == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
	}

	// Prozess- und Threadaffinit�ten f�r Multiprozessoren setzen
	HANDLE hProcess = GetCurrentProcess( );

#ifdef _WIN64
	DWORD64 dwPAM, dwSAM;
#else
	DWORD dwPAM, dwSAM;
#endif
	if( FAILED( GetProcessAffinityMask( hProcess, &dwPAM, &dwSAM ) ) )
		ITA_EXCEPT1( UNKNOWN, "Unable to get process affinity mask" );

	printf( "Process affinity mask = 0x%08Xh\n", dwPAM );

	// Clovertown mit 8 Kernen!
	if( FAILED( SetProcessAffinityMask( hProcess, 255 ) ) )
		ITA_EXCEPT1( UNKNOWN, "Unable to set process affinity mask" );

	memset( phCalcThreads, 0, nCalcThreads * sizeof( HANDLE ) );

	ThreadWrapperArgs args;
	args.pTarget = this;

	for( int i = 0; i < nCalcThreads; i++ )
	{
		ResetEvent( hThreadReadyEvent );

		args.opts.uiThreadID = i; // Thread-Nummer
		if( i == 0 )
		{
			args.opts.uiBehaviour     = CALC_THREAD_QUANTUM_BASED;
			args.opts.hNewTasksSignal = hL1StartEvent;
			// args.opts.hCascadeEvent = hL2StartEvent;
			args.opts.hCascadeEvent = 0;

			// Alle Stufen bearbeiten!
			args.opts.iMinLevel = 0;
			args.opts.iMaxLevel = INT_MAX;

			// Tasks ohne Sch�tzwerte nur bei mehreren Rechenthreads aktivieren
		}
		else
		{
			args.opts.uiBehaviour = CALC_THREAD_STRAIGHT;
			// args.opts.hNewTasksSignal = hL2StartEvent;
			args.opts.hNewTasksSignal = hL1StartEvent;
			args.opts.hCascadeEvent   = 0;

			// Alle Stufen bearbeiten!
			args.opts.iMinLevel = 0;
			args.opts.iMaxLevel = INT_MAX;
		}

		// Info: Ab sofort wird hier die C-Lib Funktion verwendet!
		if( ( phCalcThreads[i] = (HANDLE)_beginthreadex( 0, 0, (PTHREAD_START)&ThreadWrapper, &args, 0, 0 ) ) == nullptr )
		{
			TidyUp( );
			ITA_EXCEPT1( UNKNOWN, "An internal error occured" );
		}

		// Core-Affinit�t setzen (auf SMP-Systemen)
		if( FAILED( SetThreadAffinityMask( phCalcThreads[i], 1 << i ) ) )
			ITA_EXCEPT1( UNKNOWN, "Unable to set process thread mask" );

		// Auf Bereitschaft des erzeugten Threads warten
		WaitForSingleObject( hThreadReadyEvent, INFINITE );

		printf( "[ITANUPCImpl] Calculation thread #%d ready\n", i + 1 );
	}

#ifdef WITH_RECORDING
	uiRecordLength = uprmul( (int)ceil( RECORDING_DURATION_SECS * dSamplerate ), uiBlocklength );
	if( !( pfRecordLeft = fm_falloc( uiRecordLength ) ) )
	{
		TidyUp( );
		ITA_EXCEPT0( OUT_OF_MEMORY );
	}

	if( !( pfRecordRight = fm_falloc( uiRecordLength ) ) )
	{
		TidyUp( );
		ITA_EXCEPT0( OUT_OF_MEMORY );
	}
	uiRecordCursor = 0;
#endif

	// Faltungsstufen erzeugen
	m_uiInputBufferSize = ITANUPCNextPow2( oParams.iImpulseResponseFilterLength ) * 2;
	/*
	    uiInputChannels = uiNumInputChannels;
	    m_vChannels.resize(uiInputChannels);
	    for (unsigned int c=0; c<uiInputChannels; c++) {
	    m_vChannels[c]->pInputDatasource = nullptr;
	    m_vChannels[c]->pInputBuffer = new InputBuffer1(uiBlocklength, uiInputBufferSize);
	    m_vChannels[c]->vpStages.resize(scheme->uiStages, nullptr);

	    for (unsigned int i=0; i<scheme->uiStages; i++)
	    m_vChannels[c]->vpStages[i] = new Stage(scheme, i, m_vChannels[c]->pInputBuffer);
	    }
	    */

	/*
	    // Alle Stufen erzeugen
	    if (!(pppStages = new Stage**[uiInputChannels])) {
	    tidyup();
	    ITA_EXCEPT0(OUT_OF_MEMORY);
	    }
	    memset(pppStages, 0, uiInputChannels*sizeof(Stage**));
	    for (unsigned int c=0; c<uiInputChannels; c++) {
	    if (!(pppStages[c] = new Stage*[scheme->uiStages])) {
	    tidyup();
	    ITA_EXCEPT0(OUT_OF_MEMORY);
	    }
	    memset(pppStages[c], 0, scheme->uiStages*sizeof(Stage*));
	    for (unsigned int i=0; i<scheme->uiStages; i++) {
	    if (!(pppStages[c][i] = new Stage(scheme, i)))  {
	    tidyup();
	    ITA_EXCEPT0(OUT_OF_MEMORY);
	    }

	    // DEBUG: Tag der Stufe setzen
	    char buf[255];
	    sprintf(buf, "Kanal-%d-Stufe-%d", c, i);
	    pppStages[c][i]->sTag = buf;
	    }
	    }

	    // Eingabepuffer erzeugen
	    if (!(pInputBuffer = new InputBufferN(uiInputChannels, uiBlocklength, ITANUPCNextPow2(iIRLength)*2))) {
	    tidyup();
	    ITA_EXCEPT0(OUT_OF_MEMORY);
	    }
	    */

	// --= Ausg�nge =--

	// INFO: In dieser Version gibt es immer genau zwei Ausg�nge (L/R)
	m_vpOutputs.push_back( new ITANUPCOutputImpl( 0, m_oParams.iBlockLength, m_oParams.iImpulseResponseFilterLength ) );
	m_vpOutputs.push_back( new ITANUPCOutputImpl( 1, m_oParams.iBlockLength, m_oParams.iImpulseResponseFilterLength ) );
}

ITANUPConvolutionImpl::~ITANUPConvolutionImpl( )
{
#ifdef WITH_RECORDING
	if( uiRecordCursor > 0 )
	{
		// Aufnahme speichern
		ITAAudiofileProperties props;
		props.dSamplerate   = ITADatasource::m_dSamplerate;
		props.eDomain       = ITA_TIME_DOMAIN;
		props.eQuantization = ITA_INT16;
		props.uiChannels    = 2;
		props.uiLength      = uiRecordCursor;
		std::vector<float*> vpfData;
		vpfData.push_back( pfRecordLeft );
		vpfData.push_back( pfRecordRight );
		try
		{
			for( unsigned int i = 0; i < uiRecordCursor; i++ )
			{
				pfRecordLeft[i] *= RECORDING_SCALE;
				pfRecordRight[i] *= RECORDING_SCALE;
			}
			writeAudiofile( RECORDING_OUTPUT_FILE, props, vpfData );
		}
		catch( ITAException& e )
		{
			fprintf( stderr, "Error: Unable to save recording (%s)\n", e.getReason( ) );
		}
	}
#endif

#ifdef WITH_EVENT_LOGGING
	std::cout << "[ITANUPCImpl] Writing log data into file \"" << EVENT_LOGFILE << "\" ... ";
	elLogConsumer.join( elLogBackgroundThread );
	elLogConsumer.toFile( EVENT_LOGFILE );
	// elLogConsumer.toXMLFile(EVENT_XML_LOGFILE);
	std::cout << "FINISHED" << std::endl;
#endif

	TidyUp( );
}

ITANUPC::CConvolutionParameter ITANUPConvolutionImpl::GetParams( ) const
{
	return m_oParams;
}

void ITANUPConvolutionImpl::Reset( )
{
	// TODO: Thread safety!

	// Alle Tasks l�schen
	tqTasks.clear( );

	// Alle Eingabepuffer, Stufen und Ausgabepuffer zur�cksetzen
	for_each( m_vpInputs, &CNUPCInputImpl::reset );
	for_each( m_vpChannels, &Channel::reset );
	for_each( m_vpOutputs, &ITANUPCOutputImpl::Reset );

#ifdef WITH_RECORDING
	uiRecordCursor = 0;
#endif
}

void ITANUPConvolutionImpl::GetRuntimeInfo( ITANUPC::RuntimeInfo* prtiDest )
{
	if( !prtiDest )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// getRuntimeInfo greift auf Kern-Variablen zu, deshalb ab in die CS:
	csRuntimeInfo.enter( );

	prtiDest->bError  = bError;
	prtiDest->tCycles = iUserCycles;

	prtiDest->dRuntimeDirectAnswerAverage      = swDirect.mean( );
	prtiDest->dRuntimeDirectAnswerMaximum      = swDirect.maximum( );
	prtiDest->dRuntimeDirectAnswerStdDeviation = swDirect.std_deviation( );
	/*
	    double dRT = toSeconds(ITAHPT_now() - ittStart);
	    prtiDest->dRT = dRT;

	    prtiDest->dRT_DR_Avg = swDR.mean();
	    prtiDest->dRT_DR_Max = swDR.maximum();
	    prtiDest->dRT_CT_Min = swCD.minimum();
	    prtiDest->dRT_CT_Avg = swCD.mean();
	    prtiDest->dRT_CT_Max = swCD.maximum();
	    prtiDest->dRT_CT_Var = swCD.variance();

	    if (tCycle == 0) {
	    prtiDest->dLoad_Avg = 1;
	    prtiDest->dDR_Load_Avg = 1;
	    } else {
	    //prtiDest->dLoad_Avg = (tCycle - swIT.mean()) / tCycle;
	    prtiDest->dLoad_Avg = calcThreadTime() / dRT;
	    prtiDest->dDR_Load_Avg = (tSample - swDR.mean()) / tSample;
	    }
	    */
	prtiDest->iNumDropouts = iNumDropouts;
	prtiDest->vStageInfos.resize( vStageStats.size( ) );
	for( size_t i = 0; i < vStageStats.size( ); i++ )
	{
		prtiDest->vStageInfos[i].iPartLength          = m_pScheme->voSegments[i].iPartLength;
		prtiDest->vStageInfos[i].iNumDropouts         = vStageStats[i].nDropouts;
		prtiDest->vStageInfos[i].tadExecutionDeadtime = vStageStats[i].aExecutionDeadtime.getData( );
		prtiDest->vStageInfos[i].tadExecutionTime     = vStageStats[i].aExecutionTime.getData( );
		prtiDest->vStageInfos[i].tadTaskInterruptions = vStageStats[i].aInterruptions.getData( );
	}

	prtiDest->iBaseExp2 = getExp2( m_oParams.iBlockLength );

	csRuntimeInfo.leave( );
}

void ITANUPConvolutionImpl::ResetRuntimeInfo( )
{
	csRuntimeInfo.enter( );

	bError       = false;
	iUserCycles  = 0;
	iNumDropouts = 0;
	for( int i = 0; i < m_pScheme->iNumSegments; i++ )
		vStageStats[i].reset( );

	swDirect.reset( );

	/*	ittStart = ITAHPT_now();
	    swCD.reset();
	    swDR.reset();
	    swIT.reset();
	    */

	// Thread-Zeiten ermitteln und ins 4-Wort-Format umrechnen
	/* TODO: Reactivate...
	    FILETIME ftDummy, ftKernel, ftUser;
	    GetThreadTimes(hThread, &ftDummy, &ftDummy, &ftKernel, &ftUser);
	    qwThreadKernelTimeStart = FILETIME2QUADWORD(ftKernel);
	    qwThreadUserTimeStart = FILETIME2QUADWORD(ftUser);
	    */
	csRuntimeInfo.leave( );
}

void ITANUPConvolutionImpl::GetInputs( std::vector<ITANUPC::IInput*>& vpInputs ) const
{
	ITACriticalSectionLock oLock( m_csInputs );

	vpInputs.clear( );
	std::copy( m_vpInputs.begin( ), m_vpInputs.end( ), std::back_inserter( vpInputs ) );
}

ITANUPC::IInput* ITANUPConvolutionImpl::AddInput( )
{
	ITACriticalSectionLock oLock( m_csInputs );

	CNUPCInputImpl* pInput = new CNUPCInputImpl( m_iInputIDCounter++, m_oParams.iBlockLength, m_uiInputBufferSize );
	m_vpInputs.push_back( pInput );

	// INFO: In dieser Version gibt es ein 1:1-Mapping zwischen Eingang und Kanal
	Channel* pChannel = new Channel( pInput, m_vpOutputs[0], m_vpOutputs[1], m_pScheme );
	m_vpChannels.push_back( pChannel );

	return pInput;
}

void ITANUPConvolutionImpl::RemoveInput( IInput* pInput, const bool )
{
	ITACriticalSectionLock oLock( m_csInputs );

	// Eingang suchen
	std::vector<CNUPCInputImpl*>::iterator it = std::find( m_vpInputs.begin( ), m_vpInputs.end( ), pInput );
	if( it == m_vpInputs.end( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid input" )

	// TODO: In die interne L�schliste
	//...
	delete *it;
	m_vpInputs.erase( it );

	// TODO: Kanal l�schen
}

void ITANUPConvolutionImpl::GetOutputs( std::vector<IOutput*>& vpOutputs ) const
{
	vpOutputs.clear( );
	std::copy( m_vpOutputs.begin( ), m_vpOutputs.end( ), std::back_inserter( vpOutputs ) );
}

IFilter* ITANUPConvolutionImpl::RequestFilter( )
{
	return new FilterImpl( this );
}

CUFilter* ITANUPConvolutionImpl::RequestUFilter( const int iSegment )
{
	// return new UFilter(&m_pScheme->vSegments[iSegment], nullptr);
	return m_vpUFilterPools[iSegment]->RequestFilter( );
}

void ITANUPConvolutionImpl::ExchangeFilter( IInput* pInput, IFilter* pFilter )
{
	// TODO: Verbessern

	// Kanal suchen
	Channel* pChannel( nullptr );
	for( size_t c = 0; c < m_vpChannels.size( ); c++ )
	{
		if( m_vpChannels[c]->pInput == pInput )
		{
			pChannel = m_vpChannels[c];
			break;
		}
	}

	if( !pChannel )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid input" );

	FilterImpl* pFilterImpl = dynamic_cast<FilterImpl*>( pFilter );
	if( pFilter )
	{
		for( int s = 0; s < m_pScheme->iNumSegments; s++ )
			pChannel->vpStages[s]->ExchangeFilter( pFilterImpl->m_vpSegments[s] );
	}
	else
	{
		for( int s = 0; s < m_pScheme->iNumSegments; s++ )
			pChannel->vpStages[s]->ExchangeFilter( nullptr );
	}
}

double ITANUPConvolutionImpl::GetOutputGain( )
{
	return (double)fGlobalOutputGain;
}

void ITANUPConvolutionImpl::SetOutputGain( const double dOutputGain )
{
	fGlobalOutputGain = (float)dOutputGain;
}

void ITANUPConvolutionImpl::SetIRCrossfadeLength( const int iCrossfadeLength )
{
	// Die �berblendl�nge darf die Blockl�nge nicht �berschreiten!
	if( iCrossfadeLength > m_oParams.iBlockLength )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// Reentrance unterbinden
	csReentrance.enter( );

	iGlobalCrossfadeLength = iCrossfadeLength;

	m_csChannels.enter( );
	/* TODO:
	for (ChannelMap::iterator it=m_mChannels.begin(); it!=m_mChannels.end(); ++it) {
	for (unsigned int i=0; i<scheme->uiStages; i++)
	it->second->vpStages[i]->iCrossfadeLength = (int) uiCrossfadeLength;
	}
	*/
	m_csChannels.leave( );

	csReentrance.leave( );
}

void ITANUPConvolutionImpl::SetIRCrossfadeFunction( const int iCrossfadeFunc )
{
	// Reentrance unterbinden
	csReentrance.enter( );

	iGlobalCrossfadeFunc = iCrossfadeFunc;

	m_csChannels.enter( );
	/* TODO:
	    for (ChannelMap::iterator it=m_mChannels.begin(); it!=m_mChannels.end(); ++it) {
	    for (unsigned int i=0; i<scheme->uiStages; i++)
	    it->second->vpStages[i]->iCrossfadeFunc = (int) uiCrossfadeFunc;
	    }
	    */
	m_csChannels.leave( );

	csReentrance.leave( );
}

void ITANUPConvolutionImpl::SetSegmentIRCrossfadeLength( const int iSegment, const int iCrossfadeLength )
{
	// G�ltigkeit des Segments pr�fen
	if( iSegment >= m_pScheme->iNumSegments )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// Die �berblendl�nge darf die Blockl�nge nicht �berschreiten!
	if( iCrossfadeLength > m_oParams.iBlockLength )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// Reentrance unterbinden
	csReentrance.enter( );

	iGlobalCrossfadeLength = iCrossfadeLength;
	/* TODO:
	m_csChannels.enter();
	for (ChannelMap::iterator it=m_mChannels.begin(); it!=m_mChannels.end(); ++it) {
	it->second->vpStages[uiSegment]->iCrossfadeLength = (int) uiCrossfadeLength;
	}
	m_csChannels.leave();
	*/

	csReentrance.leave( );
}

void ITANUPConvolutionImpl::SetSegmentIRCrossfadeFunction( const int iSegment, const int iCrossfadeFunc )
{
	// G�ltigkeit des Segments pr�fen
	if( iSegment >= m_pScheme->iNumSegments )
		ITA_EXCEPT0( INVALID_PARAMETER );

	// Reentrance unterbinden
	csReentrance.enter( );

	iGlobalCrossfadeFunc = iCrossfadeFunc;

	/* TODO:
	m_csChannels.enter();
	for (ChannelMap::iterator it=m_mChannels.begin(); it!=m_mChannels.end(); ++it) {
	it->second->vpStages[uiSegment]->iCrossfadeFunc = (int) uiCrossfadeFunc;
	}
	m_csChannels.leave();
	*/

	csReentrance.leave( );
}

void ITANUPConvolutionImpl::TidyUp( )
{
	// Zun�chst die Berechnungs-Threads beenden
	if( phCalcThreads != nullptr )
	{
		// Terminierungs-Ereignis signalisieren
		SetEvent( hTerminateEvent );

		for( int i = 0; i < nCalcThreads; i++ )
		{
			if( phCalcThreads[i] != 0 )
			{
				// Warten auf Beendigung des Hintergrund-Thread
				WaitForSingleObject( phCalcThreads[i], INFINITE );

				// Thread freigeben
				CloseHandle( phCalcThreads[i] );
			}
		}

		delete[] phCalcThreads;
	}

	// Events freigeben
	if( hL1StartEvent != 0 )
		CloseHandle( hL1StartEvent );
	if( hL2StartEvent != 0 )
		CloseHandle( hL2StartEvent );
	if( hIdleEvent != 0 )
		CloseHandle( hIdleEvent );
	if( hTerminateEvent != 0 )
		CloseHandle( hTerminateEvent );
	if( hThreadReadyEvent != 0 )
		CloseHandle( hThreadReadyEvent );

	// Kanalstrukturen freigben
	/*
	for (unsigned int c=0; c<uiInputChannels; c++) {
	delete m_vChannels[c]->pInputBuffer;
	for (unsigned int i=0; i<scheme->uiStages; i++)
	delete m_vChannels[c]->vpStages[i];
	}
	*/

	/* TODO:
	m_csChannels.enter();
	for (ChannelMap::iterator it=m_mChannels.begin(); it!=m_mChannels.end(); ++it) {
	Channel* pChannel = it->second;
	delete pChannel->pInputBuffer;
	std::for_each(pChannel->vpStages.begin(), pChannel->vpStages.end(), deleteFunctor<Stage>);
	}
	m_csChannels.leave();
	*/

	// Alle geladenen Impulsantworten freigeben
	// for (unsigned int i=0; i<vpfcFilterComponents.size(); i++) {
	//	ITANUPCFilterComponentImpl* pComponent = vpfcFilterComponents.at(i);
	//	delete pComponent;
	//}
	// vpfcFilterComponents.clear();

	// Filterpools freigeben
	std::for_each( m_vpUFilterPools.begin( ), m_vpUFilterPools.end( ), deleteFunctor<UFilterPool> );

	// Ein- und Ausgabepuffer freigeben
	delete pOutputBuffer;

	// Zerlegungsschema freigeben
	delete m_pScheme;

#ifdef WITH_RECORDING
	fm_free( pfRecordLeft );
	fm_free( pfRecordRight );
#endif

	// --= Ausg�nge =--

	std::for_each( m_vpOutputs.begin( ), m_vpOutputs.end( ), deleteFunctor<ITANUPCOutputImpl> );
	m_vpOutputs.clear( );

	// --= Z�hler =--

	m_iInputIDCounter = 0;
}

void ITANUPConvolutionImpl::BuildPartitionScheme( )
{
	/*	enum {
	        CONSTANT,	// Konstante Zerlegung in Blockl�ngen
	        EXP2,		// Zerlegung in Zweierpotenzen: 1, 1, 2, 4, 8, 16, ...
	        // (Anmerkung: Macht keinen Sinn im Einsatz, da jeder
	        //             Job sofort gerechnet werden muss)
	        DEXP2,		// Doppelte Zerlegung in Zweierpotenzen
	        RANDOM,		// Zuf�llige Zerlegung (f�r Testzwecke)
	        CUSTOM1,
	        CUSTOM2,
	        CUSTOM3		// Wie Custom2 nur hat die erste Stufe immer Vielfachheit 1
	        } T;

	        // >>> Hier das gew�nschte Zerlegungsschema ausw�hlen: <<<
	        T = CUSTOM3;
	        //T = DEXP2;
	        */
	std::vector<int> v;             // Vektor der Teill�ngen
	std::vector<FILTERSEGMENT> z;   // Hilfsvektor
	int p = 0;                      // Zugeteilte Gesamtl�nge
	int l = m_oParams.iBlockLength; // Aktueller Teill�nge

	FILTERSEGMENTATION fs;
	FSegCollection* fsc = nullptr;

	switch( m_oParams.eSegmentationScheme )
	{
		case UNIFORM:
		{
			while( p < m_oParams.iImpulseResponseFilterLength )
			{
				v.push_back( l );
				p += l;
			}
			break;
		}
		case GARDNER:
		{
			l = m_oParams.iBlockLength;
			// Erstes Teil doppelt
			v.push_back( l );
			p += l;
			while( p < m_oParams.iImpulseResponseFilterLength )
			{
				v.push_back( l );
				p += l;
				l *= 2;
			}
			break;
		}
		case FROM_FILE:
		{
			double dTargetSamplerate = 0.0f;
			int iTargetLength        = 0;

			/* Der Dateiname kann entweder auf eine konkrete Segmentierung zeigen
			   oder auf eine Sammlung (Collection) von Segmentierungen. Entschieden wird nach
			   Suffix: *.fsc -> Collection! */

			if( toUppercase( getFilenameSuffix( m_oParams.sSegmentationFile ) ) != "FSC" )
				fs = fseg_load( m_oParams.sSegmentationFile, &dTargetSamplerate, &iTargetLength );
			else
				fsc = new FSegCollection( m_oParams.sSegmentationFile );

			if( fsc == nullptr )
			{
				// Einzelne Segmentierung gegeben:

				if( dTargetSamplerate != m_oParams.dSampleRate )
					std::cerr << "Warning: Samplerate of the segmentation file does not match the actual samplerate!" << std::endl;

				if( iTargetLength < (int)m_oParams.iImpulseResponseFilterLength )
					ITA_EXCEPT1( INVALID_PARAMETER, "The segmentation in the given segmentation file is to short!" );
			}
			else
			{
				// Segmentierungs-Sammlung (Collection) gegeben:

				/* Segmentierung exakter L�nge oder n�chstgr��ere suchen */
				if( fsc->getSegmentation( (int)m_oParams.iImpulseResponseFilterLength, m_oParams.dSampleRate, fs ) == -1 )
					ITA_EXCEPT1( INVALID_PARAMETER, "The filter segmentation collection file does not contain a suitable filter segmentation!" );

				delete fsc;
			}

			// Umpacken (Nur soviele Teile wie erforderlich)
			for( size_t i = 0; i < fs.size( ); i++ )
				for( int j = 0; j < fs[i].iMultiplicity; j++ )
					if( p < m_oParams.iImpulseResponseFilterLength )
					{
						v.push_back( fs[i].iPartlength );
						p += fs[i].iPartlength;
					}

			break;
		}
		default: // Automatische Zerlegung
		{
			// Generalisierte Zerlegung
			int MULTIPLICITY = 4;
			int MAXLEN       = 32768;

			int p = 0;
			int l = m_oParams.iBlockLength;
			int k = 1;
			int j = 0;
			int m = 0;

			p = 0;
			l = m_oParams.iBlockLength;
			k = 1;
			j = 0;
			m = MULTIPLICITY;

			while( p < m_oParams.iImpulseResponseFilterLength )
			{
				int x = __min( k * l, MAXLEN );
				v.push_back( x );
				p += x;
				if( j <= m )
					j++;
				else
				{
					if( k < 65536 )
						k *= 2;
					j = 0;
				}
			}

			break;
		}
	}


	// Schema kompilieren
	m_pScheme = new NUPartition( v );
	if( m_pScheme == nullptr )
	{
		TidyUp( );
		ITA_EXCEPT0( INVALID_PARAMETER );
	}

	// DEBUG: Schema ausgeben
	std::cout << m_pScheme->ToString( );
}

void ITANUPConvolutionImpl::Calc0( )
{
	// DEBUG: printf("[ITANUPC::calc0]\n");

	swDirect.start( );

	/* Beschreibung: calc0 berechnet f�r ALLE Kan�le die direkten Antworten,
	                 d.h. die Antworten der kleinsten Stufen und mischt
	                 den dazugeh�rigen Ausgangsblock der h�heren Stufen hinzu */

	for( ChannelListIt it = m_vpChannels.begin( ); it != m_vpChannels.end( ); ++it )
	{
		Channel* pChannel = *it;

		CStage* pStage0 = pChannel->vpStages[0];

		int iInputOffset       = pChannel->pInput->getInputBuffer( )->getOffset( pStage0->iPartLength );
		int64_t iRDTSCDeadline = ITAHPT_now( );
		iRDTSCDeadline += 2 * iRDTSCCycleDuration; // TODO: Zeitangabe eigentlich �berfl�ssig!
		Task T( pChannel->pInput, iInputOffset, 0, pStage0, 0 /* No clearance */, iRDTSCDeadline );

		/* 1. Der kleinsten Stufe des Kanals (Blockl�nge-Stufe) die neuen Eingangsdaten
		      �bergeben und die Stufe falten */
		int iResult = pStage0->Compute( &T, false, iRDTSCDeadline );

		if( iResult != CStage::TASK_CALCULATION_COMPLETED )
		{
			// Das sollte nicht geschehen!
			OutputDebugStringA( "Schwerer interner Fehler! Direkte Berechnung unterbrochen!\n" );
			iNumDropouts++;
			T.nInterruptions++;
		}

		// Task-Statistik aktualisieren
		vStageStats[0].handleTask( T, iResult );

		// 2. Ergebnis der Stufe in den Ausgabepuffer mischen
		pOutputBuffer->Put( pStage0->pfLeftOutput, 0, pOutputBuffer->uiCursor, m_oParams.iBlockLength );
		pOutputBuffer->Put( pStage0->pfRightOutput, 1, pOutputBuffer->uiCursor, m_oParams.iBlockLength );
	}

	// Neue (gemischte )Ausgabedaten �ber die ITADatasourceRealization freisetzen
#ifdef WITH_RECORDING
	float* pfLeftTarget  = GetWritePointer( 0 );
	float* pfRightTarget = GetWritePointer( 1 );
	csOutputGain.enter( );
	pOutputBuffer->Get( pfLeftTarget, 0, fGlobalOutputGain );
	pOutputBuffer->Get( pfRightTarget, 1, fGlobalOutputGain );
	csOutputGain.leave( );

	// Aufnahmedaten speichern
	if( uiRecordCursor < uiRecordLength )
	{
		memcpy( pfRecordLeft + uiRecordCursor, pfLeftTarget, uiBlocklength * sizeof( float ) );
		memcpy( pfRecordRight + uiRecordCursor, pfRightTarget, uiBlocklength * sizeof( float ) );
		uiRecordCursor += uiBlocklength;
	}
#else
	float fOutputGain = fGlobalOutputGain;
	pOutputBuffer->Get( m_vpOutputs[0]->GetBuffer( ), 0, fOutputGain );
	pOutputBuffer->Get( m_vpOutputs[1]->GetBuffer( ), 1, fOutputGain );
#endif

	// Positionen im Ausgabepuffer weitersetzen
	pOutputBuffer->IncrementCursor( );

	swDirect.stop( );
}

void ITANUPConvolutionImpl::CreateTasks( )
{
	// DEBUG: printf("[ITANUPConvolutionImpl::createTasks] tNow = %I64d\n", tNow);

	bool bNewTasksCreated = false;
	int nTasksCreated     = 0; // Anzahl erzeugter Tasks

	for( int i = 1; i < m_pScheme->iNumSegments; i++ )
	{
		if( ( ( iCurCycle * m_oParams.iBlockLength ) % m_pScheme->voSegments[i].iPartLength ) == 0 )
		{
			// Tasks f�r die entsprechenden Stufen aller Kan�le erzeugen:

			// TODO: Deadline �berpr�fen
			int iClearance         = m_pScheme->voSegments[i].iClearance;
			int iDestOffset        = pOutputBuffer->uiCursor + ( m_oParams.iBlockLength * iClearance );
			CYCLE tDeadline        = iCurCycle + iClearance;
			int64_t iRDTSCDeadline = iRDTSCProcess + ( iClearance + 1 ) * iRDTSCCycleDuration;

			if( !bNewTasksCreated )
				csTaskQueue.enter( );

			// Tasks f�r alle Kan�le erzeugen
			for( size_t c = 0; c < m_vpChannels.size( ); c++ )
			{
				// TODO: iRDTSCDeadline
				int iSourceOffset = m_vpChannels[c]->pInput->getInputBuffer( )->getOffset( m_pScheme->voSegments[i].iPartLength );

				Task T( m_vpChannels[c]->pInput, iSourceOffset, iDestOffset, m_vpChannels[c]->vpStages[i], tDeadline, iRDTSCDeadline );

				tqTasks.put( T );
				nTasksCreated++;
			}

			bNewTasksCreated = true;
		}
	}

	if( bNewTasksCreated )
	{
#ifdef WITH_EVENT_LOGGING
		elLogConsumer.addEvent( ITAHPT_now( ), iCurCycle, "UserThread",
		                        "[createTasks] Created " + IntToString( nTasksCreated ) + " news tasks\n\nQueue status:\n" + tqTasks.toString( ) );
#endif

		csTaskQueue.leave( );
		// 1st Order Event ausl�sen
		PulseEvent( hL1StartEvent );
		ttStartEventPulsed = ITAHPT_now( );

#ifdef WITH_EVENT_LOGGING
		elLogConsumer.addEvent( ITAHPT_now( ), iCurCycle, "UserThread", "[createTasks] Pulsed start event" );
#endif
	}
}

int ITANUPConvolutionImpl::CalcThreadProc( CalcThreadOpts opts )
{
	Task T;
	CalcThreadOpts options = opts;
	bool bTerminate;
	std::string sModule = "CalcThread-" + UIntToString( options.uiThreadID );

	HANDLE objs[2];
	objs[0] = options.hNewTasksSignal;
	objs[1] = hTerminateEvent;

	SetEvent( hThreadReadyEvent );


	/*
	printf("[%s] Waiting for start event...\n", sModule.c_str());

	// fwe: Erweiterung: Hintergrund-Threads warten noch auf das Startzeichen
	if (bTerminate = (WaitForMultipleObjects(2, objs, FALSE, INFINITE) == (WAIT_OBJECT_0+1))) {
	// Nicht das Start-Event sondern direkt das Terminierungsevent empfangen...
	return;
	}
	*/

	/*
	#ifdef WITH_EVENT_LOGGING
	elLogBackgroundThread.addEvent(ITAHPT_now(), tNow, sModule, "Ready!");
	#endif
	*/

	// Endlosschleife! (Wird durch return verlassen)
	while( true )
	{
		/*
		    Als erstes werden Tasks aus der Queue gefiltert, deren Deadline
		    bereits �berschritten ist (Deadline exceeded)
		    */

		bool bNoTasks = false;
		csTaskQueue.enter( );

		// Versuchen den n�chsten Task zu holen
		if( !tqTasks.getByDeadlineBeforeLevel( T, options.iMinLevel, options.iMaxLevel ) )
		{
			// Keine passenden Tasks gefunden
			bNoTasks = true;
		}
		else
		{
			// Mindestens einen passenden Task gefunden!
			// Nun die Filterung von abgelaufenen Tasks durchf�hren...

			while( T.tDeadline < Now( ) )
			{
#ifdef WITH_EVENT_LOGGING
				elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Discarding task " + T.toShortString( ) + " - Deadline exceeded",
				                                ITANUPCEvent::CONV_EVENT_ERROR );
#endif

#ifdef WITH_TASK_TRASH
				lTaskTrash.push_back( T );
#endif
				// Diese Informationen betreffen die Runtime Infos
				// Deshalb im kritischen Bereich arbeiten:
				// DEBUG: csRuntimeInfo.enter();
				bError = true;
				iNumDropouts++;
				vStageStats[T.pStage->m_pPartition->iLevel].nDropouts++;
				// DEBUG: csRuntimeInfo.leave();

				/*
				char buf[255];
				sprintf(buf, "%d. Aussetzer (Stufe: %s)\n", uiDropouts, T.pStage->sTag.c_str());
				OutputDebugString(buf);
				*/

				// Versuchen einen weiteren Task zu holen
				if( !tqTasks.getByDeadlineBeforeLevel( T, options.iMinLevel, options.iMaxLevel ) )
				{
					// Keine passenden Tasks gefunden
					bNoTasks = true;
					break;
				}
			}
		}

		csTaskQueue.leave( );

		/*
		   Sind keine Tasks verf�gbar, geht der Hintergrund-Thread in
		   den Leerlaufzustand (idle state) und signalisiert das
		   Eintreten des Leerlaufzustand durch setzen des Ereignis hIdleEvent.
		   Danach wartet der Hintergrund-Thread auf das Startzeichen
		   durch den Hauptthread gewartet.
		   */

		if( bNoTasks )
		{
			SetEvent( hIdleEvent );

			// Auf Start- ODER Terminationssignal warten
			// DEBUG: printf("[ITANUPConvImpl::calcThreadProc] Warte auf Startzeichen!\n");

#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Waiting for new tasks" );
#endif
			bTerminate = ( WaitForMultipleObjects( 2, objs, FALSE, INFINITE ) == ( WAIT_OBJECT_0 + 1 ) );
			if( !bTerminate )
			{
				ttStartEventWaitReleased = ITAHPT_now( );

				/* Terminierungs-Ereignis war nicht der Freigeber des Waits;
				   Also war es das Start-Ereignis. Folglich sind Tasks vorhanden
				   und der Leerlaufzustand wird verlassen: */
				ResetEvent( hIdleEvent );
			}

#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent(
			    ITAHPT_now( ), iCurCycle, sModule,
			    "Waiting for new tasks - Released! (Pulse->Release: " + DoubleToString( toSeconds( ttStartEventWaitReleased - ttStartEventPulsed ) * 1000, 3 ) + "ms)" );
#endif

			// Start-Ereignis signalisiert -> An den Beginn der Schleife springen!
			if( !bTerminate )
				continue;
		}
		else
		{
			/* Mindestens ein Task ist vorhanden. Trotzdem muss das Terminierungs-Event abfragt werden...
			   (Hinweis: Kein Wait, nur Abfrage, denn TIMEOUT = 0!) */
			bTerminate = ( WaitForSingleObject( hTerminateEvent, 0 ) == WAIT_OBJECT_0 );
		}

		if( bTerminate )
		{
#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Terminating!" );
#endif
			// DEBUG: printf("[ITANUPConvImpl::calcThreadProc] Hintergrund-Thread endet\n");
			return 0;
		}

		/* An dieser Stelle liegt ein passender Task in T vor, falls gesch�tzwerte Pflitcht sind
		   (siehe bSkipUnestimated) hat dieser Task auf jedenfall einen Sch�tzwert. */

		/* Kaskadiertes Event ausl�sen, falls vorhanden...
		   (Hinweis: Die Reihenfolge ist determiniert. Dieser Thread hat den Task T bereits!
		   Er kann aber nicht wissen ob es noch weitere Tasks gibt, da die Threads
		   unterschiedliche Filter haben k�nnen. Deshalb wird die Kaskade immer ausgel�st! */

		if( ( options.hCascadeEvent != 0 ) && ( nCalcThreads > 1 ) )
		{
#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Pulsing cascaded event" );
#endif
			PulseEvent( options.hCascadeEvent );
		}

		/* 2. Aufgabe: Task berechnen */

		int iResult;                     // Status der Berechnung
		CStage::CSequencePointer oldSSP; // Vorheriger Status der Berechnung eines Tasks

		// fwe: Zusatz: Timestamp der Deadline resynchronisieren
		T.iRDTSCDeadline = iRDTSCProcess + ( T.tDeadline - Now( ) + 1 ) * iRDTSCCycleDuration;
		if( m_oParams.bSynchronous )
			T.iRDTSCDeadline = INFINITE_TIME;
		/*
		#ifdef WITH_EVENT_LOGGING
		elLogBackgroundThread.addEvent(ITAHPT_now(), tNow, sModule, "Syncing task #" + IntToString(T.ID) + " deadline timestamp: " + Int64ToString(iPrev) + " -> " +
		Int64ToString(T.iRDTSCDeadline)); #endif
		*/

		/* +----------------------------------------------------------------------------+
		   | Methode [1]: Berechnung der Tasks auf das Basis von Zeitbudgets (Quantums) |
		   +----------------------------------------------------------------------------+ */

		if( options.uiBehaviour == CALC_THREAD_QUANTUM_BASED )
		{
			/*
			int64_t iNow;
			queryRDTSCRegister(iNow);
			int64_t iQuantum = iRDTSCCycleDuration * 1;
			unsigned int uiResult = T.pStage->calc(&T, true, __min(this->iRDTSCGetBlockPointer + iRDTSCCycleDuration, iNow + iQuantum));
			*/

			/*
			int64_t iRDTSCEndOfQuantum = iRDTSCGetBlockPointer + iRDTSCCycleDuration;
			double dQuantum = toSeconds(iRDTSCEndOfQuantum - ITAHPT_now());
			*/

			int64_t iRDTSCNow          = ITAHPT_now( );
			int64_t iRDTSCEndOfQuantum = iRDTSCNow + iRDTSCCycleDuration / 2;
			if( m_oParams.bSynchronous )
				iRDTSCEndOfQuantum = INFINITE_TIME;
			double dQuantum = toSeconds( iRDTSCEndOfQuantum - ITAHPT_now( ) );

			if( T.nInterruptions == 0 )
				// In neue Task, die noch nicht begonnen wurde,
				// muss das gesch�tze initiale Zeitbudget eingetragen werden:
				T.dEstMinQuantum = T.pStage->EstimatedInitalQuantum( );

#ifdef WITH_TASK_SKIPPING_BY_ESTIMATED_MIN_QUANTUM

			/* Den Task gar nicht erst anfangen, falls das kleinste Quantum das bereitgestellte Quantum �berschreitet */
			if( T.dEstMinQuantum > dQuantum )
			{
#	ifdef WITH_EVENT_LOGGING
				elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule,
				                                "Skipping task #" + IntToString( T.ID ) + ": Minimum est. quantum = " + DoubleToString( T.dEstMinQuantum * 1000000, 3 ) +
				                                    "us < quantum " + DoubleToString( dQuantum * 1000000, 3 ) + "us" );

#	endif
				// Task wieder in die Queue
				csTaskQueue.enter( );
				tqTasks.put( T );
				csTaskQueue.leave( );

				// Kaskaden-Event pulsen (Vielleicht kann ein anderer Thread der wartet diesen Task ja rechnen
				if( options.hCascadeEvent != 0 )
				{
#	ifdef WITH_EVENT_LOGGING
					elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Pulsing cascaded event" );
#	endif
					PulseEvent( options.hCascadeEvent );
				}

				continue;
			}

#endif

#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule,
			                                ( T.nInterruptions == 0 ? std::string( "Starting " ) : std::string( "Continuing " ) ) + " task " + T.toShortString( ) +
			                                    "(Assigned quantum: " + DoubleToString( dQuantum * 1000000 ) +
			                                    " us, Est. min. quantum: " + DoubleToString( T.dEstMinQuantum * 1000000 ) + ")",
			                                ITANUPCEvent::CONV_EVENT_WARNING );
#endif
			oldSSP  = T.SSP;
			iResult = T.pStage->Compute( &T, true, iRDTSCEndOfQuantum );
		};

		/* +-------------------------------------------------------------------------------+
		   | Methode [2]: Berechnung ohne Zeitbudget. Jeder Task wird immer ganz gerechnet |
		   +-------------------------------------------------------------------------------+ */

		if( options.uiBehaviour == CALC_THREAD_STRAIGHT )
		{
#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule,
			                                ( T.nInterruptions == 0 ? std::string( "Starting " ) : std::string( "Continuing " ) ) + " task " + T.toShortString( ),
			                                ITANUPCEvent::CONV_EVENT_WARNING );
#endif
			iResult = T.pStage->Compute( &T, false, 0 );
		}

		//> Ende: Task berechnung
		// ------------------------------------------------------------------

		if( iResult == CStage::TASK_CALCULATION_COMPLETED )
		{
#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Finished task #" + UIntToString( T.ID ) );
#endif
			// Berechnete Daten in den Ausgabepuffer schreiben
			COutputBuffer::pInstance->Put( T.pStage->pfLeftOutput, 0, T.iOutputOffset, T.pStage->iPartLength );
			COutputBuffer::pInstance->Put( T.pStage->pfRightOutput, 1, T.iOutputOffset, T.pStage->iPartLength );

			// Statistik aktualisieren
			vStageStats[T.pStage->m_pPartition->iLevel].handleTask( T, iResult );

#ifdef WITH_TASK_TRASH
			lTaskTrash.push_back( T );
#endif
		}

		if( iResult == CStage::TASK_QUANTUM_EXCEEDED )
		{
#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule,
			                                std::string( "Interrupting task #" ) + UIntToString( T.ID ) + " for the " + IntToString( T.nInterruptions ) + " time",
			                                ITANUPCEvent::CONV_EVENT_WARNING );
			if( ( oldSSP.iCDI == T.SSP.iCDI ) && ( oldSSP.iMain == T.SSP.iMain ) )
			{
				elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "No progress!" );
			}

#endif

			// Statistik aktualisieren
			vStageStats[T.pStage->m_pPartition->iLevel].handleTask( T, iResult );

			/* Unfertigen Task wieder zur�ck in die Queue legen.
			   Da diese damit nicht leer sein kann, mu� bNoTasks = false sein
			   und wir k�nnen sofort zum Schleifenanfang zur�ckspringen... */
			csTaskQueue.enter( );
			tqTasks.put( T );
			csTaskQueue.leave( );
			continue;
		}

		if( iResult == CStage::TASK_DEADLINE_EXCEEDED )
		{
			// Abbruch durch Timeout

#ifdef WITH_EVENT_LOGGING
			elLogBackgroundThread.addEvent( ITAHPT_now( ), iCurCycle, sModule, "Discarding task #" + UIntToString( T.ID ) + " - Deadline exceeded",
			                                ITANUPCEvent::CONV_EVENT_ERROR );
#endif

#ifdef WITH_TASK_TRASH
			lTaskTrash.push_back( T );
#endif

			// Diese Informationen betreffen die Runtime Infos
			// Deshalb im kritischen Bereich arbeiten:
			// DEBUG: csRuntimeInfo.enter();
			bError = true;
			iNumDropouts++;

			// Statistik aktualisieren
			vStageStats[T.pStage->m_pPartition->iLevel].handleTask( T, iResult );

			// DEBUG: csRuntimeInfo.leave();

			/*
			char buf[255];
			sprintf(buf, "%d. Aussetzer (Stufe: %s)\n", uiDropouts, T.pStage->sTag.c_str());
			OutputDebugString(buf);
			*/
		}
	}

	printf( "[%s] Terminated!\n", sModule.c_str( ) );
}

double ITANUPConvolutionImpl::CalcThreadTime( )
{
	/* TODO: Reactivate...
	    // Aktuelle Thread-Zeiten ermitteln und ins 4-Wort-Format umrechnen
	    FILETIME ftDummy, ftKernel, ftUser;
	    GetThreadTimes(hThread, &ftDummy, &ftDummy, &ftKernel, &ftUser);
	    int64_t qwKernel = FILETIME2QUADWORD(ftKernel) - qwThreadKernelTimeStart;
	    int64_t qwThread = FILETIME2QUADWORD(ftUser) - qwThreadUserTimeStart;
	    int64_t qwTotal = qwKernel + qwThread;

	    // Von 100-Nanosekunden-Intervallen in Sekunden umrechnen
	    return (double) qwTotal / 10000000;
	    */
	return 0;
}

ITANUPC::CYCLE ITANUPConvolutionImpl::Now( )
{
	csTimeCounters.enter( );
	ITANUPC::CYCLE tResult = iCurCycle;
	csTimeCounters.leave( );
	return tResult;
}

void ITANUPConvolutionImpl::Process( )
{
	/*
	    // Externen Zugriff auf alle Laufzeitinformationen w�hrend der direkten
	    // Berechnung unterbinden!
	    csRuntimeInfo.enter();
	    */

	// Eintrittzeitpunkt in die Methode merken
	iRDTSCProcess = ITAHPT_now( );

#ifdef WITH_EVENT_LOGGING
	elLogConsumer.addEvent( ITAHPT_now( ), iCurCycle, "UserThread", "Entering process method" );
#endif

	// Zeitz�hler inkrementieren
	csTimeCounters.enter( );
	iCurCycle++;
	iUserCycles++;
	csTimeCounters.leave( );

	// -= Neue Eingangsdaten beziehen =--------------------------------------

	for( InputListIt it = m_vpInputs.begin( ); it != m_vpInputs.end( ); ++it )
		( *it )->load( );

	// -= �nderungen der Filterkomponenten der FO bearbeiten =-----

	// TODO: �berarbeiten
	//// Exklusiver Zugriff auf alle Filterobjekte erforderlich:
	// ITANUPCFilterObject::_csUpdate.enter();
	// csFilterComponents.enter();
	// Stage::csFilterComponents.enter();

	// for (PFOVEC_CIT fo_cit=pActiveFilters->begin(); fo_cit!=pActiveFilters->end(); fo_cit++) {
	//	ITANUPCFilterObject* pFilter = (*fo_cit);

	//	// �nderung der Filterkomponenten des Filter bearbeiten:
	//	for (unsigned int s=0; s<(*fo_cit)->uiSegments; s++) {
	//		if (pFilter->_pbUpdate[s]) {
	//			ITANUPCFilterComponentImpl* pNewFC = (ITANUPCFilterComponentImpl*) pFilter->_ppfcUpdate[s];
	//			/* DEBUG:
	//			sprintf(buf, "[%s] Plaziere neue Filterkomponente 0x%08Xh im Filter 0x%08Xh (Kanal %d) ab Segments %d an.\n", __FUNCTION__, pNewFC, pFilter,
	//pFilter->_iChannel, s); 			OutputDebugString(buf);
	//			*/

	//			for (unsigned int i=pNewFC->m_uiStartStage; i<pNewFC->m_uiStartStage+pNewFC->m_uiCoveredStages; i++) {
	//				// Neue Filterkomponente zuweisen
	//				pppStages[pFilter->_iChannel][i]->pFC = pNewFC;
	//
	//				// Referenzz�hler der Plazierungen in der neuen Filterkomponente inkrementieren
	//				pNewFC->rcPlaceRefCount++;
	//			}

	//			pNewFC->uiFORefCount--;
	//			pFilter->_pbUpdate[s] = false;
	//		}
	//	}
	//}

	// Stage::csFilterComponents.leave();
	// csFilterComponents.leave();
	// ITANUPCFilterObject::_csUpdate.leave();

	// -= Direkte Antwort berechnen & Tasks erzeugen =-------------

	// Direkte Antworten (1. Segmente aller Kan�le) berechnen
	Calc0( );

	// TODO: Lieber nach oben?
	// Neue Tasks erzeugen
	CreateTasks( );

	// DEBUG:
	// char buf[255];
	// sprintf(buf, "%d tasks in Queue\n", tqTasks.size());
	// OutputDebugString(buf);
	// ------

	if( m_oParams.bSynchronous )
	{
		// int n = 0;
		while( tqTasks.size( ) > 0 )
		{
			elLogConsumer.addEvent( ITAHPT_now( ), iCurCycle, "UserThread", "Synchronous mode - Waiting for finish\n" + tqTasks.toString( ) );
			Sleep( 100 );

			// n++;
			// if (n==3) break;
		}
	}

#ifdef WITH_EVENT_LOGGING
	elLogConsumer.addEvent( ITAHPT_now( ), iCurCycle, "UserThread", "Leaving process method" );
#endif
}

/*
int ITANUPC::Impl::requestConvolutionChannel()
{
csReentrance.enter();

int iChannelID = m_iChannelIDCounter++;
// Neue Kanalstruktur mit Stufen erzeugen
ConvolutionChannel* pChannel = new ConvolutionChannel;
pChannel->iChannelID = iChannelID;
pChannel->iState = -1;
pChannel->pInputDatasource = nullptr;
pChannel->pInputBuffer = new InputBuffer1(m_oParams.uiBlocklength, m_uiInputBufferSize);
pChannel->vpStages.resize(scheme->uiStages, nullptr);
for (unsigned int i=0; i<scheme->uiStages; i++)
pChannel->vpStages[i] = new Stage(scheme, i, pChannel->pInputBuffer);

m_csChannels.enter();
m_mChannels.insert( std::pair<int,ConvolutionChannel*>(pChannel->iChannelID, pChannel) );
m_csChannels.leave();

csReentrance.leave();

return iChannelID;
}

void ITANUPCImpl::releaseConvolutionChannel(int iChannelID, bool bReleaseImmediately) {
csReentrance.enter();

// Test: G�ltige Kanal-ID
m_csChannels.enter();
ChannelMap::iterator it = m_mChannels.find(iChannelID);
if (it == m_mChannels.end()) {
m_csChannels.leave();
csReentrance.leave();

ITA_EXCEPT1(INVALID_PARAMETER, "Invalid channel ID");
}

// Kanal aus der Map entfernen (schnell das Lock freigeben)
ConvolutionChannel* pChannel = it->second;
m_mChannels.erase(it);
m_csChannels.leave();

// Kanal-Strukturen freigeben
delete pChannel->pInputBuffer;
std::for_each(pChannel->vpStages.begin(), pChannel->vpStages.end(), deleteFunctor<Stage>);

csReentrance.leave();
}

void ITANUPCImpl::setInputDatasource(int iChannelID, ITADatasource* pdsSource) {

csReentrance.enter();

if (pdsSource) {
// Test: Eigenschaften des Audio-Streams
if ((pdsSource->GetSamplerate() != m_dSamplerate) ||
(pdsSource->GetNumberOfChannels() != 1) ||
(pdsSource->GetBlocklength() != m_uiBlocklength)) {
csReentrance.leave();
ITA_EXCEPT1(INVALID_PARAMETER, "Input datasource has wrong audio stream parameters");
}
}

// Test: G�ltige Kanal-ID
m_csChannels.enter();
ChannelMap::iterator it = m_mChannels.find(iChannelID);
if (it == m_mChannels.end()) {
m_csChannels.leave();
csReentrance.leave();
ITA_EXCEPT1(INVALID_PARAMETER, "Invalid channel ID");
}

// Zuordnung �ndern
it->second->pInputDatasource = pdsSource;
m_csChannels.leave();
csReentrance.leave();
}
*/


// ITANUFilterComponent* ITANUImpl::requestFilterComponent() {
//	return pFilterPool->requestFilterComponent();
//}
//
// void ITANUPCImpl::releaseFilterComponent(ITANUFilterComponent* pFilterComponent) {
//	pFilterPool->releaseFilterComponent(pFilterComponent);
//}
//
///*
// ITANUPCFilterComponent* ITANUPCImpl::createFilterComponent(const float* pfLeftData, unsigned int uiLeftDataLength,
//
//		                                           const float* pfRightData, unsigned int uiRightDataLength) {
//	return createFilterComponent(0, __max(uiLeftDataLength, uiRightDataLength), pfLeftData, uiLeftDataLength, pfRightData, uiRightDataLength);
//}
//
// ITANUPCFilterComponent* ITANUPCImpl::createFilterComponent(unsigned int uiStartSegment,
//		                                           unsigned int uiLength,
//										           const float* pfLeftData,
//												   unsigned int uiLeftDataLength,
//		                                           const float* pfRightData,
//												   unsigned int uiRightDataLength) {
//	// Parameter �berpr�fen:
//    // Startsegment g�ltig?
//	if (uiStartSegment >= scheme->uiStages)
//		ITA_EXCEPT0(INVALID_PARAMETER);
//
//	// L�nge ung�ltig?
//	if (uiLength > (iIRLength - scheme->vSegments[uiStartSegment].uiOffset))
//		ITA_EXCEPT0(INVALID_PARAMETER);
//
//	// Nullzeiger und L�nge > 0?
//	if ((!pfLeftData && (uiLeftDataLength > 0)) ||
//		(!pfRightData && (uiRightDataLength > 0))) ITA_EXCEPT0(INVALID_PARAMETER);
//
//	// Hinweis: uiDataLength darf gr��er als uiLength sein.
//	//          In diesem Falle werden nicht alle Samples aus pfData benutzt!
//
//	// Hier wissen wir: Alle Parameter sind okay.
//
//	// Versuche eine neue Filterkomponente zu erzeugen
//	ITANUPCFilterComponentImpl* pResult = new ITANUPCFilterComponentImpl(this, uiStartSegment, uiLength, pfLeftData, uiLeftDataLength, pfRightData, uiRightDataLength);
//
//	// Filterkomponente im Falter vermerken
//	csFilterComponents.enter();
//	vpfcFilterComponents.push_back(pResult);
//	csFilterComponents.leave();
//
//	return pResult;
//}
//
// void ITANUPCImpl::destroyFilterComponent(ITANUPCFilterComponent* pfcFilterComponent) {
//	csFilterComponents.enter();
//
//	// Suche die Komponente in der Liste
//	std::vector<ITANUPCFilterComponentImpl*>::iterator I;
//	if ((I = std::find(vpfcFilterComponents.begin(), vpfcFilterComponents.end(), pfcFilterComponent)) == vpfcFilterComponents.end()) {
//		csFilterComponents.leave();
//		// Komponente nicht enthalten!
//		ITA_EXCEPT0(INVALID_PARAMETER);
//	}
//
//	// Komponente aus der Liste entfernen
//	vpfcFilterComponents.erase(I);
//	csFilterComponents.leave();
//
//	// Filterkomponente freigeben
//	delete (ITANUPCFilterComponentImpl*) pfcFilterComponent;
//}
//*/
//
// std::vector<unsigned int> ITANUPCImpl::getSegmentOffsets() {
//	// TODO: Implementieren
//	std::vector<unsigned int> vResult;
//	for (unsigned int i=0; i<m_pScheme->nSegments; i++) vResult.push_back(m_pScheme->vSegments[i].uiOffset);
//	return vResult;
//}
//
// unsigned int ITANUPCImpl::getSegment(unsigned int uiSample) {
//	if (uiSample >= m_Params.iImpulseResponseFilterLength) ITA_EXCEPT0(INVALID_PARAMETER);
//
//	for (unsigned int s=1; s<m_pScheme->nSegments; s++)
//		if (uiSample < m_pScheme->vSegments[s].uiOffset) return s-1;
//
//	return m_pScheme->nSegments-1;
//}
//
// unsigned int ITANUPCImpl::getLowerSegmentOffset(unsigned int uiSample) {
//	unsigned int i = m_pScheme->nSegments-1;
//	unsigned int o;
//	while (uiSample < (o = m_pScheme->vSegments[i--].uiOffset));
//	return o;
//}
//
// unsigned int ITANUPCImpl::getUpperSegmentOffset(unsigned int uiSample) {
//	// Alles innerhalb und nach des letzten Segments hat kein Nachfolgesegment
//	if (uiSample >= m_pScheme->vSegments[m_pScheme->nSegments-1].uiOffset)
//		return(m_pScheme->vSegments[m_pScheme->nSegments-1].uiOffset);
//		//ITA_EXCEPT0(INVALID_PARAMETER);
//
//	// Jetzt ist sicher, das folgende Schleife terminiert:
//	unsigned int i=0;
//	unsigned int o;
//	while (uiSample >= (o = m_pScheme->vSegments[i++].uiOffset));
//	return o;
//}
//
// unsigned int ITANUPCImpl::getLastSegmentOffset() {
//	return m_pScheme->vSegments[m_pScheme->nSegments-1].uiOffset;
//}
//
// void ITANUPCImpl::setFilter(int iChannelID, ITANUPCFilterComponent* pfcFilterComponent) {
//	csReentrance.enter();
//
//	/* TODO: Zur Leistungssteigerung kann dieser Test �bersprungen werden,
//	         wenn man weiss was man tut...
//
//	if (pfcFilterComponent) {
//		// Test: Zugeh�rigkeit im eigenen Pool
//		if (!pFilterPool->managesFilterComponent(pfcFilterComponent)) {
//			csReentrance.leave();
//			ITA_EXCEPT0(RESOURCE_UNFEASIBLE);
//		}
//	}
//
//	*/
//
//	// Test: G�ltige Kanal-ID
//	/* TODO:
//	m_csChannels.enter();
//	ChannelMap::iterator it = m_mChannels.find(iChannelID);
//	if (it == m_mChannels.end()) {
//		m_csChannels.leave();
//		csReentrance.leave();
//		ITA_EXCEPT1(INVALID_PARAMETER, "Invalid channel ID");
//	}
//
//
//	Channel* pChannel = it->second;
//	m_csChannels.leave();
//
//	ITANUPCFilterComponentImpl* pFilter = dynamic_cast<ITANUPCFilterComponentImpl*>( pfcFilterComponent );
//	for (unsigned int i=pFilter->m_uiStartStage; i<pFilter->m_uiStartStage+pFilter->m_uiCoveredStages; i++)
//		pChannel->vpStages[i]->setFilterComponent(pFilter);
//		*/
//	csReentrance.leave();
//}
