#ifndef __STAGESTATISTIC_H__
#define __STAGESTATISTIC_H__

#include "ITANUPCStage.h"
#include "ITANUPCTask.h"

#include <ITATimeseriesAnalyzer.h>

/*

*/
class StageStatistic
{
public:
	// Anzahl Aussetzer
	int nDropouts;

	// Execution Deadtime = Dauer zwischen Erzeugung und Start der Bearbeitung der Tasks
	ITATimeseriesAnalyzer<double> aExecutionDeadtime;

	// Execution Time = Gesamtdauer der Bearbeitung der Tasks
	ITATimeseriesAnalyzer<double> aExecutionTime;

	// Interruptions = Anzahl der Unterbrechungen der Tasks
	ITATimeseriesAnalyzer<int> aInterruptions;

	StageStatistic( ) { reset( ); }

	// Statistik zurücksetzen
	void reset( )
	{
		nDropouts = 0;
		aExecutionDeadtime.reset( );
		aExecutionTime.reset( );
		aInterruptions.reset( );
	}

	// Einen Task behandeln
	void handleTask( const Task& task, int iStatus )
	{
		aExecutionDeadtime.handle( toSeconds( task.iRDTSCStart - task.iRDTSCCreation ) );

		if( iStatus == CStage::TASK_DEADLINE_EXCEEDED )
			nDropouts++;

		if( iStatus == CStage::TASK_CALCULATION_COMPLETED )
			aExecutionTime.handle( toSeconds( task.iRDTSCEnd - task.iRDTSCStart ) );

		if( iStatus == CStage::TASK_QUANTUM_EXCEEDED )
			aInterruptions.handle( task.nInterruptions );
	}
};

#endif