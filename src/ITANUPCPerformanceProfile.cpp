#include <list>
#ifdef ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER

#	include "ITANUPCPerformanceProfile.h"

#	include <ITAASCIITable.h>
#	include <ITAConfigUtils.h>
#	include <ITAException.h>
#	include <ITANumericUtils.h>
#	include <ITAStringUtils.h>
#	include <QSX.h>
#	include <cmath>

using namespace QSX;

PerformanceProfile::PerformanceProfile( )
{
	pOMT     = 0;
	pSMT     = 0;
	sComment = "";
}

PerformanceProfile::PerformanceProfile( std::string sFilename )
{
	pOMT               = 0;
	pSMT               = 0;
	sComment           = "";
	QSX::XMLNode* root = 0;

	try
	{
		root                         = XMLNode::loadXMLDocument( sFilename );
		std::list<XMLNode*> children = root->getChildren( );

		for( std::list<XMLNode*>::const_iterator cit = children.begin( ); cit != children.end( ); ++cit )
		{
			std::string sName = ( *cit )->getName( );
			if( sName == "operations_measurement" )
				pOMT = new OperationMeasurementTable( *cit );

			if( sName == "stage_measurement" )
				pSMT = new StageMeasurementTable( *cit );
		}

		delete root;
	}
	catch( XMLException& e )
	{
		delete root;

		// XMLException in ITAException delegieren
		ITA_EXCEPT1( IO_ERROR, e.toString( ) );
	}
}

PerformanceProfile::PerformanceProfile( ITASerializationBuffer& sb )
{
	pOMT     = 0;
	pSMT     = 0;
	sComment = "";

	bool bOMT, bSMT;
	sb >> bOMT;
	if( bOMT )
		pOMT = new OperationMeasurementTable( sb );
	sb >> bSMT;
	if( bSMT )
		pSMT = new StageMeasurementTable( sb );
}

PerformanceProfile::~PerformanceProfile( )
{
	removeOMT( );
	removeSMT( );
}

void PerformanceProfile::store( std::string sFilename )
{
	XMLNode* root = 0;

	try
	{
		root = XMLNode::createEmptyXMLDocument( "performanceprofile" );
		if( pOMT != 0 )
			pOMT->store( root );
		if( pSMT != 0 )
			pSMT->store( root );

		XMLNode::storeXMLDocument( root, sFilename );
		delete root;
	}
	catch( XMLException& e )
	{
		delete root;

		// XMLException in ITAException delegieren
		ITA_EXCEPT1( IO_ERROR, e.toString( ) );
	}
}

void PerformanceProfile::write( ITASerializationBuffer& sb )
{
	bool bOMT = ( pOMT != 0 );
	bool bSMT = ( pSMT != 0 );

	sb << bOMT;
	if( bOMT )
		pOMT->write( sb );
	sb << bSMT;
	if( bSMT )
		pSMT->write( sb );
}

OperationMeasurementTable* PerformanceProfile::createOMT( unsigned int uiStartExponent, unsigned int uiEndExponent )
{
	if( pOMT )
		removeOMT( );
	return pOMT = new OperationMeasurementTable( uiStartExponent, uiEndExponent );
}

void PerformanceProfile::removeOMT( )
{
	delete pOMT;
	pOMT = 0;
}

StageMeasurementTable* PerformanceProfile::createSMT( unsigned int uiStartExponent, unsigned int uiEndExponent, unsigned int uiMaxMultiplicity )
{
	if( pSMT )
		removeSMT( );
	return pSMT = new StageMeasurementTable( uiStartExponent, uiEndExponent, uiMaxMultiplicity );
}

void PerformanceProfile::removeSMT( )
{
	delete pSMT;
	pSMT = 0;
}

std::string PerformanceProfile::toString( ) const
{
	std::string s = "Performance profile:\n";
	s += "--------------------\n\n";

	if( !sComment.empty( ) )
		s += std::string( "\nComment: " ) + sComment + "\n\n";

	if( pOMT )
	{
		s += pOMT->toString( );
		s += "\n\n";
	}
	if( pSMT )
		s += pSMT->toString( );

	return s;
}

// ----------------------------------------------------------------------

OperationMeasurementTable::OperationMeasurementTable( unsigned int uiStartExponent, unsigned int uiEndExponent )
{
	a    = uiStartExponent;
	b    = uiEndExponent;
	n    = b - a + 1;
	data = new float[n * 5];
	for( unsigned int i = 0; i < n * 5; i++ )
		data[i] = std::numeric_limits<float>::max( );
}

OperationMeasurementTable::OperationMeasurementTable( XMLNode* root )
{
	// TODO: Improve parsing and error detection

	// First walk: Determine the minimum and maximum exponent
	std::list<XMLNode*> children = root->getChildren( );
	XMLNode* op                  = children.front( );

	a = UINT_MAX, b = 0;
	std::list<XMLNode*> datatags = op->getChildren( );
	for( std::list<XMLNode*>::const_iterator cit = datatags.begin( ); cit != datatags.end( ); ++cit )
	{
		unsigned int exp = (unsigned int)( *cit )->getAttributeAsInt( "exponent" );
		a                = std::min( a, exp );
		b                = std::max( b, exp );
	}

	n    = b - a + 1;
	data = new float[n * 5];

	// Second walk: Read the data records
	for( std::list<XMLNode*>::const_iterator cit = children.begin( ); cit != children.end( ); ++cit )
	{
		op                 = *cit;
		unsigned int op_id = UINT_MAX;
		if( op->getAttribute( "name" ) == "FFT" )
			op_id = FFT;
		if( op->getAttribute( "name" ) == "IFFT" )
			op_id = IFFT;
		if( op->getAttribute( "name" ) == "SMUL" )
			op_id = SMUL;
		if( op->getAttribute( "name" ) == "CMUL" )
			op_id = CMUL;
		if( op->getAttribute( "name" ) == "VADD" )
			op_id = VADD;

		std::list<XMLNode*> datatags = op->getChildren( );
		for( std::list<XMLNode*>::const_iterator cit2 = datatags.begin( ); cit2 != datatags.end( ); ++cit2 )
		{
			unsigned int exp = (unsigned int)( *cit2 )->getAttributeAsInt( "exponent" );
			float value      = (float)( *cit2 )->getContentAsDouble( );
			setRuntime( op_id, exp, value );
		}
	}
}

OperationMeasurementTable::OperationMeasurementTable( ITASerializationBuffer& sb )
{
	sb >> a >> b >> n;
	data = new float[n * 5];
	sb.read( data, n * 5 * sizeof( float ) );
}

OperationMeasurementTable::~OperationMeasurementTable( )
{
	delete[] data;
}

void OperationMeasurementTable::store( XMLNode* parent )
{
	XMLNode* root = parent->addChild( "operations_measurement" );
	char buf[255];

	XMLNode* op = root->addChild( "operation" )->setAttribute( "name", "FFT" );
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf, "%0.12f", getRuntime( FFT, i ) );
		op->addChild( "data" )->setAttribute( "exponent", (int)i )->setContent( buf );
	}

	op = root->addChild( "operation" )->setAttribute( "name", "IFFT" );
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf, "%0.12f", getRuntime( IFFT, i ) );
		op->addChild( "data" )->setAttribute( "exponent", (int)i )->setContent( buf );
	}

	op = root->addChild( "operation" )->setAttribute( "name", "SMUL" );
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf, "%0.12f", getRuntime( SMUL, i ) );
		op->addChild( "data" )->setAttribute( "exponent", (int)i )->setContent( buf );
	}

	op = root->addChild( "operation" )->setAttribute( "name", "CMUL" );
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf, "%0.12f", getRuntime( CMUL, i ) );
		op->addChild( "data" )->setAttribute( "exponent", (int)i )->setContent( buf );
	}

	op = root->addChild( "operation" )->setAttribute( "name", "VADD" );
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf, "%0.12f", getRuntime( VADD, i ) );
		op->addChild( "data" )->setAttribute( "exponent", (int)i )->setContent( buf );
	}
}

void OperationMeasurementTable::write( ITASerializationBuffer& sb )
{
	sb << a << b << n;
	sb.write( data, n * 5 * sizeof( float ) );
}

std::string OperationMeasurementTable::toString( ) const
{
	char buf1[255], buf2[255];

	std::string s = "Messwerte f�r Operationen (Einheit: Microsekunden)\n\n";

	// Header formatieren
	s += "Gr��e       FFT         IFFT        Vekt.Add.   Skal.Mul.   Kompl.Mul.\n";
	s += "----------------------------------------------------------------------\n";

	// Daten formatieren
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf1, "%d", 1 << i );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		float x = getRuntime( FFT, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( IFFT, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( VADD, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( SMUL, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( CMUL, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		s += "\n";
	}

	s += "----------------------------------------------------------------------\n";

	return s;


	/*
	    ITAASCIITable T;
	    T.addColumn("Size");
	    T.setColumnJustify(0, ITAASCIITable::RIGHT);
	    T.addColumn("VAdd\n[ns]");
	    T.addColumn("SMul\n[ns]");
	    T.addColumn("CMul\n[ns]");
	    T.addColumn("FFT\n[ns]");
	    T.addColumn("IFFT\n[ns]");

	    for (unsigned int i=a; i<=b; i++) {
	        T.addRow();
	        unsigned int k = T.rows()-1;
	        unsigned int e = 1<<a;

	        T.setContent(k, 0, UIntToString(e));
	        T.setContent(k, 1, DoubleToString(getRuntime(VADD, i), 1));
	        T.setContent(k, 2, DoubleToString(getRuntime(SMUL, i), 1));
	        T.setContent(k, 3, DoubleToString(getRuntime(CMUL, i), 1));
	        T.setContent(k, 4, DoubleToString(getRuntime(FFT, i), 1));
	        T.setContent(k, 5, DoubleToString(getRuntime(IFFT, i), 1));
	    }

	    return T.toString();*/
}

std::string OperationMeasurementTable::toDataString( ) const
{
	char buf1[255], buf2[255];

	std::string s;

	// Header formatieren
	s += "# [0] Size\n";
	s += "# [1] FFT total runtime [us]   [2]  FFT runtime per sample [ns]\n";
	s += "# [3] IFFT total runtime [us]  [4]  IFFT runtime per sample [ns]\n";
	s += "# [5] VADD total runtime [us]  [6]  VADD runtime per sample [ns]\n";
	s += "# [7] SMUL total runtime [us]  [8]  SMUL runtime per sample [ns]\n";
	s += "# [9] CMUL total runtime [us]  [10] CMUL runtime per sample [ns]\n\n";

	// Daten formatieren
	for( unsigned int i = a; i <= b; i++ )
	{
		int l = 1 << i;
		sprintf( buf1, "%d", l );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		float x = getRuntime( FFT, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x /= (float)l;
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;


		x = getRuntime( IFFT, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x /= (float)l;
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( VADD, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x /= (float)l;
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( SMUL, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x /= (float)l;
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x = getRuntime( CMUL, i );
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		x /= (float)l;
		if( x == std::numeric_limits<float>::max( ) )
			sprintf( buf1, "+INF" );
		else
			sprintf( buf1, "%0.3f", x * 1000000000 );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		s += "\n";
	}

	return s;
}

// ----------------------------------------------------------------------

StageMeasurementTable::StageMeasurementTable( unsigned int uiStartExponent, unsigned int uiEndExponent, unsigned int uiMaxMultiplicity )
{
	a    = uiStartExponent;
	b    = uiEndExponent;
	n    = b - a + 1;
	m    = uiMaxMultiplicity;
	data = new float[m * n];
	for( unsigned int i = 0; i < m * n; i++ )
		data[i] = std::numeric_limits<float>::max( );
}

StageMeasurementTable::StageMeasurementTable( XMLNode* root )
{
	// TODO:
}

StageMeasurementTable::StageMeasurementTable( ITASerializationBuffer& sb )
{
	sb >> a >> b >> n >> m;
	data = new float[m * n];
	sb.read( data, m * n * sizeof( float ) );
}

StageMeasurementTable::~StageMeasurementTable( )
{
	delete[] data;
}

void StageMeasurementTable::store( XMLNode* parent )
{
	XMLNode* root = parent->addChild( "stage_measurement_table" );

	for( unsigned int i = a; i <= b; i++ )
		for( unsigned int j = 1; j <= m; j++ )
			root->addChild( "data" )->setAttribute( "exponent", (int)i )->setAttribute( "multiplicity", (int)j )->setContent( getRuntime( i, j ) );
}

void StageMeasurementTable::write( ITASerializationBuffer& sb )
{
	sb << a << b << n << m;
	sb.write( data, m * n * sizeof( float ) );
}

std::string StageMeasurementTable::toString( ) const
{
	char buf1[255], buf2[255];

	std::string s = "Messwerte f�r Stufen (Einheit: Microsekunden)\n\n";

	// Header formatieren
	s += "Gr��e/VVH   ";
	std::string l = "------------";
	for( unsigned int i = 1; i <= m; i++ )
	{
		sprintf( buf1, "%d", i );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;
		l += "------------";
	}
	s += "\n";
	l += "\n";
	s += l;

	// Daten formatieren
	for( unsigned int i = a; i <= b; i++ )
	{
		sprintf( buf1, "%d", 1 << i );
		sprintf( buf2, "%-12s", buf1 );
		s += buf2;

		for( unsigned int j = 1; j <= m; j++ )
		{
			float x = getRuntime( i, m );
			if( x == std::numeric_limits<float>::max( ) )
				sprintf( buf1, "+INF" );
			else
				sprintf( buf1, "%0.3f", x * 1000000 );
			sprintf( buf2, "%-12s", buf1 );
			s += buf2;
		}

		s += "\n";
	}

	s += l;

	return s;
}

#endif // ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER