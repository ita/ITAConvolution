#include "ITADirectConvolutionImpl.h"

#include <ITADirectConvolution.h>

ITADirectConvolution::ITADirectConvolution( ) : m_pImpl( NULL ) {}

ITADirectConvolution::ITADirectConvolution( const int src1length, const int src2length, const int flags ) : m_pImpl( NULL )
{
	Plan( src1length, src2length, flags );
}

ITADirectConvolution::~ITADirectConvolution( )
{
	delete m_pImpl;
}

void ITADirectConvolution::Plan( const int src1length, const int src2length, const int flags )
{
	if( m_pImpl )
	{
		delete m_pImpl;
		m_pImpl = NULL;
	}

	m_pImpl = new ITADirectConvolutionImpl( src1length, src2length, flags );
}

bool ITADirectConvolution::IsPlanned( ) const
{
	return ( m_pImpl != NULL );
}

void ITADirectConvolution::Convolve( const float* src1, const int src1length, const float* src2, const int src2length, float* dest, const int destlength )
{
	if( m_pImpl )
		m_pImpl->Convolve( src1, src1length, src2, src2length, dest, destlength );
}
