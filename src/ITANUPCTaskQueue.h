#ifndef __TASKQUEUE_H__
#define __TASKQUEUE_H__

#include "ITANUPCTask.h"

#include <ITACriticalSection.h>
#include <ITANUPConvolution.h>
#include <functional>
#include <limits.h>
#include <queue>

// Vorw�rtsdeklarationen
class CStage;

/* Ordnung auf Tasks, welche aufsteigend nach Deadline sortiert */
struct TaskOrderDeadlineAsc : public std::binary_function<Task, Task, bool>
{
	bool operator( )( const Task& T1, const Task& T2 ) const
	{
		if( T1.tDeadline < T2.tDeadline )
			return true;
		if( T1.tDeadline > T2.tDeadline )
			return false;

		// Gleiche Deadlines
		return T1.ID < T2.ID;
	}
};

/*
// Ableitung der STL priority_queue die Zugriff auf den unterliegenden Kontainer zul�sst
class TaskPriorityQueue : public std::priority_queue<Task, std::vector<Task>, TaskOrderDeadlineAsc > {
public:
    // Iteratortypen (Nur konstante, damit die Queue �ber diesen Weg NICHT ver�ndert werden kann)
    typedef std::vector<Task>::const_iterator const_iterator;

    // Alle Tasks l�schen
    void clear();

    // Keine Tasks enthalten?
    bool empty() const;

    // Anzahl der enthaltenen Tasks zur�ckgeben
    size_t size() const;

    // Anfang der Queue zur�ckgeben
    const_iterator begin() const;

    // Ende der Queue zur�ckgeben
    const_iterator end() const;
};
*/

// Ableitung der STL priority_queue die Zugriff auf den unterliegenden Kontainer zul�sst
class TaskPriorityQueue
{
public:
	// Iteratortypen (Nur konstante, damit die Queue �ber diesen Weg NICHT ver�ndert werden kann)
	typedef std::vector<Task>::const_iterator const_iterator;

	// Alle Tasks l�schen
	void clear( );

	// Keine Tasks enthalten?
	bool empty( ) const;

	// Anzahl der enthaltenen Tasks zur�ckgeben
	size_t size( ) const;

	// Task hineingeben
	void push( const Task& T );

	// Obersten Task entnehmen
	void pop( );

	// Obersten Task zur�ckgeben
	Task& top( );

	// Anfang der Queue zur�ckgeben
	const_iterator begin( ) const;

	// Ende der Queue zur�ckgeben
	const_iterator end( ) const;

private:
	std::vector<Task> c;
};

/*
    Die Klasse TaskQueue realisiert einen Kontainer in dem der Falter seine
    Berechnungsaufgaben (tasks) speichert. Intern ist der Kontainer im Form
    mehrer priority queues aufgebaut. Verschiedene Methoden erlauben das
    unterschiedliche Strategien zur Auswahl und Entnahme von Tasks.

    \note Die Klasse ist nicht Thread-safe. Absicherung mu�, seitens
          der Benutzer, au�erhalb der Klasse erfolgen
*/

class TaskQueue
{
public:
	// Konstruktor
	TaskQueue( );

	// Anzahl der Stufen setzen (intern Queues bereitstellen)
	void setup( unsigned int uiNumStages );

	// Alle Tasks l�schen
	void clear( );

	// Keine Tasks enthalten?
	//	bool empty() const;

	// Anzahl der enthaltenen Tasks zur�ckgeben
	unsigned int size( ) const;

	// Neuen Task hinzuf�gen
	void put( Task& T );

	/* Deaktiviert: Keine Abw�rtskompartibilit�t mehr
	    // Standard-Entnahme Funktion: Deadline vor Stufenl�nge (wie bei alter Impl)
	    Task pop();
	*/

	// Entnahme eines Tasks. Ordnung: Deadline vor Stufe. Filterung m�glich...
	// R�ckgabe: Task vorhanden (ja/nein)
	bool getByDeadlineBeforeLevel( Task& T, int iMinLevel, int iMaxLevel = INT_MAX );

	// Zeichenkette mit Informationen �ber den Status zur�ckgeben
	std::string toString( );

private:
	unsigned int uiSize; // Gesamtanzahl enthaltener Tasks
	std::vector<TaskPriorityQueue> vtqQueues;
	ITACriticalSection csTaskQueues;
};

#endif