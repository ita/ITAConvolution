#include "ITANUPCUFilterPool.h"

#include "ITANUPCUFilter.h"

#include <ITADebug.h>
#include <ITAException.h>
#include <ITAFunctors.h>
#include <algorithm>
#include <cassert>
#include <cstdio>

// Debug-Meldung ausgeben
#define UFILTERPOOL_VERBOSE 1

// Zugeh�rigskeits-Test bei release()
#define UFILTERPOOL_CHECK_OWNERSHIP 1

UFilterPool::UFilterPool( const CUPartition* pPartition, int iInitialSize ) : m_pPartition( pPartition )
{
	assert( pPartition );
	assert( iInitialSize >= 0 );

	Grow( iInitialSize );
}

UFilterPool::~UFilterPool( )
{
	ITACriticalSectionLock oLock( m_csFilters );

	std::for_each( m_lpFreeFilters.begin( ), m_lpFreeFilters.end( ), deleteFunctor<CUFilter> );
	std::for_each( m_lpUsedFilters.begin( ), m_lpUsedFilters.end( ), deleteFunctor<CUFilter> );
	std::for_each( m_lpAutoFilters.begin( ), m_lpAutoFilters.end( ), deleteFunctor<CUFilter> );
}

int UFilterPool::GetNumFreeFilters( ) const
{
	ITACriticalSectionLock oLock( m_csFilters );
	return (int)m_lpFreeFilters.size( );
}

int UFilterPool::GetNumUsedFilters( ) const
{
	ITACriticalSectionLock oLock( m_csFilters );
	return (int)m_lpUsedFilters.size( ) + (int)m_lpAutoFilters.size( );
}

int UFilterPool::GetNumTotalFilters( ) const
{
	ITACriticalSectionLock oLock( m_csFilters );
	return (int)m_lpFreeFilters.size( ) + (int)m_lpUsedFilters.size( ) + (int)m_lpAutoFilters.size( );
}

void UFilterPool::Grow( int iDelta )
{
	ITACriticalSectionLock oLock( m_csFilters );

	for( int i = 0; i < iDelta; i++ )
		m_lpFreeFilters.push_back( CreateFilter( ) );
}

CUFilter* UFilterPool::RequestFilter( )
{
	ITACriticalSectionLock oLock( m_csFilters );

	CUFilter* pFilter( nullptr );

	if( m_lpFreeFilters.empty( ) )
	{
		// Fall: Keine Filter in der Frei-Liste -> Zun�chst schauen ob freie Filter in der Auto-Liste
		if( !m_lpAutoFilters.empty( ) )
		{
			for( std::list<CUFilter*>::iterator it = m_lpAutoFilters.begin( ); it != m_lpAutoFilters.end( ); ++it )
			{
				/*
				 *  Wichtig: Wenn der Filter bereits in der Auto-Liste ist,
				 *           hat der Benutzer f�r dieses Filter release() aufgerufen.
				 *           Er braucht den Filter also nicht mehr und darf den
				 *           ihm zuvor genannten Zeiger nicht mehr weiterbenutzen.
				 *           Deshalb kann auch den Filter auch nicht zwischenzeitlich
				 *           in einen anderen Falter einsetzen.
				 *
				 *           Fazit: Es ist sicher hier nur-lesend zu testen!
				 */

				assert( ( *it ) != 0 );

				if( !( *it )->IsInUse( ) )
				{
					pFilter = ( *it );

					// Filter wieder vergeben
					m_lpAutoFilters.erase( it );
#if( UFILTERPOOL_VERBOSE == 1 )
					DEBUG_PRINTF( "[UFilterPool] Reusing auto filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ),
					              m_lpAutoFilters.size( ), m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif
					break;
				}
				//				} else {
				//					// Nur Meldung machen
				//					UFilter::State::StateStruct ss;
				//					ss.iPrepRefCount = 0;
				//					ss.iUseRefCount = 0;
				//					if (pFilter) atomic_read32( &pFilter->m_oState.m_oState, &ss );
				//#if (UFILTERPOOL_VERBOSE==1)
				//					DEBUG_PRINTF("[UFilterPool] Cannot reusing auto filter 0x%08Xh (preps=%d, uses=%d) [Used %d, Auto %d, Free %d, Total %d]\n",
				//						pFilter, ss.iPrepRefCount, ss.iUseRefCount, m_lpUsedFilters.size(), m_lpAutoFilters.size(), m_lpFreeFilters.size(),
				//getNumTotalFilters()); #endif
				//				}
			}
		}

		if( !pFilter )
		{
			// Fall: Auch kein freies Filter in der Auto-Liste -> Neues Filter erzeugen
			pFilter = CreateFilter( );
		}

		m_lpUsedFilters.push_back( pFilter );
	}
	else
	{
		// Fall: Freie Filter verf�gbar
		pFilter = m_lpFreeFilters.back( );
		m_lpFreeFilters.pop_back( );
		m_lpUsedFilters.push_back( pFilter );
	}

#if( UFILTERPOOL_VERBOSE == 1 )
	DEBUG_PRINTF( "[UFilterPool] Request returns filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ), m_lpAutoFilters.size( ),
	              m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif

	return pFilter;
}

void UFilterPool::ReleaseFilter( CUFilter* pFilter )
{
	// TODO: Release muss ohne Lock auskommen!
	// Sonst haben wir Blocking-Artefakte im Stream-Processing
	ITACriticalSectionLock oLock( m_csFilters );

	assert( pFilter );

#if( UFILTERPOOL_VERBOSE == 1 )
	DEBUG_PRINTF( "[UFilterPool] Release filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ), m_lpAutoFilters.size( ),
	              m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif

#if( UFILTERPOOL_CHECK_OWNERSHIP == 1 )
	assert( pFilter->m_pParentPool == this );
	if( pFilter->m_pParentPool != this )
		ITA_EXCEPT1( UNKNOWN, "Internal error E2331" );

	std::list<CUFilter*>::iterator it = std::find( m_lpUsedFilters.begin( ), m_lpUsedFilters.end( ), pFilter );
	assert( it != m_lpUsedFilters.end( ) );
	if( it == m_lpUsedFilters.end( ) )
		ITA_EXCEPT1( UNKNOWN, "Internal error E2332" );
#endif

	if( pFilter->IsInUse( ) )
	{
		// Ist der Filter bereits oder noch in Benutzung
		// -> Zur Freigabe vormerken (Auto-Liste)
		m_lpUsedFilters.erase( it );
		m_lpAutoFilters.push_back( pFilter );

#if( UFILTERPOOL_VERBOSE == 1 )
		DEBUG_PRINTF( "[UFilterPool] Auto-release filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ), m_lpAutoFilters.size( ),
		              m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif
		return;
	}

	// Filter wieder in die Frei-Liste
	m_lpUsedFilters.erase( it );
	m_lpFreeFilters.push_back( pFilter );

#if( UFILTERPOOL_VERBOSE == 1 )
	DEBUG_PRINTF( "[UFilterPool] Instant-release filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ), m_lpAutoFilters.size( ),
	              m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif
}

CUFilter* UFilterPool::CreateFilter( )
{
	CUFilter* pFilter = new CUFilter( m_pPartition, this );

#if( UFILTERPOOL_VERBOSE == 1 )
	DEBUG_PRINTF( "[UFilterPool] Created new filter 0x%08Xh [Used %d, Auto %d, Free %d, Total %d]\n", pFilter, m_lpUsedFilters.size( ), m_lpAutoFilters.size( ),
	              m_lpFreeFilters.size( ), GetNumTotalFilters( ) );
#endif

	return pFilter;
}
