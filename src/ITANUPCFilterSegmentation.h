﻿#ifndef __FSEG_H__
#define __FSEG_H__

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER
#	include "ITASerializationBuffer.h"
#endif
#include <string>
#include <vector>

typedef struct
{
	int iPartlength;
	int iMultiplicity;
} FILTERSEGMENT;

// Creation helper function
FILTERSEGMENT fseg_make( int iPartlength, int iMultiplicity );

typedef std::vector<FILTERSEGMENT> FILTERSEGMENTATION;

//! Create a vector of filtersegments from an array of filtersegments
FILTERSEGMENTATION fseg_vector( const FILTERSEGMENT* pfsFSegs, int iNumFSegs );

//! Checks and corrects a filter segmentation
FILTERSEGMENTATION fseg_tidy( const std::vector<FILTERSEGMENT>& vFSegs );

//! Loads a filter segmentation from a file
std::vector<FILTERSEGMENT> fseg_load( const std::string& sFilename, double* pdSamplerate = 0, int* piLength = 0, int* piNumParts = 0 );

//! Store a filter segmentation to a file
void fseg_store( const std::string& sFilename, const std::vector<FILTERSEGMENT>& vFSegs, double dSamplerate, std::string sComment = "" );

//! Sum up the length of a segmentation
int fseg_len( const FILTERSEGMENTATION& vFSegs );

//! Return a string representation of an segmentation
std::string fseg_str( const FILTERSEGMENTATION& vFSegs );


#ifdef ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER

// Inserter for ITASerializationBuffers
ITASerializationBuffer& operator>>( ITASerializationBuffer& buffer, FILTERSEGMENT& seg );

// Extractor for ITASerializationBuffers
ITASerializationBuffer& operator<<( ITASerializationBuffer& buffer, const FILTERSEGMENT& seg );

#endif // ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER


/* This class realizes a container of filtersegmentations. Each filter segmentation contained
   has a unique length, but not necessarily every possible length has to be present. Because this
   a method for deriving intermediate length segmentations is provided. It will return the next
   greater oversized segmentation. */

class FSegCollection
{
public:
	// Matching modes
	enum
	{
		NO_MATCH    = -1,
		EXACT_MATCH = 0,
		NEXT_LONGER = 1
	};

	// Default constructor. Creates an empty collection.
	FSegCollection( );

	// Load constructor. Load a collection from a XML file.
	FSegCollection( const std::string& sFilename );

	// Store method. Writes a collection to a XML file.
	// (In case of error, ITAExceptions will be thrown)
	void store( const std::string& sFilename );

	// Returns the number of segmentations contained
	int size( ) const;

	// Gets a segmentation by its index
	void getSegmentationByIndex( int iIndex, FILTERSEGMENTATION& vDest, double* pdSamplerate = nullptr, int* piLength = nullptr ) const;

	// Gets a segmentation for the given length
	// (Returns the matching mode)
	int getSegmentation( int iLength, double dSamplerate, FILTERSEGMENTATION& vDest ) const;

	// Adds a segmentation - An existing segmentation of the same length will be replaced
	// (In case of error, ITAExceptions will be thrown)
	void addSegmentation( const FILTERSEGMENTATION& vSeg, double dSamplerate );

	// Removes a segmentation by index
	void removeSegmentationByIndex( int iIndex );

	// Removes a segmentation by length
	void removeSegmentation( int iLength, double dSamplerate );

	// Return a string representing the collection
	std::string toString( ) const;

private:
	typedef struct
	{
		int iLength;
		double dSamplerate;
		FILTERSEGMENTATION vSeg;
	} Item;

	struct ItemOrder : public std::binary_function<Item, Item, bool>
	{
		bool operator( )( const Item& I1, const Item& I2 ) const { return I1.iLength < I2.iLength; }
	};

	std::vector<Item> m_vItems;
};

#endif // __FSEG_H__
