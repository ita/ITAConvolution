#ifndef __FADE_H__
#define __FADE_H__

#include <ITANUPConvolution.h>

// Ein Zeitbereichsignal einblenden
void fade_in( float* pfData, unsigned int uiFadeLength, unsigned int uiFadeFunction );

// Ein Zeitbereichsignal ausblenden
void fade_out( float* pfData, unsigned int uiFadeLength, unsigned int uiFadeFunction );

// Zwei Zeitbereichssignale ineinander überblenden
// (pfSource wird Ausgeblendet, pfDest wird Eingeblendet)
void crossfade( float* pfDest, const float* pfSource, unsigned int uiCrossfadeLength, unsigned int uiFadeFunction );

#endif