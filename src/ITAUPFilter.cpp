#include <ITAException.h>
#include <ITAFFT.h>
#include <ITAFastMath.h>
#include <ITANumericUtils.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <algorithm>
#include <cassert>
#include <cstring>


ITAUPFilter::ITAUPFilter( const int iBlocklength, const int iMaxFilterLength )
    : m_iBlocklength( iBlocklength )
    , m_pParent( NULL )
    , m_pFFT( NULL )
    , m_iNumEffectiveFilterParts( 0 )
    , m_oState( this )
{
	m_iNumFilterParts = uprdiv( iMaxFilterLength, iBlocklength );
	int iFreqCoeffs   = iBlocklength + 1;

	// Anzahl Filterteile bestimmen
	m_vpfFreqData.resize( m_iNumFilterParts, NULL );

	// Datenpuffer allozieren
	for( int i = 0; i < m_iNumFilterParts; i++ )
		m_vpfFreqData[i] = fm_falloc( 2 * iFreqCoeffs, true );

	// FFT initial planen
	m_pFFT = new ITAFFT( ITAFFT::FFT_R2C, 2 * iBlocklength, m_vpfFreqData[0], m_vpfFreqData[0] );
}

ITAUPFilter::~ITAUPFilter( )
{
	// Datenpuffer freigeben
	std::for_each( m_vpfFreqData.begin( ), m_vpfFreqData.end( ), fm_free );

	delete m_pFFT;
}

ITAUPFilterPool* ITAUPFilter::getParentFilterPool( ) const
{
	return m_pParent;
}

void ITAUPFilter::Release( )
{
	if( m_pParent )
		m_pParent->ReleaseFilter( this );
}

bool ITAUPFilter::IsInUse( ) const
{
	return m_oState.IsInUse( );
}

void ITAUPFilter::Load( const float* pfFilterData, const int iFilterLength )
{
	m_csReentrance.enter( );

	// Wichtig: Das Laden des Filter ist nur möglich,
	//          wenn das Filter in keinem Falter aktuell benutzt wird
	if( IsInUse( ) )
	{
		m_csReentrance.leave( );
		ITA_EXCEPT1( MODAL_EXCEPTION, "Filter is placed for use or already in use" );
	}

	int iFreqCoeffs = m_iBlocklength + 1;

	// Sonderfall: Leeres Filter -> Leerer Teilspektren
	if( ( pfFilterData == NULL ) || ( iFilterLength == 0 ) )
	{
		for( int i = 0; i < m_iNumFilterParts; i++ )
			fm_zero( m_vpfFreqData[i], iFreqCoeffs * 2 );
		m_iNumEffectiveFilterParts = 0;
		m_csReentrance.leave( );
		return;
	}

	// Filter segmentieren und transformieren
	for( int i = 0; i < m_iNumFilterParts; i++ )
	{
		// Zunächst den Zeitbereichspuffer links mit Nullen initialisieren
		fm_zero( m_vpfFreqData[i], m_iBlocklength );

		// Filterkoeffizienten des Teils in die rechte Pufferhälfte laden. Danach transformieren.
		// Es ist müglich das die Restdaten kürzer sind als eine Blocklänge!

		int iOffset = i * m_iBlocklength;                         // Leseposition in der Impulsantwort
		int iRemain = ( std::max )( 0, iFilterLength - iOffset ); // Anzahl verbleibende Filterkoeffizienten

#ifdef ITA_CONVOLUTION_UPCONV_WITH_POWER_SAVER
		// Test ob alle Filterkoeffizienten im Block Nullen sind (Schwelle: -200 dB = 10^-10)
		const float ZERO_THRESHOLD = 0.0000000001F;
		bool bZeros                = true;

		if( iRemain > 0 )
		{
			for( int k = 0; k < std::min( iRemain, m_iBlocklength ); k++ )
			{
				if( std::abs( pfFilterData[iOffset + k] ) >= ZERO_THRESHOLD )
				{
					// Keine Nullen in diesem Block
					m_iNumEffectiveFilterParts = i + 1;
					bZeros                     = false;
					break;
				}
			}
		}
#else
		bool bZeros = false;
#endif // WITH_POWER_SAVER

		// IFFT Normalisierungsfaktor
		const float fScale = m_iBlocklength * 2.0F;

		if( ( iRemain == 0 ) || bZeros )
		{
			// Block Nullen setzen
			memset( m_vpfFreqData[i] + m_iBlocklength, 0, m_iBlocklength * sizeof( float ) );
		}
		else
		{
			if( iRemain < m_iBlocklength )
			{
				// Weniger als ein ganzer Block Filterkoeffizienten
				// (Normalisieren mit 1/2B da der Falter eine unnormalisierte IFFT benutzt)
				// memcpy(m_vpfFreqData[i]+m_iBlocklength, pfFilterData+iOffset, iRemain*sizeof(float));
				for( int k = 0; k < iRemain; k++ )
					m_vpfFreqData[i][m_iBlocklength + k] = pfFilterData[iOffset + k] / fScale;

				memset( m_vpfFreqData[i] + m_iBlocklength + iRemain, 0, ( m_iBlocklength - iRemain ) * sizeof( float ) );
			}
			else
			{
				// Ein ganzer Block Filterkoeffizienten
				// Kopieren am Stück
				// (Normalisieren mit 1/2B da der Falter eine unnormalisierte IFFT benutzt)
				// memcpy(m_vpfFreqData[i]+m_iBlocklength, pfFilterData+iOffset, m_iBlocklength*sizeof(float));
				for( int k = 0; k < m_iBlocklength; k++ )
					m_vpfFreqData[i][m_iBlocklength + k] = pfFilterData[iOffset + k] / fScale;
			}
		}

		// Daten in den Frequenzbereich transformieren (in-place)
		m_pFFT->execute( m_vpfFreqData[i], m_vpfFreqData[i] );
	}

#ifndef ITA_CONVOLUTION_UPCONV_WITH_POWER_SAVER
	// Immer alle Teile falten
	m_iNumEffectiveFilterParts = m_iNumFilterParts;
#endif

	m_csReentrance.leave( );
}

void ITAUPFilter::Zeros( )
{
	Load( NULL, 0 );
}

void ITAUPFilter::identity( )
{
	Zeros( );
	const float pfDirac[] = { 1 };
	Load( pfDirac, 1 );
}

// --= State class =--

ITAUPFilter::State::State( ITAUPFilter* pParent )
{
	m_pParent              = pParent;
	m_oState.iPrepRefCount = 0;
	m_oState.iUseRefCount  = 0;
}

bool ITAUPFilter::State::IsInUse( ) const
{
	return ( 0 < m_oState.iPrepRefCount + m_oState.iUseRefCount );
}

void ITAUPFilter::State::AddPrep( )
{
	ModifyState( 1, 0 );
}

void ITAUPFilter::State::RemovePrep( )
{
	ModifyState( -1, 0 );
}

void ITAUPFilter::State::ExchangePrep2Use( )
{
	ModifyState( -1, +1 );
}

void ITAUPFilter::State::RemoveUse( )
{
	ModifyState( 0, -1 );
}

void ITAUPFilter::State::ModifyState( const int16_t iPrepDelta, const int16_t iUseDelta )
{
	m_oState.iPrepRefCount += iPrepDelta;
	m_oState.iUseRefCount += iUseDelta;
}
