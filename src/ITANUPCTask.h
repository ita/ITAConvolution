#ifndef __TASK_H__
#define __TASK_H__

#include "ITANUPCStage.h"

#include <ITAConvolutionDefinitions.h>
#include <ITAHPT.h>
#include <ITANUPConvolution.h>
#include <ITAStringUtils.h>

class CNUPCInputImpl;

class Task
{
public:
	int ID;                       // Identifikationsnummer
	CNUPCInputImpl* pInput;       // Eingang
	int iInputOffset;             // Position im Eingangspuffer
	int iOutputOffset;            // Position im Ausgangspuffer
	CStage* pStage;               // Zugeordnete Stufe
	ITANUPC::CYCLE tDeadline;     // Zyklus zu dem der Task bearbeitet sein muss
	int64_t iRDTSCCreation;       // RDTSC-Z�hlerstand, an dem der Task erzeugt wurde
	int64_t iRDTSCDeadline;       // RDTSC-Z�hlerstand der zeitliche Deadline markiert
	int64_t iRDTSCStart;          // RDTSC-Z�hlerstand, an dem die Bearbeitung des Tasks begann
	int64_t iRDTSCEnd;            // RDTSC-Z�hlerstand, an dem die Bearbeitung des Tasks beendet wurde
	CStage::CSequencePointer SSP; // Zeiger auf den Schritte in der Stufenberechnung
	int nInterruptions;           // Anzahl der Unterbrechungen des Task
	double dEstMinQuantum;        // Gesch�tztes Minimales Zeitbudget f�r den n�chsten Berechnungsschritt

	inline Task( ) { };

	explicit inline Task( CNUPCInputImpl* pInput, int iSourceOffset, int iDestOffset, CStage* pStage, ITANUPC::CYCLE tDeadline, int64_t iRDTSCDeadline )
	{
		// TODO: ID-Counter ist nicht Thread-safe!
		ID                   = IDCounter++;
		this->pInput         = pInput;
		this->iInputOffset   = iSourceOffset;
		this->iOutputOffset  = iDestOffset;
		this->pStage         = pStage;
		this->tDeadline      = tDeadline;
		this->iRDTSCDeadline = iRDTSCDeadline;

		iRDTSCCreation = ITAHPT_now( );
		iRDTSCStart = iRDTSCEnd = -1;
		nInterruptions          = 0;
		dEstMinQuantum          = 0;
	}

	inline std::string toString( ) const
	{
		std::string s = "Task {\n";
		s += "  ID:				 " + IntToString( ID ) + "\n";
		// s += "  Input channel:  " + IntToString(pInput->getID()) + "\n";
		s += "  Length:         " + IntToString( pStage->iPartLength ) + "\n";
		s += "  Source offset:  " + IntToString( iInputOffset ) + "\n";
		s += "  Dest offset:    " + IntToString( iOutputOffset ) + "\n";
		s += "  Deadline:       " + IntToString( (int)tDeadline ) + "\n";
		s += "  SSP (Main,CDI): (" + IntToString( SSP.iMain ) + "," + UIntToString( SSP.iCDI ) + ")\n";
		s += "  EstMinQuantum:  " + DoubleToString( dEstMinQuantum * 1000000, 3 ) + " us\n";
		s += "}\n\n";
		return s;
	}

	inline std::string toShortString( ) const
	{
		return "#" + IntToString( ID ) + " (L = " + IntToString( pStage->iPartLength ) + ", DL = " + IntToString( (int)tDeadline ) +
		       ", SSP Main = " + UIntToString( SSP.iMain ) + ", CDI = " + UIntToString( SSP.iCDI ) + ", EMQ = " + DoubleToString( dEstMinQuantum * 1000000, 3 ) + " us)";
	}

	void inline print( ) { printf( "%s", toString( ).c_str( ) ); }

private:
	static unsigned int IDCounter; // Globaler Z�hler f�r Task-IDs
};

#endif
