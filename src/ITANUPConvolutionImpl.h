#ifndef IW_ITA_NUP_CONV_IMPL
#define IW_ITA_NUP_CONV_IMPL

// Fertige/Verworfene Tasks in einen "Papierkorb" um sp�ter Informationen zu sammeln?
//#define WITH_TASK_TRASH

// Falter-interne Ereignisse mitloggen
#define WITH_EVENT_LOGGING

#define EVENT_LOGFILE     "NUPCEvents.log"
#define EVENT_XML_LOGFILE "NUPCEvents.xml"

// Ausgabe aufzeichnen und im Destruktor in eine Audiodatei schreiben?
//#define WITH_RECORDING
#define RECORDING_OUTPUT_FILE   "NUPCRecording.wav"
#define RECORDING_DURATION_SECS 60
#define RECORDING_SCALE         1.0

// Mindestdauer f�r n�chsten Task-Berechnungsschritt berechnen und �berspringen erm�glichen
#define WITH_TASK_SKIPPING_BY_ESTIMATED_MIN_QUANTUM


#include "ITANUPCInputBuffer1.h"
#include "ITANUPCStageStatistic.h"
#include "ITANUPCTaskQueue.h"
#include "ITANUPartitioningScheme.h"

#include <ITACriticalSection.h>
#include <ITAException.h>
#include <ITAFFT.h>
#include <ITAFastMath.h>
#include <ITAFunctors.h>
#include <ITAHPT.h>
#include <ITAMutex.h>
#include <ITANUPConvolution.h>
#include <ITANumericUtils.h>
#include <ITASampleBuffer.h>
#include <ITAStopWatch.h>
#include <list>
#include <map>
#include <set>
#include <vector>
//#include <windows.h>

#ifdef WITH_EVENT_LOGGING
#	include "ITANUPCEventLog.h"
#endif

// Forwards
class CUFilter;
class UFilterPool;
class CInputBuffer1;
class InputBufferN;
class COutputBuffer;
class CStage;

//! Non-uniform partitioned convolution implementation
class CNUPCInputImpl : public ITANUPC::IInput
{
public:
	inline CNUPCInputImpl( const int iID, const int iBlockLength, const int iInputBufferSize ) : m_iID( iID ), m_sbInputData( iBlockLength, true ), m_pInputBuffer( NULL )
	{
		m_pInputBuffer = new CInputBuffer1( iBlockLength, iInputBufferSize );
	};

	inline ~CNUPCInputImpl( ) { delete m_pInputBuffer; };

	inline int GetID( ) const { return m_iID; };

	inline float* GetBuffer( ) const { return const_cast<float*>( m_sbInputData.data( ) ); };

	inline CInputBuffer1* getInputBuffer( ) const { return m_pInputBuffer; };

	inline void Put( const float* pfData )
	{
		if( pfData )
			m_sbInputData.write( pfData, m_sbInputData.length( ) );
		else
			m_sbInputData.Zero( );
	};

	// Eingang zur�cksetzen: Alle Puffer mit Nullen f�llen
	inline void reset( )
	{
		m_sbInputData.Zero( );
		m_pInputBuffer->reset( );
	};

	// Neue Eingabedaten im Eingangspuffer speichern
	inline void load( ) { m_pInputBuffer->put( m_sbInputData.data( ) ); };

private:
	int m_iID;
	ITASampleBuffer m_sbInputData; // �bergabepuffer f�r die Eingangsdaten von Au�en
	CInputBuffer1* m_pInputBuffer; // Interner Eingabepuffer mit Historie
};

// ID and data buffer class
class ITANUPCOutputImpl : public ITANUPC::IOutput
{
public:
	inline ITANUPCOutputImpl( const int iID, const int uiBlockLength, const int iIRLength )
	    : m_iID( iID )
	    , m_sbOutputData( uiBlockLength, true )
	    , m_iState( 0 )
	    , m_iCyclesRemain( uprdiv( iIRLength, uiBlockLength ) ) {

	    };

	inline bool remove( ) const { return ( m_iState == 1 ) || ( ( m_iState == 2 ) && ( m_iCyclesRemain <= 0 ) ); };

	inline int GetID( ) const { return m_iID; };

	inline const float* GetBuffer( ) const { return m_sbOutputData.data( ); };

	inline float* GetBuffer( ) { return const_cast<float*>( m_sbOutputData.data( ) ); };

	inline void Get( float* pfData )
	{
		if( pfData )
			m_sbOutputData.read( pfData, m_sbOutputData.length( ) );
		else
			ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer not allowed" );
	};

	// Ausgang zur�cksetzen: Alle Puffer mit Nullen f�llen
	inline void Reset( ) { m_sbOutputData.Zero( ); };

private:
	int m_iID;
	ITASampleBuffer m_sbOutputData; // �bergabepuffer f�r die Ausgangsdaten nach Au�en
	ITAAtomicInt m_iState;          // 0 => Bereit, 1 => Sofort entfernen, 2 => Ausklingen + entfernen
	int m_iCyclesRemain;            // Anzahl Verbleibende Bl�cke bis zum l�schen
};

/*
 *	This class implements the mainly abstract convolution class. Use
 * the static function create() to create an instanec for convolution.
 */
class ITANUPConvolutionImpl : public ITANUPC::IConvolution
{
public:
	//! Hilfsdatenstruktur f�r die Steuerdaten f�r die Berechnungssthreads kapselt
	typedef struct
	{
		unsigned int uiThreadID;  // Thread-Nummer
		unsigned int uiBehaviour; // Verhalten
		HANDLE hNewTasksSignal;   // Signal das neue Tasks verf�gbar
		HANDLE hCascadeEvent;     // Kaskadiertes Task-Signal
		int iMinLevel;            // Mindest Stufe
		int iMaxLevel;            // Maximal Stufe
	} CalcThreadOpts;

	//! Verhaltenstypen f�r Berechnungsthreads
	enum
	{
		CALC_THREAD_QUANTUM_BASED, // Budgetierte Berechnung
		CALC_THREAD_STRAIGHT       // Keine budgetierte Berechnung, immer ganz durchrechnen
	};

	ITANUPC::CConvolutionParameter m_oParams;

	unsigned int uiInputChannels; // Anzahl Eingangskan�le
	int nDropouts;                // Anzahl Aussetzer
	bool bError;                  // Fehler aufgetreten?
	unsigned int m_uiInputBufferSize;

	ITAAtomicFloat fGlobalOutputGain;    // Globale Ausgabeverst�rkung
	ITAAtomicInt iGlobalCrossfadeLength; // Globale �berblendl�nge (in Samples)
	ITAAtomicInt iGlobalCrossfadeFunc;   // Globale �berblendfunktion

	// std::vector< NUPFilterComponentImpl* > vpfcFilterComponents;	// Filterkomponenten des Falters

	NUPartition* m_pScheme; // Globales Partitionierungschema
	COutputBuffer* pOutputBuffer;
	TaskQueue tqTasks;

	ITATimerTicks ttStartEventPulsed, ttStartEventWaitReleased;

#ifdef WITH_EVENT_LOGGING
	ITANUPCEventLog elLogConsumer;         // Ereignisse des Aufrufers der Datenquellen Methode "GetBlockPointer"
	ITANUPCEventLog elLogBackgroundThread; // Ereignisse des Berechnungs-Threads
#endif

#ifdef WITH_TASK_TRASH
	std::list<Task> lTaskTrash; // Task-Papierkorb
#endif

#ifdef WITH_RECORDING
	unsigned int uiRecordLength;
	unsigned int uiRecordCursor;
	float* pfRecordLeft;
	float* pfRecordRight;
#endif

	// -= Zeitmessung =-
	ITANUPC::CYCLE iCurCycle;   // Hauptzyklusz�hler
	ITANUPC::CYCLE iUserCycles; // Benutzerzyklusz�hler (f�r Intervalle)
	int iNumDropouts;
	double tCycleDuration;           // Periodendauer einer Blockl�nge @ Samplerate
	double tSampleDuration;          // Periodendauer eines Samples @ Samplerate
	int64_t qwThreadKernelTimeStart; // Startwert der Kernelzeit des Calc-Thread
	int64_t qwThreadUserTimeStart;   // Startwert der Benutzerzeit des Calc-Thread

	int64_t iRDTSCProcess;       // RDTSC-Stand beim letzten GetBlockPointer
	int64_t iRDTSCCycleDuration; // Dauer einer Blockl�nge in RDTSC-Ticks

	ITAStopWatch swDirect; // Stoppuhr f�r die Dauer der direkten Berechnung

	// -= Multithreading und Thread-Synchronisation =-

	HANDLE hL1StartEvent;     // Ereignis f�r den Berechnungstart 1. Stufe
	HANDLE hL2StartEvent;     // Ereignis f�r den Berechnungstart 1. Stufe
	HANDLE hIdleEvent;        // Hintergrund-Thread ist im Leerlauf
	HANDLE hTerminateEvent;   // Hintergrund-Thread soll enden
	HANDLE hThreadReadyEvent; // Hintergrund-Thread bereit?
	// HANDLE hThread;				// Handle des Hintergrund-Thread
	HANDLE* phCalcThreads; // Handles der Berechungsthreads
	int nCalcThreads;      // Anzahl der Berechnungsthreads

	ITACriticalSection csReentrance;       // Zum Blocken von Reentrance
	ITACriticalSection csFilterComponents; // Exklusiver Zugriff auf die Filterkomponenten und deren Kontainer
	ITACriticalSection csRuntimeInfo;      // Exklusiver Zugriff auf Laufzeit-Informationen
	ITACriticalSection csTaskQueue;        // Exklusiver Zugriff auf die TaskQueue
	ITACriticalSection csTimeCounters;     // Exklusiver Zugriff auf die Zeitz�hler

	std::vector<StageStatistic> vStageStats; // Statistics for all stages


	ITANUPConvolutionImpl( const ITANUPC::CConvolutionParameter& oParams );
	~ITANUPConvolutionImpl( );

	ITANUPC::CConvolutionParameter GetParams( ) const;
	void Reset( );
	void Process( );

	void GetRuntimeInfo( ITANUPC::RuntimeInfo* prtiDest );
	void ResetRuntimeInfo( );

	/* +----------------------------------------------------------+ *
	 * |                                                          | *
	 * |   Kan�le                                                 | *
	 * |                                                          | *
	 * +----------------------------------------------------------+ */

	// --= Eing�nge und Ausg�nge =--

	typedef std::vector<CNUPCInputImpl*> InputList;
	typedef InputList::iterator InputListIt;
	typedef std::vector<ITANUPCOutputImpl*> OutputList;
	typedef OutputList::iterator OutputListIt;

	InputList m_vpInputs;
	ITACriticalSection m_csInputs; // Verhindert Reentrance der Input-Methoden
	int m_iInputIDCounter;
	OutputList m_vpOutputs;

	void GetInputs( std::vector<ITANUPC::IInput*>& vpInputs ) const;
	ITANUPC::IInput* AddInput( );
	void RemoveInput( ITANUPC::IInput* pInput, const bool bRemoveImmediately );
	void GetOutputs( std::vector<ITANUPC::IOutput*>& vpOutputs ) const;

	// --= Faltungskan�le =--

	class Channel
	{
	public:
		CNUPCInputImpl* pInput;
		ITANUPCOutputImpl* pOutputL;
		ITANUPCOutputImpl* pOutputR;
		ITAAtomicInt iState;           // Doku!
		std::vector<CStage*> vpStages; // Uniforme Faltungsstufen

		inline Channel( CNUPCInputImpl* input, ITANUPCOutputImpl* outputL, ITANUPCOutputImpl* outputR, NUPartition* scheme )
		    : pInput( input )
		    , pOutputL( outputL )
		    , pOutputR( outputR )
		{
			// Alle Faltungsstufen erzeugen
			vpStages.resize( scheme->iNumSegments, nullptr );
			for( int i = 0; i < scheme->iNumSegments; i++ )
				vpStages[i] = new CStage( &scheme->voSegments[i] );
		};

		inline ~Channel( ) { std::for_each( vpStages.begin( ), vpStages.end( ), deleteFunctor<CStage> ); };

		inline void reset( ) { for_each( vpStages, &CStage::Reset ); };
	};

	typedef std::vector<Channel*> ChannelList;
	typedef ChannelList::iterator ChannelListIt;

	ChannelList m_vpChannels;        // Zeiger auf interne Kanalliste
	ITACriticalSection m_csChannels; // Lock: Exklusiver Zugriff auf m_vChannels


	/* +----------------------------------------------------------+ *
	 * |                                                          | *
	 * |   Filter                                                 | *
	 * |                                                          | *
	 * +----------------------------------------------------------+ */

	std::vector<UFilterPool*> m_vpUFilterPools;

	inline const NUPartition* getPartitioningScheme( ) const { return m_pScheme; };

	ITANUPC::IFilter* RequestFilter( );

	// Filterkomponente f�r ein Segment anfordern
	CUFilter* RequestUFilter( int iSegment );

	//! Leeres Filter anfordern
	/*NUPFilterComponent* createFilterComponent(const float* pfLeftData, unsigned int uiLeftDataLength,
	                                          const float* pfRightData, unsigned int uiRightDataLength);
	                                          NUPFilterComponent* createFilterComponent(unsigned int uiStartSegment,
	                                          unsigned int uiLength,
	                                          const float* pfLeftData,
	                                          unsigned int uiLeftDataLength,
	                                          const float* pfRightData,
	                                          unsigned int uiRightDataLength);
	                                          void destroyFilterComponent(NUPFilterComponent* pfcFilterComponent);*/


	// void releaseFilterComponent(NUPFilterComponent* pFilterComponent);


	// unsigned int getMaxIRLength() const { return m_oParams.uiIRLength; }

	// std::vector<unsigned int> getSegmentOffsets();
	// unsigned int getSegment(unsigned int uiSample);
	// unsigned int getLowerSegment(unsigned int uiSample);
	// unsigned int getLowerSegmentOffset(unsigned int uiSample);
	// unsigned int getUpperSegment(unsigned int uiSample);
	// unsigned int getUpperSegmentOffset(unsigned int uiSample);
	// unsigned int getLastSegmentOffset();

	// void SetFilter(int iChannel, NUPFilterComponent* pfcFilterComponent);

	void ExchangeFilter( ITANUPC::IInput* pInput, ITANUPC::IFilter* pFilter );
	double GetOutputGain( );
	void SetOutputGain( const double dOutputGain );
	void SetIRCrossfadeLength( const int iCrossfadeLength );
	void SetIRCrossfadeFunction( const int iCrossfadeFunc );
	void SetSegmentIRCrossfadeLength( const int iSegment, const int iCrossfadeLength );
	void SetSegmentIRCrossfadeFunction( const int iSegment, const int iCrossfadeFunc );


	// Alle angeforderten Resourcen (Speicher, Kernel-Objekte) freigeben
	void TidyUp( );

	// Falterweites Partitionsschema berechnen
	void BuildPartitionScheme( );

	// Alle Blockl�nge-Stufen eines Kanals falten und die direkte Teilantwort
	// zusammenstellen und im Ausgabepuffer ablegen.
	void Calc0( );

	// Neue Tasks erzeugen
	void CreateTasks( );

	// Threadmethode f�r Berechnungsthreads (Hintergrundthreads)
	int CalcThreadProc( CalcThreadOpts opts );

	// Gesamte Rechenzeit des Berechungsthread zur�ckgeben (in Sekunden)
	double CalcThreadTime( );

	// Hauptzeitz�hler zur�ckgeben (Thread-safe)
	ITANUPC::CYCLE Now( );
};

#endif // IW
