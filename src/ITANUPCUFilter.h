#ifndef IW_ITA_NUP_CONV_FILTERCOMPONENTIMPL
#define IW_ITA_NUP_CONV_FILTERCOMPONENTIMPL

#include "ITANUPartitioningScheme.h"

#include <ITAAtomicPrimitives.h>
#include <ITACriticalSection.h>
#include <ITAFFT.h>
#include <ITAStopWatch.h>
#include <vector>

// Forwards
class UFilterPool;

// Implementation class for filter componentes
class CUFilter
{
public:
	// Konstruktor
	CUFilter( const CUPartition* pPartition, UFilterPool* pParentPool );
	~CUFilter( );

	bool IsInUse( ) const;
	void Release( );

	void Load( const float* pfLeftData, int iLeftDataLength, const float* pfRightData, int iRightDataLength );

	void Zero( );
	void Identity( );

	friend class ITANUPConvolutionImpl;

private:
	// Interne Zustandklasse, welche Referenzz�hler enth�lt.
	// Es werden immer zwei Z�hler ben�tigt: Prep und Use.
	class State
	{
	public:
		State( );

		bool isInUse( ) const;
		void addPrep( );
		void removePrep( );
		void xchangePrep2Use( );
		void removeUse( );

		// Damit auf �lteren Windows-System, welche keine DCAS-Operationen bereistellen,
		// dieser zwei-elementige Zustand atomar ver�ndert werden kann,
		// muss eine Datenstruktur von 32-Bit realisiert werden.
		// 16-Bit f�r die Z�hler ist immer noch ausreichend
		typedef struct
		{
			int16_t iPrepRefCount;
			int16_t iUseRefCount;
		} StateStruct;

		CUFilter* m_pParent;
		volatile StateStruct m_oState;

		void modifyState( int16_t iPrepDelta, int16_t iUseDelta );
	};

	const CUPartition* m_pPartition;     // Index des zugeh�rigen Segments in Partitionierung
	UFilterPool* m_pParentPool;          // �bergeordneter Pool
	State m_oState;                      // Aktueller Zustand
	int m_iDataBufSize;                  // Anzahl der floats im internen Datenpuffer
	float* m_pfDataBuf;                  // Interner Datenpuffer
	std::vector<float*> vpfPartSpecBuf;  // Datenpuffer f�r die Teilspektren (niemals Null)
	std::vector<float*> vpfPartSpecData; // Zeiger auf Teilspektren (k�nnen auch Null sein)
	ITAFFT m_fft;
	ITACriticalSection m_csReentrance; // Reentrance-Lock f�r die Load-Methode

	// PROFILING:
	ITAStopWatch sw;

	friend class CStage;
	friend class UFilterPool;
};

#endif // IW_ITA_NUP_CONV_FILTERCOMPONENTIMPL
