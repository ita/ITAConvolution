#ifndef IW_ITA_CONVOLUTION_IMPL
#define IW_ITA_CONVOLUTION_IMPL

#include <ITADirectConvolution.h>

// Vorwärtsdeklarationen
class ITAUPConvolution;
class ITAUPFilter;

// Implementierungsklasse der Faltungsklasse ITAConvolution
class ITADirectConvolutionImpl : public ITADirectConvolution
{
public:
	//! Konstruktor
	ITADirectConvolutionImpl( const int src1length, const int src2length, const int flags );

	//! Destruktor
	virtual ~ITADirectConvolutionImpl( );

	//! Faltungsmethode
	virtual void Convolve( const float* src1, const int src1length, const float* src2, const int src2length, float* dest, const int destlength );

private:
	ITAUPConvolution* m_pConvolver;
	ITAUPFilter* m_pFilter;
	int m_iFilter; // Index welche Eingabe der Filter ist (src1|src2)
	int m_iBlockLength;
};

#endif // __ITA_CONVOLUTION_IMPL_H__
