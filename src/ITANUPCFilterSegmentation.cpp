﻿#include "ITANUPCFilterSegmentation.h"

#include <ITAException.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER

#	include <QSX.h>
using namespace QSX;
// XML Schema used for validation of the single filtersegmentation file format
#	define FSEG_XML_SCHEMA \
		"\
<?xml version=\"1.0\"?>\n\
\n\
<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n\
	<xs:complexType name=\"filtersegmentType\">\n\
		<xs:simpleContent>\n\
			<xs:extension base=\"xs:positiveInteger\">\n\
				<xs:attribute name=\"multiplicity\" type=\"xs:positiveInteger\"/>\n\
			</xs:extension>\n\
		</xs:simpleContent>\n\
	</xs:complexType>\n\
	\n\
	<xs:simpleType name=\"decimalGreaterZero\">\n\
		<xs:restriction base=\"xs:decimal\">\n\
			<xs:minExclusive value=\"0\"/>\n\
		</xs:restriction>\n\
	</xs:simpleType>\n\
	\n\
	<xs:element name=\"filtersegmentation\">\n\
		<xs:complexType>\n\
			<xs:sequence>\n\
				<xs:element name=\"filtersegment\" type=\"filtersegmentType\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>\n\
			</xs:sequence>\n\
			\n\
			<xs:attribute name=\"samplerate\" type=\"decimalGreaterZero\" use=\"required\"/>\n\
			<xs:attribute name=\"length\" type=\"xs:nonNegativeInteger\"/>\n\
			<xs:attribute name=\"parts\" type=\"xs:nonNegativeInteger\"/>\n\
		</xs:complexType>\n\
	</xs:element>\n\
</xs:schema>"

#endif

FILTERSEGMENT fseg_make( int iPartlength, int iMultiplicity )
{
	FILTERSEGMENT fs;
	fs.iPartlength = iPartlength, fs.iMultiplicity = iMultiplicity;
	return fs;
};

FILTERSEGMENTATION fseg_vector( const FILTERSEGMENT* pfsFSegs, int iNumFSegs )
{
	FILTERSEGMENTATION result;
	for( int i = 0; i < iNumFSegs; i++ )
		result.push_back( pfsFSegs[i] );
	return result;
}

FILTERSEGMENTATION fseg_tidy( const FILTERSEGMENTATION& vFSegs )
{
	FILTERSEGMENTATION result;

	// Ensure that no part is negative and the series is nondecreasing
	// Remove parts whose multiplicity is 0
	int l = 1;
	int s = 0;
	for( FILTERSEGMENTATION::const_iterator cit = vFSegs.begin( ); cit != vFSegs.end( ); ++cit )
	{
		if( ( ( *cit ).iPartlength < 0 ) || ( ( *cit ).iPartlength < l ) || ( ( *cit ).iMultiplicity < 0 ) )
			ITA_EXCEPT1( INVALID_PARAMETER, "Not a causal binary partition" );

		if( ( *cit ).iMultiplicity > 0 )
		{
			// Causality check
			if( ( cit != vFSegs.begin( ) ) && ( s < ( *cit ).iPartlength ) )
				ITA_EXCEPT1( INVALID_PARAMETER, "Not a causal binary partition" );

			result.push_back( *cit );

			s += ( *cit ).iPartlength * ( *cit ).iMultiplicity;
		}

		l = ( *cit ).iPartlength;
	}

	// Stuff same parts following each other together
	int i = 0;
	while( i < (int)result.size( ) )
	{
		while( ( ( i + 1 ) < (int)result.size( ) ) && ( result[i + 1].iPartlength == result[i].iPartlength ) )
		{
			result[i].iMultiplicity += result[i + 1].iMultiplicity;
			result.erase( result.begin( ) + i + 1 );
		}
		i++;
	}

	return result;
}

FILTERSEGMENTATION fseg_load( const std::string& sFilename, double* pdSamplerate, int* piLength, int* piNumParts )
{
	FILTERSEGMENTATION segs_checked;

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	XMLNode* root = 0;

	try
	{
		/* Read the filtersegmentation from the given XML file and
		   validate it against the filtersegmentation XML schema */
		root = XMLNode::loadXMLDocument( sFilename, XMLNode::XML_SCHEMA_VALIDATION | XMLNode::READ_FROM_STRING, FSEG_XML_SCHEMA );

		double samplerate = 0;
		int length        = -1;
		int numparts      = -1;

		// The "samplerate" attribute is mandatory
		samplerate = root->getAttributeAsDouble( "samplerate" );

		if( root->hasAttribute( "length" ) )
			length = root->getAttributeAsInt( "length" );

		if( root->hasAttribute( "parts" ) )
			numparts = root->getAttributeAsInt( "parts" );

		/* We known that all nodes directly below the root node are "filtersegment" nodes */
		std::list<XMLNode*> children = root->getChildren( );
		FILTERSEGMENTATION segs;
		for( std::list<XMLNode*>::const_iterator cit = children.begin( ); cit != children.end( ); ++cit )
		{
			XMLNode* child = *cit;
			FILTERSEGMENT seg;
			seg.iPartlength = child->getContentAsInt( );

			if( child->hasAttribute( "multiplicity" ) )
				seg.iMultiplicity = child->getAttributeAsInt( "multiplicity" );
			else
				seg.iMultiplicity = 1;

			segs.push_back( seg );
		}

		// Free the XML document
		delete root;

		// More semantical checks that cannot be done via XML Schemata
		int l = 0;
		int p = 0;
		for( FILTERSEGMENTATION::const_iterator cit = segs.begin( ); cit != segs.end( ); ++cit )
		{
			l += ( *cit ).iPartlength * ( *cit ).iMultiplicity;
			p += ( *cit ).iMultiplicity;
		}

		if( ( length != -1 ) && ( length != l ) )
			std::cout << "Warning: The filtersegmentation file \"" << sFilename << "\" contains a wrong \"length\" attribute (Should be " << l << ")" << std::endl;

		if( ( numparts != -1 ) && ( numparts != p ) )
			std::cout << "Warning: The filtersegmentation file \"" << sFilename << "\" contains a wrong \"parts\" attribute (Should be " << p << ")" << std::endl;

		// Check and correct the segmentation
		segs_checked = fseg_tidy( segs );

		if( segs.size( ) != segs_checked.size( ) )
			std::cout << "Note: The filtersegmentation file \"" << sFilename << "\" is untidy" << std::endl;

		if( pdSamplerate )
			*pdSamplerate = samplerate;
		if( piLength )
			*piLength = l;
		if( piNumParts )
			*piNumParts = p;
	}
	catch( XMLException& e )
	{
		delete root;
		std::cerr << "XML-Exception: " << e.toString( ) << std::endl;
		ITA_EXCEPT1( UNKNOWN, std::string( "XML-Exception: " ) + e.toString( ) );
	}

	return segs_checked;

#else // ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Read/write of filter segmentation with XML file format not supported" );
#endif
}

void fseg_store( const std::string& sFilename, const FILTERSEGMENTATION& vFSegs, double dSamplerate, std::string sComment )
{
	/* Add the attributes for the length and number of parts */
	FILTERSEGMENTATION segs = fseg_tidy( vFSegs );
	int iLength             = 0;
	int iParts              = 0;
	for( FILTERSEGMENTATION::const_iterator cit = segs.begin( ); cit != segs.end( ); ++cit )
	{
		iLength += ( ( *cit ).iPartlength * ( *cit ).iMultiplicity );
		iParts += ( *cit ).iMultiplicity;
	}

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	XMLNode* root = 0;

	// Initialize the XML parser library
	try
	{
		root = XMLNode::createEmptyXMLDocument( "filtersegmentation" );
		root->setAttribute( "samplerate", dSamplerate );
		root->setAttribute( "length", iLength );
		root->setAttribute( "parts", iParts );

		/* Write a child node for each filter segment */
		for( FILTERSEGMENTATION::const_iterator cit = segs.begin( ); cit != segs.end( ); ++cit )
		{
			XMLNode* child = root->addChild( "filtersegment", ( *cit ).iPartlength );
			if( ( *cit ).iMultiplicity > 0 )
				child->setAttribute( "multiplicity", ( *cit ).iMultiplicity );
		}

		// Write the XML document into the file. Afterwards free it...
		XMLNode::storeXMLDocument( root, sFilename );
	}
	catch( XMLException& e )
	{
		delete root;
		ITA_EXCEPT1( IO_ERROR, e.toString( ) );
	}
#else // ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Read/write of filter segmentation with XML file format not supported" );
#endif
}

int fseg_len( const FILTERSEGMENTATION& vFSegs )
{
	int s = 0;
	for( FILTERSEGMENTATION::const_iterator cit = vFSegs.begin( ); cit != vFSegs.end( ); ++cit )
		s += ( cit->iPartlength * cit->iMultiplicity );
	return s;
}

std::string fseg_str( const FILTERSEGMENTATION& vFSegs )
{
	std::stringstream ss;
	// for (FILTERSEGMENTATION::const_iterator cit=vFSegs.begin(); cit!=vFSegs.end(); ++cit)
	//	ss << (*cit).iPartlength << "^" << (*cit).iMultiplicity << " ";
	for( int i = 0; i < (int)vFSegs.size( ); i++ )
		if( vFSegs[i].iMultiplicity > 0 )
			ss << vFSegs[i].iPartlength << "^" << vFSegs[i].iMultiplicity << " ";
	return ss.str( );
}

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER
ITASerializationBuffer& operator>>( ITASerializationBuffer& buffer, FILTERSEGMENT& seg )
{
	return buffer >> seg.iPartlength >> seg.iMultiplicity;
}

ITASerializationBuffer& operator<<( ITASerializationBuffer& buffer, const FILTERSEGMENT& seg )
{
	return buffer << seg.iPartlength << seg.iMultiplicity;
}
#endif


// XML Schema used for validation of the signel filtersegmentation file format
#define FSEGCOLL_XML_SCHEMA \
	"\
<?xml version=\"1.0\"?>\n\
\n\
<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n\
	<xs:complexType name=\"filtersegmentType\">\n\
		<xs:simpleContent>\n\
			<xs:extension base=\"xs:positiveInteger\">\n\
				<xs:attribute name=\"multiplicity\" type=\"xs:positiveInteger\"/>\n\
			</xs:extension>\n\
		</xs:simpleContent>\n\
	</xs:complexType>\n\
	\n\
	<xs:simpleType name=\"decimalGreaterZero\">\n\
		<xs:restriction base=\"xs:decimal\">\n\
			<xs:minExclusive value=\"0\"/>\n\
		</xs:restriction>\n\
	</xs:simpleType>\n\
	\n\
	<xs:complexType name=\"filtersegmentationType\">\n\
		<xs:sequence>\n\
			<xs:element name=\"filtersegment\" type=\"filtersegmentType\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>\n\
		</xs:sequence>\n\
		\n\
		<xs:attribute name=\"samplerate\" type=\"decimalGreaterZero\" use=\"required\"/>\n\
		<xs:attribute name=\"length\" type=\"xs:nonNegativeInteger\"/>\n\
		<xs:attribute name=\"parts\" type=\"xs:nonNegativeInteger\"/>\n\
	</xs:complexType>\n\
	\n\
	<xs:element name=\"filtersegmentation_collection\">\n\
		<xs:complexType>\n\
			<xs:sequence>\n\
				<xs:element name=\"filtersegmentation\" type=\"filtersegmentationType\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>\n\
			</xs:sequence>\n\
		</xs:complexType>\n\
	</xs:element>\n\
</xs:schema>"


FSegCollection::FSegCollection( ) {}

FSegCollection::FSegCollection( const std::string& sFilename )
{
#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER

	XMLNode* root = 0;
	FILTERSEGMENTATION segs_checked;

	try
	{
		/* Read the filtersegmentation from the given XML file and
		   validate it against the filtersegmentation XML schema */
		root = XMLNode::loadXMLDocument( sFilename, XMLNode::XML_SCHEMA_VALIDATION | XMLNode::READ_FROM_STRING, FSEGCOLL_XML_SCHEMA );

		std::list<XMLNode*> fseg_nodes = root->getChildren( );

		for( std::list<XMLNode*>::iterator it = fseg_nodes.begin( ); it != fseg_nodes.end( ); ++it )
		{
			XMLNode* fseg = *it;

			// Quick & dirty bugfix: Skip non "filtersegmentation" nodes
			// if (fseg->getName() != "filtersegmentation") continue;

			double samplerate = 0;
			int length        = -1;
			int numparts      = -1;

			// The "samplerate" attribute is mandatory
			samplerate = fseg->getAttributeAsDouble( "samplerate" );

			if( fseg->hasAttribute( "length" ) )
				length = fseg->getAttributeAsInt( "length" );

			if( fseg->hasAttribute( "parts" ) )
				numparts = fseg->getAttributeAsInt( "parts" );

			/* We known that all nodes directly below the root node are "filtersegment" nodes */
			std::list<XMLNode*> children = fseg->getChildren( );
			FILTERSEGMENTATION segs;
			for( std::list<XMLNode*>::const_iterator cit = children.begin( ); cit != children.end( ); ++cit )
			{
				XMLNode* child = *cit;
				FILTERSEGMENT seg;
				seg.iPartlength = child->getContentAsInt( );

				if( child->hasAttribute( "multiplicity" ) )
					seg.iMultiplicity = child->getAttributeAsInt( "multiplicity" );
				else
					seg.iMultiplicity = 1;

				segs.push_back( seg );
			}

			// More semantical checks that cannot be done via XML Schemata
			int l = 0;
			int p = 0;
			for( FILTERSEGMENTATION::const_iterator cit = segs.begin( ); cit != segs.end( ); ++cit )
			{
				l += ( *cit ).iPartlength * ( *cit ).iMultiplicity;
				p += ( *cit ).iMultiplicity;
			}

			// Check and correct the segmentation
			segs_checked = fseg_tidy( segs );

			addSegmentation( segs_checked, fseg->getAttributeAsDouble( "samplerate" ) );
		}

		// Free the XML document
		delete root;
	}
	catch( XMLException& e )
	{
		delete root;
		std::cerr << "XML-Exception: " << e.toString( ) << std::endl;
		ITA_EXCEPT1( UNKNOWN, std::string( "XML-Exception: " ) + e.toString( ) );
	}

#else // ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Read/write of filter segmentation with XML file format not supported" );
#endif
}

void FSegCollection::store( const std::string& sFilename )
{
#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER

	XMLNode* root = 0;

	// Initialize the XML parser library
	try
	{
		root = XMLNode::createEmptyXMLDocument( "filtersegmentation_collection" );

		for( std::vector<Item>::const_iterator cit = m_vItems.begin( ); cit != m_vItems.end( ); ++cit )
		{
			XMLNode* segnode = root->addChild( "filtersegmentation" );
			segnode->setAttribute( "samplerate", cit->dSamplerate );
			segnode->setAttribute( "length", cit->iLength );
			segnode->setAttribute( "parts", (int)cit->vSeg.size( ) );

			/* Write a child node for each filter segment */
			for( FILTERSEGMENTATION::const_iterator cit2 = cit->vSeg.begin( ); cit2 != cit->vSeg.end( ); ++cit2 )
			{
				XMLNode* child = segnode->addChild( "filtersegment", cit2->iPartlength );
				if( cit2->iMultiplicity > 0 )
					child->setAttribute( "multiplicity", cit2->iMultiplicity );
			}
		}

		// Write the XML document into the file. Afterwards free it...
		XMLNode::storeXMLDocument( root, sFilename );
	}
	catch( XMLException& e )
	{
		delete root;
		ITA_EXCEPT1( IO_ERROR, e.toString( ) );
	}

#else // ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Read/write of filter segmentation with XML file format not supported" );
#endif
}

int FSegCollection::size( ) const
{
	return (int)m_vItems.size( );
}

void FSegCollection::getSegmentationByIndex( int iIndex, FILTERSEGMENTATION& vDest, double* pdSamplerate, int* piLength ) const
{
	if( ( iIndex < 0 ) || ( iIndex >= (int)m_vItems.size( ) ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index out of bounds" );

	vDest = m_vItems[iIndex].vSeg;
	if( pdSamplerate != nullptr )
		*pdSamplerate = m_vItems[iIndex].dSamplerate;
	if( piLength != nullptr )
		*piLength = m_vItems[iIndex].iLength;
}

int FSegCollection::getSegmentation( int iLength, double dSamplerate, FILTERSEGMENTATION& vDest ) const
{
	// Search all items for the segmentation having the smallest length greater or equal
	for( std::vector<Item>::const_iterator cit = m_vItems.begin( ); cit != m_vItems.end( ); ++cit )
		if( ( cit->iLength >= iLength ) && ( cit->dSamplerate == dSamplerate ) )
		{
			// Exact match
			vDest = cit->vSeg;
			return ( cit->iLength == iLength ? EXACT_MATCH : NEXT_LONGER );
		}

	return NO_MATCH;
}

void FSegCollection::addSegmentation( const FILTERSEGMENTATION& vSeg, double dSamplerate )
{
	int iLength = fseg_len( vSeg );

	if( iLength == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Empty segmentations may not be added" );

	// Search if a segmentation of the exact length is already contained. If so: Replace it.
	for( std::vector<Item>::iterator it = m_vItems.begin( ); it != m_vItems.end( ); ++it )
		if( ( it->iLength == iLength ) && ( it->dSamplerate == dSamplerate ) )
		{
			it->vSeg = vSeg;
			return;
		}

	// New length: Insert and maintain the order.
	Item item;
	item.iLength     = iLength;
	item.dSamplerate = dSamplerate;
	item.vSeg        = vSeg;
	m_vItems.push_back( item );
	std::sort( m_vItems.begin( ), m_vItems.end( ), ItemOrder( ) );
}

void FSegCollection::removeSegmentationByIndex( int iIndex )
{
	if( ( iIndex < 0 ) || ( iIndex >= (int)m_vItems.size( ) ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index out of bounds" );

	m_vItems.erase( m_vItems.begin( ) + iIndex );
}

void FSegCollection::removeSegmentation( int iLength, double dSamplerate )
{
	// Search the segmentation of the exact length
	for( std::vector<Item>::iterator it = m_vItems.begin( ); it != m_vItems.end( ); ++it )
		if( ( it->iLength == iLength ) && ( it->dSamplerate == dSamplerate ) )
		{
			m_vItems.erase( it );
			return;
		}

	ITA_EXCEPT1( INVALID_PARAMETER, "No segmentation of the given length contained" );
}

std::string FSegCollection::toString( ) const
{
	if( m_vItems.empty( ) )
		return "Empty filter segmentation collection";

	std::string s = "Filter segmentatation collection:\n\n";
	for( std::vector<Item>::const_iterator cit = m_vItems.begin( ); cit != m_vItems.end( ); ++cit )
		s += IntToString( cit->iLength ) + ":\t" + fseg_str( cit->vSeg ) + "\n";

	return s;
}
