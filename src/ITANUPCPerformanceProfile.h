#ifndef __PERFORMANCEPROFILE_H__
#define __PERFORMANCEPROFILE_H__

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER

#	include <ITASerializationBuffer.h>
#	include <QSX.h>
#	include <limits>
#	include <stdio.h>
#	include <string>

// Vorw�rtsdeklarationen
class OperationMeasurementTable;
class StageMeasurementTable;

class PerformanceProfile
{
public:
	// Standard-Konstruktor
	PerformanceProfile( );

	// Lade-Konstruktor
	PerformanceProfile( std::string sFilename );

	// Deserialisierungs-Konstruktor
	PerformanceProfile( ITASerializationBuffer& sb );

	// Destruktor
	~PerformanceProfile( );

	// In Datei schreiben
	void store( std::string sFilename );

	// Serialisierung
	void write( ITASerializationBuffer& sb );

	// Kommentar zur�ckgeben
	std::string getComment( ) const { return sComment; }

	// Kommentar setzen
	void setComment( const std::string& sComment ) { this->sComment = sComment; }

	// Messwerttabelle f�r Operationen dem Profil hinzuf�gen
	// (Hinweis: Falls schon eine solche vorhanden ist, wird sie �berschrieben)
	OperationMeasurementTable* createOMT( unsigned int uiStartExponent, unsigned int uiEndExponent );

	// Zeiger auf die Messwerttabelle der Operationen zur�ckgeben
	// (R�ckgabewert: 0, fall keine solche Tabelle im Profil enthalten ist)
	OperationMeasurementTable* getOMT( ) const { return pOMT; }

	// Messwerttabelle der Operationen entfernen
	void removeOMT( );

	// Messwerttabelle f�r Stufen dem Profil hinzuf�gen
	// (Hinweis: Falls schon eine solche vorhanden ist, wird sie �berschrieben)
	StageMeasurementTable* createSMT( unsigned int uiStartExponent, unsigned int uiEndExponent, unsigned int uiMaxMultiplicity );

	// Zeiger auf die Messwerttabelle der Stufen zur�ckgeben
	// (R�ckgabewert: 0, fall keine solche Tabelle im Profil enthalten ist)
	StageMeasurementTable* getSMT( ) const { return pSMT; }

	// Messwerttabelle der Stufen entfernen
	void removeSMT( );

	std::string toString( ) const;

private:
	std::string sComment;
	OperationMeasurementTable* pOMT;
	StageMeasurementTable* pSMT;
};

// Tabelle f�r Messwerte (Einzelmessungen: FFT, IFFT, etc.)
class OperationMeasurementTable
{
public:
	// Operationen
	enum
	{
		FFT = 0,
		VADD, // Vektoraddition (FastMath: fm_add)
		SMUL, // Skalarmultiplikation (FastMath: fm_mul)
		CMUL, // Komplexwertige, komponentenweise Vektormultiplikation (FastMath: fm_cmulx)
		IFFT
	};

	// Unteren Grenze des Messbereich (Zweierexponent) zur�ckgeben
	unsigned int getStartExponent( ) const { return a; }

	// Oberen Grenze des Messbereich (Zweierexponent) zur�ckgeben
	unsigned int getEndExponent( ) const { return b; }

	// Laufzeit zur�ckgeben
	float getRuntime( unsigned int uiOperation, unsigned int uiExponent ) const { return data[uiOperation * n + ( uiExponent - a )]; }

	// Laufzeit setzen
	void setRuntime( unsigned int uiOperation, unsigned int uiExponent, float fRuntime ) { data[uiOperation * n + ( uiExponent - a )] = fRuntime; }

	std::string toString( ) const;

	// DAT-Format f�r gnuplot
	std::string toDataString( ) const;

private:
	unsigned int a, b, n; // Start-/Endexponent; Anzahl Exponenten
	float* data;

	// Standard-Konstruktor
	OperationMeasurementTable( unsigned int uiStartExponent, unsigned int uiEndExponent );

	// Lade-Konstruktor
	OperationMeasurementTable( QSX::XMLNode* root );

	// Deserialisierungs-Konstruktor
	OperationMeasurementTable( ITASerializationBuffer& sb );

	// Destruktor
	~OperationMeasurementTable( );

	// Serialisieren
	void store( QSX::XMLNode* parent );

	// Serialisierung
	void write( ITASerializationBuffer& sb );

	friend class PerformanceProfile;
};

// Complete Stage Measurements Tabelle (Messungen der Stufen f�r Exponenten/Vielfachheiten)
class StageMeasurementTable
{
public:
	// Unteren Grenze des Messbereich (Zweierexponent) zur�ckgeben
	unsigned int getStartExponent( ) const { return a; }

	// Oberen Grenze des Messbereich (Zweierexponent) zur�ckgeben
	unsigned int getEndExponent( ) const { return b; }

	// Maximale Vielfachheit zur�ckgeben
	unsigned int getMaxMultiplicity( ) const { return m; }

	// Laufzeit zur�ckgeben
	float getRuntime( unsigned int uiExponent, unsigned int uiMultiplicity ) const { return data[( uiMultiplicity - 1 ) * n + ( uiExponent - a )]; }

	// Laufzeit setzen
	void setRuntime( unsigned int uiExponent, unsigned int uiMultiplicity, float fRuntime ) { data[( uiMultiplicity - 1 ) * n + ( uiExponent - a )] = fRuntime; }

	std::string toString( ) const;

private:
	unsigned int a, b, n, m; // Start-/Endexponent; Anzahl Exponenten; Maximale Vielfachheit
	float* data;

	// Standard-Konstruktor
	StageMeasurementTable( unsigned int uiStartExponent, unsigned int uiEndExponent, unsigned int uiMaxMultiplicity );

	// Lade-Konstruktor
	StageMeasurementTable( QSX::XMLNode* root );

	// Deserialisierungs-Konstruktor
	StageMeasurementTable( ITASerializationBuffer& sb );

	// Destruktor
	~StageMeasurementTable( );

	// Serialisieren
	void store( QSX::XMLNode* parent );

	// Serialisierung
	void write( ITASerializationBuffer& sb );

	friend class PerformanceProfile;
};

#endif // ITA_CONVOLUTION_NUPCONV_WITH_PERFORMANCE_LOGGER

#endif // IW
