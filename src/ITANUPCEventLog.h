#ifndef IW_ITA_NUP_CONV_EVENTLOG_H__
#define IW_ITA_NUP_CONV_EVENTLOG_H__

// STL includes
#include <ITACriticalSection.h>
#include <ITAHPT.h>
#include <ITANUPConvolution.h>
#include <string>
#include <vector>

class ITANUPCEvent
{
public:
	// Eigenschaften
	static const unsigned int CONV_EVENT_WARNING        = 1;
	static const unsigned int CONV_EVENT_ERROR          = 2;
	static const unsigned int CONV_EVENT_CRITICAL_ERROR = 3;

	ITATimerTicks ttTime;  // RDTSC Zeit an dem das Ereignis eintrat
	ITANUPC::CYCLE cycle;  // Falter-Zyklus in dem das Ereignis auftrat
	std::string sLocation; // Ort/Modul indem das Ereignis auftrat
	std::string sText;     // Beschreibung des Ereignisses
	unsigned int uiFlags;  // Eigenschaften

	ITANUPCEvent( );
	ITANUPCEvent( ITATimerTicks ttTime, ITANUPC::CYCLE cycle, const std::string& sLocation, const std::string& sText, unsigned int uiFlags = 0 );
};

class ITANUPCEventLog
{
public:
	ITANUPCEventLog( unsigned int uiMaxEntries = 65536 );

	void clear( );

	void addEvent( const ITANUPCEvent& e );
	void addEvent( ITATimerTicks ttTime, ITANUPC::CYCLE cycle, const std::string& sLocation, const std::string& sText, unsigned int uiFlags = 0 );

	// Log mit einem anderen zusammenführen
	void join( ITANUPCEventLog& l );

	std::string toString( );

	// In Datei schreiben
	void toFile( const std::string& sFilename );

	// In XML-Datei schreiben
	void toXMLFile( const std::string& sFilename );

private:
	std::vector<ITANUPCEvent> m_vEntries;
	ITACriticalSection m_csEntries;
	bool bLiveOutput;
};

#endif
