#ifndef __OUTPUTBUFFER_H__
#define __OUTPUTBUFFER_H__

#include <ITACriticalSection.h>

/*
   Die Klasse OutputBuffer realisiert den Ausgabepuffer des Falters.
   Er hat eine feste Gr��e und wird f�r eine feste Anzahl von Kan�len
   erzeugt. Eingabedaten werden Block- und Kanalweise �bergeben.
   Eingabedaten werden nicht einfach hineinkopiert, sondern sie werden
   eingemischt (sampleweise Addition). Die Eingabedaten werden in
   unterschiedlichen L�ngen eingemischt (je nach Stufenl�nge).
   Die Ausgabedaten werden immer in Blockl�nge entnommen.
   Der Ausgabecursor wird daher immer in Blockl�nge-Schritten erh�ht.
   */

class COutputBuffer
{
public:
	static COutputBuffer* pInstance;
	unsigned int uiCursor;

	// Konstruktor
	// Wichtig: Die L�nge mu� Vielfaches der Blockl�nge sein!
	COutputBuffer( unsigned int uiChannels, unsigned int uiBlocklength, unsigned int uiSize );

	// Destruktor
	~COutputBuffer( );

	// Puffer zur�cksetzen
	void Reset( );

	//(Ausgabe)Daten lesen
	// Info: Hier wird immer genau eine Blockl�nge kopiert!
	void Get( float* pfDest, unsigned int uiChannel, float fGain = 1.0 );

	/* Daten hinmischen

	   Parameter: pfDest     Zielbuffer
	   uiChannel  Kanal
	   uiOffset	 (Lese)Position
	   uiLength   L�nge (Anzahl Samples)
	   */
	void Put( const float* pfSource, unsigned int uiChannel, unsigned int uiOffset, unsigned int uiLength );

	/* Daten an aktueller Position einmischen
	   (Ausgabe erfolgt dann direkt im n�chsten Zyklus. Ben�tigt f�r BINAURAL-Quellen)

	   Parameter: pfDest     Zielbuffer
	   uiChannel  Kanal
	   uiLength   L�nge (Anzahl Samples)
	   */
	void PutAtCursor( const float* pfSource, unsigned int uiChannel, unsigned int uiLength );

	// (Lese)Cursor weitersetzen
	void IncrementCursor( );

private:
	unsigned int uiChannels;
	unsigned int uiBlocklength;
	unsigned int uiSize;
	float** ppfData;
	ITACriticalSection csData;

	// Aufr�umen (Speicher freigeben)
	void tidyup( );
};

#endif