#include "ITANUPCFade.h"

#include <cmath>

void fade_in( float* pfData, unsigned int uiFadeLength, unsigned int uiFadeFunction )
{
	if( uiFadeLength == 0 )
		return;

	if( uiFadeFunction == ITANUPC::COSINE_SQUARE )
	{
		const double halfPi = 1.5707963267948966192;
		double omega        = halfPi / (double)uiFadeLength;
		for( unsigned int i = 0; i < uiFadeLength; i++ )
		{
			double c    = cos( omega * (double)i );
			float alpha = (float)( c * c );
			pfData[i] *= ( 1 - alpha );
		}
	}
	else // Standard: Rampe
		for( unsigned int i = 0; i < uiFadeLength; i++ )
		{
			float alpha = (float)i / (float)uiFadeLength;
			pfData[i] *= alpha;
		}
}

// Ein Zeitbereichsignal ausblenden
void fade_out( float* pfData, unsigned int uiFadeLength, unsigned int uiFadeFunction )
{
	if( uiFadeLength == 0 )
		return;

	if( uiFadeFunction == ITANUPC::COSINE_SQUARE )
	{
		const double halfPi = 1.5707963267948966192;
		double omega        = halfPi / (double)uiFadeLength;
		for( unsigned int i = 0; i < uiFadeLength; i++ )
		{
			double c    = cos( omega * (double)i );
			float alpha = (float)( c * c );
			pfData[i] *= alpha;
		}
	}
	else // Standard: Rampe
		for( unsigned int i = 0; i < uiFadeLength; i++ )
		{
			float alpha = (float)i / (float)uiFadeLength;
			pfData[i] *= ( 1 - alpha );
		}
}

void crossfade( float* pfDest, const float* pfSource, unsigned int uiCrossfadeLength, unsigned int uiFadeFunction )
{
	if( uiCrossfadeLength == 0 )
		return;

	if( uiFadeFunction == ITANUPC::COSINE_SQUARE )
	{
		const double halfPi = 1.5707963267948966192;
		double omega        = halfPi / (double)uiCrossfadeLength;
		for( unsigned int i = 0; i < uiCrossfadeLength; i++ )
		{
			double c    = cos( omega * (double)i );
			float alpha = (float)( c * c );
			pfDest[i] *= ( 1 - alpha );
			pfDest[i] += ( pfSource[i] * alpha );
		}
	}
	else // Standard: Rampe
		for( unsigned int i = 0; i < uiCrossfadeLength; i++ )
		{
			float alpha = (float)i / (float)uiCrossfadeLength;
			pfDest[i] *= alpha;
			pfDest[i] += ( pfSource[i] * ( 1 - alpha ) );
		}
}
