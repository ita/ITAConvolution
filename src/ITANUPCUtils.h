#ifndef _UTILS_H_
#define _UTILS_H_

#include <vector>

// Algorithmus zur sicheren Bestimmung des Exponenten einer Ganzzahl
// zur Basis 2. (Sehr schnell! Hardcoded!)
int ITANUPCGetExp2( unsigned int x );

// N�chstgr��eres Vielfaches von 4 einer Zahl zur�ckgeben
unsigned int ITANUPCUMul4( unsigned int x );

// N�chstgr��ere 2er-Potenz zu einer Zahl bestimmen (Beispiel: 16 f�r x=11)
// (Hinweis: Diese Funktion stammt aus den ISPLUtils)
unsigned int ITANUPCNextPow2( unsigned int x );

// Float-Buffer mischen
void ITANUPCFVMix( const std::vector<float*>& vBufs, unsigned int uiBufLength );

//// Variante f�r Zeiger auf Zeiger auf Arrays
///* Erl�uterung: Warum float***? array_ptrs ist ein Array ("*" Nr.1) von
//                Zeigern ("*" Nr.2) auf float-Arrays ("*" Nr.3)!
//				Diese Variante wird verwendet f�r Arrays von variablen Zeigern */
// void mix(float*** array_ptrs_ptrs, unsigned int num_arrays, unsigned int array_dim);

// Test ob Float-Buffer nur Nullen enth�lt
// (R�ckgabe: true => Nur Nullen)
bool ITANUPCFCheckZero( const float* pfData, int iCount );

// Testet ob ein Pointer auf 16-Byte ausgerichtet ist
bool ITANUPCIsAligned16Byte( void* ptr );

#endif // _UTILS_H_