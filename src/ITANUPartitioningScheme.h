#ifndef IW_ITA_NUPC_PARTITIONING_SCHEME
#define IW_ITA_NUPC_PARTITIONING_SCHEME

#include <string>
#include <vector>

// Data class for parameters of non-uniform segments
class CUPartition
{
public:
	int iOffset;       //!< Offset in overall impulse response at which this segment begins
	int iPartLength;   //!< Part length in number of filter coefficients
	int iMultiplicity; //!< Multiplicity in number of filter coefficients
	int iLength;       //!< Overall length in number of filter coefficients
	int iLevel;        //!< Index of segment in non-uniform segmentation

	/*
	 *  Spielraum der Berechnungszeit in Anzahl Blocklängen, d.h. die Zeitspanne
	 *  vom frühstmöglichen Zeitpunkt der Berechnung der Stufe, bis zum spätest
	 *  möglichen. Wichtiges Feld! Wird zur Bestimmung der Deadlines von Tasks genutzt.
	 *  iClearance = 0 -> Task muss sofort nach seiner Erzeugung gerechnet werden,
	 *                    da er im nächstfolgenden Zyklus bereits Teil der Ausgabe ist.
	 */
	int iClearance;

	inline CUPartition( ) : iOffset( 0 ), iPartLength( 0 ), iMultiplicity( 0 ), iClearance( 0 ), iLevel( 0 ) { };

	inline CUPartition( const int iOffset, const int iPartLength, const int iMultiplicity )
	    : iOffset( iOffset )
	    , iPartLength( iPartLength )
	    , iMultiplicity( iMultiplicity )
	    , iClearance( -1 )
	    , iLevel( 0 )
	{
		iLength = iPartLength * iMultiplicity;

		//@todo: define clearence
	};
};

// Non-uniform partition
class NUPartition
{
public:
	int iLength;                         //!< Overall length in number of filter coefficients
	int iNumSegments;                    //!< Number of segment
	std::vector<CUPartition> voSegments; //!< Segment list

	//! Creates a non-uniform partitioned filter with given parts and length
	/**
	 * Groups subsequent parts.
	 */
	NUPartition( const std::vector<int>& vParts );

	std::string ToString( ) const;
};

#endif // IW_ITA_NUPC_PARTITIONING_SCHEME
