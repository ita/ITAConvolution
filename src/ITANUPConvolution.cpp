#include "ITANUPConvolutionImpl.h"

#include <ITAAtomicPrimitives.h>
#include <ITAException.h>
#include <ITAFastMath.h>
#include <ITAHPT.h>
#include <ITANUPConvolution.h>

unsigned int uiConvolutioEngineLibraryVersionFlags = 0;
ITAAtomicBool bInitLibrary                         = false;

void InitializeITANUPCLibrary( )
{
	if( !bInitLibrary )
	{
		// ISPL-Modul f�r die Hochgenaue Zeitmessung initialisieren
		ITAHPT_init( );

		// FastMath-Modul initialisieren
		fm_init( );

		bInitLibrary = true;
	}
}

ITANUPC::IConvolution::IConvolution( ) { };

ITANUPC::IConvolution::~IConvolution( ) { };

// Factory method
ITANUPC::IConvolution* ITANUPC::IConvolution::Create( const CConvolutionParameter& oParams )
{
	// Bibliothek initialisieren
	InitializeITANUPCLibrary( );

	// Instanz der Implementierungsklasse erzeugen
	ITANUPC::IConvolution* pInstance = (ITANUPC::IConvolution*)new ITANUPConvolutionImpl( oParams );
	if( !pInstance )
		ITA_EXCEPT1( UNKNOWN, "An internal error occured" );

	return pInstance;
}

unsigned int ITANUPC::IConvolution::GetFlags( )
{
	// Bibliothek initialisieren
	InitializeITANUPCLibrary( );

	return uiConvolutioEngineLibraryVersionFlags;
}
