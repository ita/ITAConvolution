#include "ITANUPCEventLog.h"

#include <ITAASCIITable.h>
#include <ITAException.h>
#include <ITAHPT.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>


#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
#	include <QSX.h>
#endif

ITANUPCEvent::ITANUPCEvent( ) : ttTime( 0 ), cycle( 0 ), sLocation( "<unknown module>" ), sText( "<unknown event>" ) {}

ITANUPCEvent::ITANUPCEvent( ITATimerTicks ttTime, ITANUPC::CYCLE cycle, const std::string& sLocation, const std::string& sText, unsigned int uiFlags )
{
	this->ttTime    = ttTime;
	this->cycle     = cycle;
	this->sLocation = sLocation;
	this->sText     = sText;
	this->uiFlags   = uiFlags;
}


// --------------------------------------------------------------------


ITANUPCEventLog::ITANUPCEventLog( unsigned int uiMaxEntries )
{
	m_vEntries.reserve( uiMaxEntries );
	bLiveOutput = true;
}

void ITANUPCEventLog::clear( )
{
	ITACriticalSectionLock oLock( m_csEntries );
	m_vEntries.clear( );
}

void ITANUPCEventLog::addEvent( const ITANUPCEvent& e )
{
	ITACriticalSectionLock oLock( m_csEntries );

	m_vEntries.push_back( e );

	if( bLiveOutput )
	{
		// Live output
		ITATimerTicks ttRef = m_vEntries.front( ).ttTime;
		double dTime        = toSeconds( e.ttTime - ttRef );
		printf( "%0.3fms [%s] %s\n", dTime * 1000, e.sLocation.c_str( ), e.sText.c_str( ) );
	}
}

void ITANUPCEventLog::addEvent( ITATimerTicks ttTime, ITANUPC::CYCLE cycle, const std::string& sLocation, const std::string& sText, unsigned int uiFlags )
{
	ITANUPCEvent e( ttTime, cycle, sLocation, sText, uiFlags );
	addEvent( e );
}

bool cmpEvent( const ITANUPCEvent& e1, const ITANUPCEvent& e2 )
{
	return ( e1.ttTime < e2.ttTime );
}

void ITANUPCEventLog::join( ITANUPCEventLog& l )
{
	ITACriticalSectionLock oLock( m_csEntries );
	ITACriticalSectionLock oLockSrc( l.m_csEntries );

	m_vEntries.reserve( m_vEntries.size( ) + l.m_vEntries.size( ) );
	std::copy( l.m_vEntries.begin( ), l.m_vEntries.end( ), std::back_inserter( m_vEntries ) );
	std::sort( m_vEntries.begin( ), m_vEntries.end( ), cmpEvent );
}

std::string ITANUPCEventLog::toString( )
{
	ITACriticalSectionLock oLock( m_csEntries );

	if( m_vEntries.empty( ) )
		return "";

	// Zun�chst sortieren
	std::sort( m_vEntries.begin( ), m_vEntries.end( ), cmpEvent );

	ITATimerTicks ttRef = m_vEntries.front( ).ttTime;
	double dPrev        = 0;

	ITAASCIITable T( (unsigned int)m_vEntries.size( ), 5 );
	T.setColumnTitle( 0, "Time\n[ms]" );
	T.setColumnJustify( 0, ITAASCIITable::RIGHT );
	T.setColumnTitle( 1, "Delta\n[ms]" );
	T.setColumnJustify( 1, ITAASCIITable::RIGHT );
	T.setColumnTitle( 2, "Cycle" );
	T.setColumnJustify( 2, ITAASCIITable::RIGHT );
	T.setColumnTitle( 3, "Location" );
	T.setColumnTitle( 4, "Description" );

	for( unsigned int i = 0; i < (unsigned int)m_vEntries.size( ); i++ )
	{
		double dTime = toSeconds( m_vEntries[i].ttTime - ttRef );

		T.setContent( i, 0, DoubleToString( dTime * 1000, 3 ) );
		T.setContent( i, 1, DoubleToString( ( dTime - dPrev ) * 1000, 3 ) );

		dPrev = dTime;

		T.setContent( i, 2, Int64ToString( m_vEntries[i].cycle ) );
		if( !m_vEntries[i].sLocation.empty( ) )
			T.setContent( i, 3, m_vEntries[i].sLocation );
		T.setContent( i, 4, m_vEntries[i].sText );
	}

	return T.toString( );
}

void ITANUPCEventLog::toFile( const std::string& sFilename )
{
	ITACriticalSectionLock oLock( m_csEntries );

	std::ofstream file;
	file.open( sFilename.c_str( ) );
	file << toString( );
	file.close( );
}

void ITANUPCEventLog::toXMLFile( const std::string& sFilename )
{
	ITACriticalSectionLock oLock( m_csEntries );

#ifdef ITA_CONVOLUTION_NUPCONV_WITH_XML_READER

	QSX::XMLNode* root = 0;
	std::map<std::string, int> indices;
	int index_max = 0;

	try
	{
		root = QSX::XMLNode::createEmptyXMLDocument( "eventlog" );

		if( !m_vEntries.empty( ) )
		{
			// Zun�chst sortieren
			std::sort( m_vEntries.begin( ), m_vEntries.end( ), cmpEvent );

			ITATimerTicks ttRef = m_vEntries.front( ).ttTime;
			double dPrev        = 0;

			for( unsigned int i = 0; i < (unsigned int)m_vEntries.size( ); i++ )
			{
				// Generate the module index
				int index                              = -1;
				std::map<std::string, int>::iterator I = indices.find( toLowercase( m_vEntries[i].sLocation ) );
				if( I != indices.end( ) )
					index = I->second;
				else
					indices.insert( std::pair<std::string, int>( toLowercase( m_vEntries[i].sLocation ), index = index_max++ ) );

				QSX::XMLNode* child = root->addChild( "event" );
				if( index != -1 )
					child->setAttribute( "color", index + 1 );

				double dTime = toSeconds( m_vEntries[i].ttTime - ttRef );

				child->addChild( "time", DoubleToString( dTime * 1000, 3 ) )->setAttribute( "unit", "ms" );
				child->addChild( "delta", DoubleToString( ( dTime - dPrev ) * 1000, 3 ) )->setAttribute( "unit", "ms" );

				dPrev = dTime;

				child->addChild( "cycle", Int64ToString( m_vEntries[i].cycle ) );
				if( !m_vEntries[i].sLocation.empty( ) )
					child->addChild( "location", m_vEntries[i].sLocation );

				child->addChild( "text", m_vEntries[i].sText );
			}
		}

		QSX::XMLNode::storeXMLDocument( root, sFilename );
		delete root;
	}
	catch( QSX::XMLException& e )
	{
		// XMLException in ITAException umleiten...
		delete root;
		ITA_EXCEPT1( IO_ERROR, e.toString( ) );
	}

#else // ITA_CONVOLUTION_NUPCONV_WITH_XML_READER
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Read/write of filter segmentation with XML file format not supported, refusing to load " + sFilename );
#endif
}
