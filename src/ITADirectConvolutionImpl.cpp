#include "ITADirectConvolutionImpl.h"

#include <ITABaseDefinitions.h>
#include <ITANumericUtils.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>

ITADirectConvolutionImpl::ITADirectConvolutionImpl( const int src1length, const int src2length, const int ) : m_pConvolver( NULL ), m_pFilter( NULL )
{
	int iSignallength;
	int iFilterlength;

	// K�rzere Eingabe = Signal, die andere das Filter
	if( src1length < src2length )
	{
		iSignallength = src2length;
		iFilterlength = src1length;
		m_iFilter     = 1;
	}
	else
	{
		iSignallength = src1length;
		iFilterlength = src2length;
		m_iFilter     = 2;
	}

	// Blockl�nge = N�chste Zweierpotenz gr��er der Filterl�nge
	m_iBlockLength = (int)nextPow2( (unsigned int)iFilterlength );
	m_pConvolver   = new ITAUPConvolution( m_iBlockLength, iFilterlength );
	m_pConvolver->SetFilterExchangeFadingFunction( ITABase::FadingFunction::SWITCH );

	// Filter holen
	m_pFilter = m_pConvolver->RequestFilter( );
}

ITADirectConvolutionImpl::~ITADirectConvolutionImpl( )
{
	delete m_pConvolver;
}

void ITADirectConvolutionImpl::Convolve( const float* src1, const int src1length, const float* src2, const int src2length, float* dest, const int destlength )
{
	if( !m_pConvolver )
		return;

	const float* pfInput;
	int iInputLength;

	// Filter laden
	if( m_iFilter == 1 )
	{
		m_pFilter->Load( src1, src1length );
		pfInput      = src2;
		iInputLength = src2length;
	}
	else
	{
		m_pFilter->Load( src2, src2length );
		pfInput      = src1;
		iInputLength = src1length;
	}
	m_pConvolver->ExchangeFilter( m_pFilter );

	// Iterativ durchfalten
	int k = 0;
	while( k < destlength )
	{
		// Restliche Samples in der Eingabe
		int ri = ( std::min )( ( std::max )( iInputLength - k, 0 ), m_iBlockLength );

		// Restliche Samples in der Ausgabe
		int ro = ( std::min )( ( std::max )( destlength - k, 0 ), m_iBlockLength );

		m_pConvolver->Process( pfInput + k, ri, dest + k, ro );
		k += ro;
	}
}
