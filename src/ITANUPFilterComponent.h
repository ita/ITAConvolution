#ifndef IW_ITA_NUP_CONV_FILTERCOMPONENT_H__
#define IW_ITA_NUP_CONV_FILTERCOMPONENT_H__

// Forwards
class IConvolution;
class ITASampleBuffer;
class ITASampleFrame;

//! Klasse f�r 2-Kanal Filterkomponenten
/**
 * \image html Filterkomponente.png
 *
 * Eine Filterkomponente ist ein Array von Filterkoeffizienten, dessen Offset (Position)
 * auf ein festes Segment festgelegt ist. In dieser Falterversion (Binaural) haben alle
 * Filterkomponenten zwei Kan�le.
 *
 * Passt auf eine Stufe!
 * TODO: Genauere Beschreibung
 */
// class ITA_CONVOLUTION_API ITANUFilterComponent
{
	// public:
	//	//! �bergeordneten Falter zur�ckgeben
	//	virtual ITANUPC::IConvolution* getParent() const=0;
	//
	//	//! Startsegment zur�ckgeben
	//	/**
	//	 * Diese Methode gibt den Index des Segments zur�ck,
	//	 * an dem diese Filterkomponente plaziert ist.
	//	 */
	//	virtual unsigned int getStartSegment() const=0;
	//
	//	//! Offset zur�ckgeben
	//	/**
	//	 * Diese Methode gibt den Offset zur�ck,
	//	 * d.h. dessen Position (Startsample) in der Impulsantwort.
	//	 */
	//	virtual unsigned int getOffset() const=0;
	//
	//	//! Anzahl Filterkoeffizienten zur�ckgeben
	//	/**
	//	 * Gibt die Anzahl Filterkoeffizienten (taps) zur�ck
	//	 */
	//	virtual unsigned int getLength() const=0;
	//
	//	//! Zur�ckgeben ob die Filterkomponente f�r die Benutzung zugewiesen ist
	//	/**
	//	 * \return true, falls die Filterkomponente im Falter zur Benutzung zugewiesen ist.
	//	 */
	//	virtual bool isPlacedForUse() const=0;
	//
	//	//! Zur�ckgeben ob die Filterkomponente benutzt wird
	//	/**
	//	 * \return true, falls die Filterkomponente im Falter in Benutzung ist.
	//	 */
	//	virtual bool isInUse() const=0;
	//
	//	//! Filterkomponente freigeben
	//	/**
	//	 * Gibt die Filterkomponente zur Freigabe frei.
	//	 * Die tats�chliche Freigabe erfolgt, wenn sie nicht mehr benutzt wird.
	//	 */
	//	virtual void release()=0;
	//
	//	//! Filterkoeffizienten laden
	//	/**
	//	 * L�dt neue Filterkoeffizienten.
	//	 * Dies ist nur m�glich, wenn die Filterkomponente unbenutzt ist.
	//	 * ui{Left|Right}DataLength dienen dazu auch aus k�rzeren (<getLength())
	//	 * Quellpuffern laden zu k�nnen. Die restlichen Taps werden dann intern Null gesetzt.
	//	 *
	//	 * Hinweis: Dies f�hrt entsprechende Konditionierungsarbeiten durch (FFTs, etc.)
	//	 */
	//	virtual void load(const float* pfLeftData, unsigned int uiLeftDataLength,
	//		              const float* pfRightData, unsigned int uiRightDataLength)=0;
	//
	//	//! Filterkoeffizienten laden
	//	/**
	//	 * L�dt neue Filterkoeffizienten.
	//	 * Dies ist nur m�glich, wenn die Filterkomponente unbenutzt ist.
	//	 * Er darf nicht l�nger als die maximale Filterl�nge sein,
	//	 * darf aber durchaus k�rzer sein.
	//	 * Die restlichen Taps werden dann intern mit Null gesetzt.
	//	 *
	//	 * Hinweis: Dies f�hrt entsprechende Konditionierungsarbeiten durch (FFTs, etc.)
	//	 */
	//	virtual void load(const ITASampleBuffer* psbLeftFilter,
	//		              const ITASampleBuffer* psbRightFilter)=0;
	//
	//	//! Filterkoeffizienten laden
	//	/**
	//	 * L�dt neue Filterkoeffizienten.
	//	 * Dies ist nur m�glich, wenn die Filterkomponente unbenutzt ist.
	//	 * Der SampleFrame muss genau zwei Kan�le haben.
	//	 * Er darf nicht l�nger als die maximale Filterl�nge sein,
	//	 * darf aber durchaus k�rzer sein.
	//	 * Die restlichen Taps werden dann intern mit Null gesetzt.
	//	 *
	//	 * Hinweis: Dies f�hrt entsprechende Konditionierungsarbeiten durch (FFTs, etc.)
	//	 */
	//	virtual void load(const ITASampleFrame* psfData)=0;
	//
	// protected:
	//	// Gesch�tzer Destruktor
	//	virtual ~ITANUFilterComponent() { };
	//};

#endif // IW_ITA_NUP_CONV_FILTERCOMPONENT_H__
