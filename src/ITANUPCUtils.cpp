#include "ITANUPCUtils.h"

#include <ITAFastMath.h>

int ITANUPCGetExp2( unsigned int x )
{
	switch( x )
	{
		case( 1 << 0 ):
			return 0;
		case( 1 << 1 ):
			return 1;
		case( 1 << 2 ):
			return 2;
		case( 1 << 3 ):
			return 3;
		case( 1 << 4 ):
			return 4;
		case( 1 << 5 ):
			return 5;
		case( 1 << 6 ):
			return 6;
		case( 1 << 7 ):
			return 7;
		case( 1 << 8 ):
			return 8;
		case( 1 << 9 ):
			return 9;
		case( 1 << 10 ):
			return 10;
		case( 1 << 11 ):
			return 11;
		case( 1 << 12 ):
			return 12;
		case( 1 << 13 ):
			return 13;
		case( 1 << 14 ):
			return 14;
		case( 1 << 15 ):
			return 15;
		case( 1 << 16 ):
			return 16;
		case( 1 << 17 ):
			return 17;
		case( 1 << 18 ):
			return 18;
		case( 1 << 19 ):
			return 19;
		case( 1 << 20 ):
			return 20;
		case( 1 << 21 ):
			return 21;
		case( 1 << 22 ):
			return 22;
		case( 1 << 23 ):
			return 23;
		case( 1 << 24 ):
			return 24;
		case( 1 << 25 ):
			return 25;
		case( 1 << 26 ):
			return 26;
		case( 1 << 27 ):
			return 27;
		case( 1 << 28 ):
			return 28;
		case( 1 << 29 ):
			return 29;
		case( 1 << 30 ):
			return 30;
		case( 1 << 31 ):
			return 31;
	}
	return -1;
}

unsigned int ITANUPCUMul4( unsigned int x )
{
	unsigned int k = x / 4;
	return ( ( x % 4 ) == 0 ? x : ( k + 1 ) * x );
}

unsigned int ITANUPCNextPow2( unsigned int x )
{
	// Trivialer Fall:
	if( x == 0 )
		return 1;

	bool s = false;           // Suchflag
	int n  = sizeof( x ) * 8; // Anzahl der Bits im Datentyp
	int k  = n - 1;           // Z�hler

	// Erstes 1-Bit von links nach rechts (MSB->LSB) suchen
	// (Muss gefunden werden, da x!=0)
	while( ( ( x >> k ) & 1 ) == 0 )
		k--;

	unsigned int l = k; // Exponent speichern

	// Weitersuchen
	if( k > 1 )
	{
		k--;
		while( !( s = ( ( ( x >> k ) & 1 ) == 1 ) ) && k > 0 )
			k--;
	}

	// Zweites 1-Bit gefunden? Dann keine Zweierpotenz...
	return ( s ? ( (unsigned int)1 ) << ( l + 1 ) : x );
}

void ITANUPCFVMix( const std::vector<float*>& vBufs, unsigned int uiBufLength )
{
	for( size_t i = 1; i < vBufs.size( ); i++ )
		fm_add( vBufs[0], vBufs[i], uiBufLength );
}

bool ITANUPCFCheckZero( const float* pfData, int iCount )
{
	for( int i = 0; i < iCount; i++ )
		if( pfData[i] != 0 )
			return false;
	return true;
}

bool ITANUPCIsAligned16Byte( void* ptr )
{
	return ( reinterpret_cast<size_t>( ptr ) % 16 == 0 );
}
