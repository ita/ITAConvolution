#include "ITANUPCHelpers.h"

#include <ITAStringUtils.h>
#include <iostream>

void show( float* in, unsigned int n )
{
	for( unsigned int i = 0; i < n; i++ )
		printf( "%0.6f\n", in[i] );
}

void show( float* ri, float* ii, unsigned int n )
{
	for( unsigned int i = 0; i < n; i++ )
		printf( "%0.6f + %0.6fi\n", ri[i], ii[i] );
}

inline void cmulf( float* dest, const float* src, unsigned int n )
{
	float re;
	for( unsigned int i = 0; i < n; i++ )
	{
		// re1 = re1*re2 - im1*im2
		// im1 = re1*im2 + re2*im1

		// Aktuellen Realteil in dest sichern
		re                   = dest[( i << 1 )];
		dest[( i << 1 )]     = dest[( i << 1 )] * src[( i << 1 )] - dest[( i << 1 ) + 1] * src[( i << 1 ) + 1];
		dest[( i << 1 ) + 1] = re * src[( i << 1 ) + 1] + dest[( i << 1 ) + 1] * src[( i << 1 )];
	}
}

int fwrite_uchar( FILE* file, unsigned int i )
{
	unsigned char j = (unsigned char)i;
	return ( fwrite( &j, 1, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_ushort( FILE* file, unsigned int i )
{
	unsigned short j = (unsigned short)i;
	return ( fwrite( &j, 2, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_uint( FILE* file, unsigned int i )
{
	return ( fwrite( &i, 4, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_float( FILE* file, float x )
{
	return ( fwrite( &x, 4, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_sz( FILE* file, char* s )
{
	return ( fwrite( s, strlen( s ) + 1, 1, file ) == 1 ) ? 0 : 1;
}

int fwrite_sz( FILE* file, std::string s )
{
	return ( fwrite( s.c_str( ), s.length( ) + 1, 1, file ) == 1 ) ? 0 : 1;
}

void TRACE_BUFFER( char* name, float* src, unsigned int count, unsigned int digits )
{
	std::cout << name << ": " << FloatArrayToString( src, count, digits ) << std::endl;
}

void TRACE_BUFFER( char* name, double* src, unsigned int count, unsigned int digits )
{
	std::cout << name << ": " << DoubleArrayToString( src, count, digits ) << std::endl;
}
