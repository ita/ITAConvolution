#ifndef IW_ITA_NUP_CONV_FILTERIMPL
#define IW_ITA_NUP_CONV_FILTERIMPL

#include "ITANUPCUFilter.h"
#include "ITANUPConvolutionImpl.h"

#include <ITACriticalSection.h>
#include <ITANUPFilter.h>
#include <algorithm>
#include <atomic>
#include <cassert>

class NUPartition;

class FilterImpl : public ITANUPC::IFilter
{
public:
	inline FilterImpl( ITANUPConvolutionImpl* pParent ) : m_pParent( pParent ), m_pScheme( pParent->getPartitioningScheme( ) )
	{
		assert( m_pParent && m_pScheme );

		m_vpSegments.resize( m_pScheme->iNumSegments, nullptr );
		m_iRefCount = 0;
	};

	inline ~FilterImpl( ) { };

	inline ITANUPC::IConvolution* GetParent( ) const { return m_pParent; };

	inline bool Used( ) const
	{
		for( int s = 0; s < m_pScheme->iNumSegments; s++ )
			if( m_vpSegments[s] && m_vpSegments[s]->IsInUse( ) )
				return true;
		return false;
	};

	inline void Release( )
	{
		ITACriticalSectionLock oLock( m_csReentrance ); // No reentance

		// Alle Segmente freigeben
		for( int s = 0; s < m_pScheme->iNumSegments; s++ )
			m_vpSegments[s]->Release( );

		// if (--m_iRefCount == 0) delete this;
	};

	inline void Zeros( ) { Load( nullptr, 0, nullptr, 0 ); };

	inline void Identity( )
	{
		const float one[1] = { 1 };
		Load( one, 1, one, 1 );
	};

	inline void Load( const float* pfLeftFilterCoeffs, const int iNumLeftFilterCoeffs, const float* pfRightFilterCoeffs, const int iNumRightFilterCoeffs )
	{
		ITACriticalSectionLock oLock( m_csReentrance ); // No reentance

		if( Used( ) )
			ITA_EXCEPT1( INVALID_PARAMETER, "Filter is in use" );

		int iMaxLength = ( std::max )( iNumLeftFilterCoeffs, iNumRightFilterCoeffs );
		if( iMaxLength > m_pScheme->iLength )
			ITA_EXCEPT1( INVALID_PARAMETER, "Maximum filter length exceeded" );

		// INFO: Nullptr or length 0 causes the filter to be set with zeros

		// Über alle Segmente iterieren und Abdeckung prüfen

		int ol( 0 ), or ( 0 );                                       // Offsets
		int rl( iNumLeftFilterCoeffs ), rr( iNumRightFilterCoeffs ); // # verbleibende Koeffizienten

		for( int s = 0; s < m_pScheme->iNumSegments; s++ )
		{
			CUPartition si = m_pScheme->voSegments[s];

			// Anzahl zu ladende Koeffizienten im Segment
			int nl = ( std::min )( rl, si.iLength );
			int nr = ( std::min )( rr, si.iLength );

			if( ( nl > 0 ) || ( nr > 0 ) )
			{
				CUFilter* pComp = m_pParent->RequestUFilter( s );

				pComp->Load( pfLeftFilterCoeffs + ol, nl, pfRightFilterCoeffs + or, nr );
				ol += nl;
				or += nr;
				rl = ( std::max )( 0, iNumLeftFilterCoeffs - ol );
				rr = ( std::max )( 0, iNumRightFilterCoeffs - or );

				// Neue Komponente speichern
				if( m_vpSegments[s] )
					m_vpSegments[s]->Release( );
				m_vpSegments[s] = pComp;
			}
			else
			{
				// Keine Überdeckung
				if( m_vpSegments[s] )
					m_vpSegments[s]->Release( );
				m_vpSegments[s] = nullptr;
			}
		}
	};

private:
	ITANUPConvolutionImpl* m_pParent;
	const NUPartition* m_pScheme;
	std::atomic<int> m_iRefCount;
	std::vector<CUFilter*> m_vpSegments;
	ITACriticalSection m_csReentrance;

	friend class ITANUPConvolutionImpl;
};

#endif // IW_ITA_NUP_CONV_FILTERIMPL_H__
