#include "ITANUPCTaskQueue.h"

#include "ITANUPCStage.h"
#include "ITANUPartitioningScheme.h"

#include <ITANUPConvolution.h>

/*
void TaskPriorityQueue::clear() {
c.clear();
}

bool TaskPriorityQueue::empty() const {
return c.empty();
}

size_t TaskPriorityQueue::size() const {
return c.size();
}

std::vector<Task>::const_iterator TaskPriorityQueue::begin() const {
return c.begin();
}

std::vector<Task>::const_iterator TaskPriorityQueue::end() const {
return c.end();
}
*/

void TaskPriorityQueue::clear( )
{
	c.clear( );
}

bool TaskPriorityQueue::empty( ) const
{
	return c.empty( );
}

size_t TaskPriorityQueue::size( ) const
{
	return c.size( );
}

void TaskPriorityQueue::push( const Task& T )
{
	std::vector<Task>::iterator it;
	for( it = c.begin( ); it != c.end( ); ++it )
		if( TaskOrderDeadlineAsc( )( T, *it ) )
			break;
	c.insert( it, T );
}

void TaskPriorityQueue::pop( )
{
	c.erase( c.begin( ) );
}

Task& TaskPriorityQueue::top( )
{
	return c.front( );
}

std::vector<Task>::const_iterator TaskPriorityQueue::begin( ) const
{
	return c.begin( );
}

std::vector<Task>::const_iterator TaskPriorityQueue::end( ) const
{
	return c.end( );
}

// --------------------------------------------------------------------

TaskQueue::TaskQueue( ) : uiSize( 0 ) {}

void TaskQueue::setup( unsigned int uiNumStages )
{
	vtqQueues.resize( uiNumStages );
}

void TaskQueue::clear( )
{
	ITACriticalSectionLock oLock( csTaskQueues );

	for( std::vector<TaskPriorityQueue>::iterator it = vtqQueues.begin( ); it != vtqQueues.end( ); ++it )
		it->clear( );
	uiSize = 0;
}

unsigned int TaskQueue::size( ) const
{
	return uiSize;
}

void TaskQueue::put( Task& T )
{
	ITACriticalSectionLock oLock( csTaskQueues );
	vtqQueues[T.pStage->m_pPartition->iLevel].push( T );
	uiSize++;
}

/*
Task TaskQueue::pop() {
Task T;
if (!getByDeadlineBeforeLevel(T))
ITA_EXCEPT1(UNKNOWN, "Attept to pop empty task priority queue");
return T;
}
*/

bool TaskQueue::getByDeadlineBeforeLevel( Task& T, int iMinLevel, int iMaxLevel )
{
	ITACriticalSectionLock oLock( csTaskQueues );

	// Unter allen Front-Elementen in allen Priority-Queues das mit der geringsten Deadline suchen
	bool first = true;
	std::vector<TaskPriorityQueue>::iterator i;
	ITANUPC::CYCLE d = 0;
	for( std::vector<TaskPriorityQueue>::iterator j = vtqQueues.begin( ); j != vtqQueues.end( ); ++j )
	{
		if( !j->empty( ) )
		{
			Task& x = j->top( );
			int l   = x.pStage->m_pPartition->iLevel;
			// Ein Task kann nur dann gerechnet werden, wenn seine Stufe frei (ungesperrt) ist
			// oder seine Stufe diesen Task in Unterbrechnung hat:
			int ct = 0; // ID=0 => Kein Task
			if( x.pStage->GetCurrentTask( ) )
				ct = x.pStage->GetCurrentTask( )->ID;
			bool w = ( ( ct == 0 ) || ( ct == x.ID ) );
			if( ( l >= iMinLevel ) && ( l <= iMaxLevel ) && w )
			{
				if( first )
				{
					i     = j;
					d     = x.tDeadline;
					first = false;
				}
				else
				{
					if( x.tDeadline < d )
					{
						i = j;
						d = x.tDeadline;
					}
				}
			}
		}
	}

	if( first )
	{
		// Keinen Task gefunden
		return false;
	}
	else
	{
		// Task gefunden. Jetzt entnehmen
		T = i->top( );
		i->pop( );
		uiSize--;
		return true;
	}
}

std::string TaskQueue::toString( )
{
	ITACriticalSectionLock oLock( csTaskQueues );

	/*
	std::string sResult;
	for (const_iterator cit=begin(); cit!=end(); ++cit)
	sResult += (*cit).toShortString();
	return sResult;
	*/

	if( uiSize == 0 )
		return "Empty task queue";

	std::string s;
	for( std::vector<TaskPriorityQueue>::iterator it = vtqQueues.begin( ); it != vtqQueues.end( ); ++it )
	{
		if( !it->empty( ) )
		{
			s += IntToString( it->top( ).pStage->iPartLength ) + "-Tasks:\n\n";
			for( TaskPriorityQueue::const_iterator cit = it->begin( ); cit != it->end( ); ++cit )
				s += ( *cit ).toShortString( ) + "\n";
			s += "\n";
		}
	}

	return s;
}