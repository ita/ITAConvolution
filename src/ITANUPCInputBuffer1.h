#ifndef __INPUTBUFFER1_H__
#define __INPUTBUFFER1_H__

#include <vector>

/*
   Die Klasse InputBuffer1 realisiert den Eingabepuffer f�r einen Kanal.
   Er hat eine feste Gr��e erzeugt.
   Eingabedaten werden Block- und Kanalweise �bergeben.
   Der Eingabecursor wird immer in Blockl�nge-Schritten erh�ht.
   Daten k�nnen mittels der Methode get entnommen werden.
   */

class CInputBuffer1
{
public:
	// Konstruktor
	// Wichtig: Die L�nge mu� Vielfaches der Blockl�nge sein!
	CInputBuffer1( unsigned int uiBlocklength, unsigned int uiSize );

	// Destruktor
	~CInputBuffer1( );

	// Puffer zur�cksetzen
	void reset( );

	/* Daten lesen

	   Parameter: pfDest     Zielbuffer
	   uiOffset	 (Lese)Position
	   uiLength   L�nge (Anzahl Samples)

	   R�ckgabewert: false, falls die angeforderten Daten nur Stille sind
	   und ausschlie�lich Nullsamples enthalten
	   true,  falls die Daten nicht Stille sind
	   */

	bool get( float* pfDest, unsigned int uiOffset, unsigned int uiLength );

	// Daten hineinlegen
	// Info: Hier wird immer genau eine Blockl�nge kopiert!
	void put( const float* pfSource, float fGain = 1.0 );

	// Daten hineinmischen
	// Info: Hier wird immer genau eine Blockl�nge eingemischt!
	void mix( const float* pfSource, float fGain = 1.0 );

	// (Schreib)Cursor weitersetzen
	void incrementCursor( );

	/* Gibt die Leseposition im Puffer zur�ck, ab der - inklusive des
	   aktuellen Block - die gew�nschte L�nge Samples gelesen werden k�nnen.
	   (F�r Tasks) */
	unsigned int getOffset( unsigned int uiLength );

private:
	unsigned int uiCursor; // Schreibcursor/Basiscursor
	unsigned int m_uiBlocklength;
	unsigned int m_uiSize;
	float* m_pfData;
	std::vector<bool> m_vbSilence;
};

#endif // __INPUTBUFFER1_H__
