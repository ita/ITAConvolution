cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITAConvolutionBenchmarks)

# ######################################################################################################################

add_executable (ITAConvolution_BM_UPConv ITAConvolution_BM_UPConv.cpp)
target_link_libraries (ITAConvolution_BM_UPConv ITAConvolution::ITAConvolution ITADataSources::ITADataSources)

set_property (TARGET ITAConvolution_BM_UPConv PROPERTY FOLDER "Benchmarks/ITAConvolution")

install (TARGETS ITAConvolution_BM_UPConv RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
