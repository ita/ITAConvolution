#include "NUPCFilterComponentBenchmark.h"

#include <ITAFastMath.h>
#include <ITAHPT.h>
#include <ITANUPConvolution.h>
#include <ITAStopWatch.h>
#include <cmath>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <vector>

using namespace std;

// Puffergr��en welche von Interesse sind (0 signalisiert das Ende der Liste}
unsigned int buffersizes[] = { 64, 128, 256, 512, 0 };
// unsigned int buffersizes[] = {256, 0};

// IR-L�ngen in Sekunden welche von Interesse sind (0 signalisiert das Ende der Liste}
// double ir_durations[] = {1.0, 0};
// double ir_durations[] = {0.1, 0.25, 0.5, 1.0, 1.5, 2.0, 0};
double ir_durations[] = { 0.1, 0.25, 0.5, 1.0, 1.5, 2.0, 3.0, 5.0, 7.5, 10.0, 15.0, 20.0, 30.0, 0 };

// IR-L�ngen f�r segmentierten Test
double segload_ir_durations[] = { 0.5, 1.0, 1.5, 2.0, 3.0, 5.0, 7.5, 10.0, 15.0, 0 };

// L�nge des ersten Segments (Samples)
unsigned int first_part_length = 7000; // Samples
const double samplerate        = 44100.0;

void benchmarkFilterComponentCreation( )
{
	vector<double> vIRDurations;
	unsigned int i = 0;
	while( ir_durations[i] != 0 )
		vIRDurations.push_back( ir_durations[i++] );
	vector<unsigned int> vBuffersizes;
	i = 0;
	while( buffersizes[i] != 0 )
		vBuffersizes.push_back( buffersizes[i++] );

	unsigned int n = 30;

	ITAHPT_init( );

	unsigned int j;
	unsigned int k; // Schleifenz�hler
	unsigned int b; // Puffergr��e
	double d;       // Dauer der Impulsantwort
	ITAStopWatch sw;

	vector<vector<double> > results;

	// Zufallsgenerator initialisieren
	srand( (unsigned int)time( NULL ) );

	for( i = 0; i < vBuffersizes.size( ); i++ )
	{
		b = vBuffersizes[i];
		vector<double> v;
		for( j = 0; j < vIRDurations.size( ); j++ )
		{
			d = vIRDurations[j];

			// Anzahl der Samples der IR bestimmen
			unsigned int l = (unsigned int)ceil( d * samplerate );

			// Falter erzeugen
			ITANUPC::IConvolution* pConv = ITANUPC::IConvolution::create( samplerate, 1, b, l );
			float* pfLeft                = fm_falloc( l, false );
			float* pfRight               = fm_falloc( l, false );

			// Speicher mit Zufallszahlen initialisieren
			for( k = 0; k < l; k++ )
				pfLeft[k] = pfRight[k] = (float)rand( ) / (float)RAND_MAX;

			// Filterkomponente erzeugen
			ITANUPC::C* pFC = pConv->createFilterComponent( 0, l, 0, 0, 0, 0 );

			// Mess-Vorlauf zur Einspielung des Systems
			for( k = 0; k < 3; k++ )
				;
			{
				sw.start( );
				ITANUPC::CFilterComponent* pFC = pConv->createFilterComponent( 0, l, pfLeft, l, pfRight, l );
				sw.stop( );
				pFC->destroy( );
			}

			// Eigentliche Messschleife:
			sw.reset( );
			for( k = 0; k < n; k++ )
			{
				sw.start( );
				ITANUPC::CFilterComponent* pFC = pConv->createFilterComponent( 0, l, pfLeft, l, pfRight, l );
				sw.stop( );
				pFC->destroy( );
			}

			// Speicher freigeben und Falter l�schen
			fm_free( pfLeft );
			fm_free( pfRight );
			delete pConv;

			// Infos ausgeben
			printf( "\nbs = %d, dur = %0.2f s = %d samples\nmin = %0.12f s\navg = %0.12f s\nmax = %0.12f s\ncyc = %d\n\n", b, d, l, sw.minimum( ), sw.mean( ),
			        sw.maximum( ), sw.cycles( ) );

			v.push_back( sw.mean( ) );
		}
		results.push_back( v );
	}

	// Ergebnisse ausgeben
	printf( "\n\n\nErzeugen von FCs:\n\n" );
	printf( "IRL / BS \t" );
	for( j = 0; j < vBuffersizes.size( ); j++ )
		printf( "%6d    \t", vBuffersizes[j] );
	printf( "\n" );

	for( i = 0; i < vIRDurations.size( ); i++ )
	{
		printf( "%0.3f s  \t", vIRDurations[i] );
		for( j = 0; j < vBuffersizes.size( ); j++ )
			printf( "%0.6f s  \t", results[j][i] );
		printf( "\n" );
	}
}

void benchmarkFilterComponentLoad( )
{
	vector<double> vIRDurations;
	unsigned int i = 0;
	while( ir_durations[i] != 0 )
		vIRDurations.push_back( ir_durations[i++] );
	vector<unsigned int> vBuffersizes;
	i = 0;
	while( buffersizes[i] != 0 )
		vBuffersizes.push_back( buffersizes[i++] );

	unsigned int n = 30;

	ITAHPT_init( );

	unsigned int j;
	unsigned int k; // Schleifenz�hler
	unsigned int b; // Puffergr��e
	double d;       // Dauer der Impulsantwort
	ITAStopWatch sw;

	vector<vector<double> > results;

	// Zufallsgenerator initialisieren
	srand( time( NULL ) );

	for( i = 0; i < vBuffersizes.size( ); i++ )
	{
		b = vBuffersizes[i];
		vector<double> v;
		for( j = 0; j < vIRDurations.size( ); j++ )
		{
			d = vIRDurations[j];

			// Anzahl der Samples der IR bestimmen
			unsigned int l = (unsigned int)ceil( d * samplerate );

			// Falter erzeugen
			ITANUPC::IConvolution* pConv = ITANUPC::IConvolution::create( NULL, samplerate, b, l );
			float* pfLeft                = fm_falloc( l, false );
			float* pfRight               = fm_falloc( l, false );

			// Speicher mit Zufallszahlen initialisieren
			for( k = 0; k < l; k++ )
				pfLeft[k] = pfRight[k] = (float)rand( ) / (float)RAND_MAX;

			// Filterkomponente erzeugen
			ITANUPC::CFilterComponent* pFC = pConv->createFilterComponent( 0, l, 0, 0, 0, 0 );

			// Mess-Vorlauf zur Einspielung des Systems
			for( k = 0; k < 3; k++ )
			{
				sw.start( );
				pFC->load( pfLeft, l, pfRight, l );
				sw.stop( );
			}

			// Eigentliche Messschleife:
			sw.reset( );
			for( k = 0; k < n; k++ )
			{
				sw.start( );
				pFC->load( pfLeft, l, pfRight, l );
				sw.stop( );
			}

			// Speicher freigeben und Falter l�schen
			fm_free( pfLeft );
			fm_free( pfRight );
			delete pConv;

			// Infos ausgeben
			printf( "\nbs = %d, dur = %0.2f s = %d samples\nmin = %0.12f s\navg = %0.12f s\nmax = %0.12f s\ncyc = %d\n\n", b, d, l, sw.minimum( ), sw.mean( ),
			        sw.maximum( ), sw.cycles( ) );

			v.push_back( sw.mean( ) );
		}
		results.push_back( v );
	}

	// Ergebnisse ausgeben
	printf( "\n\n\nLaden von FCs:\n\n" );
	printf( "IRL / BS \t" );
	for( j = 0; j < vBuffersizes.size( ); j++ )
		printf( "%6d    \t", vBuffersizes[j] );
	printf( "\n" );

	for( i = 0; i < vIRDurations.size( ); i++ )
	{
		printf( "%0.3f s  \t", vIRDurations[i] );
		for( j = 0; j < vBuffersizes.size( ); j++ )
			printf( "%0.6f s  \t", results[j][i] );
		printf( "\n" );
	}
}

void benchmarkFilterComponentSegLoad( )
{
	vector<double> vIRDurations;
	unsigned int i = 0;
	while( segload_ir_durations[i] != 0 )
		vIRDurations.push_back( segload_ir_durations[i++] );
	vector<unsigned int> vBuffersizes;
	i = 0;
	while( buffersizes[i] != 0 )
		vBuffersizes.push_back( buffersizes[i++] );

	unsigned int n = 30;

	ITAHPT_init( );

	unsigned int j;
	unsigned int k; // Schleifenz�hler
	unsigned int b; // Puffergr��e
	double d;       // Dauer der Impulsantwort
	ITAStopWatch sw;

	vector<vector<double> > results;

	// Zufallsgenerator initialisieren
	srand( time( NULL ) );

	for( i = 0; i < vBuffersizes.size( ); i++ )
	{
		b = vBuffersizes[i];
		vector<double> v;
		for( j = 0; j < vIRDurations.size( ); j++ )
		{
			d = vIRDurations[j];

			// Anzahl der Samples der IR bestimmen
			unsigned int l = (unsigned int)ceil( d * samplerate );

			// Falter erzeugen
			ITANUPC::IConvolution* pConv = ITANUPC::IConvolution::create( NULL, samplerate, b, l );
			float* pfLeft                = fm_falloc( l, false );
			float* pfRight               = fm_falloc( l, false );

			// Speicher mit Zufallszahlen initialisieren
			for( k = 0; k < l; k++ )
				pfLeft[k] = pfRight[k] = (float)rand( ) / (float)RAND_MAX;

			unsigned int L1 = pConv->getUpperSegmentOffset( first_part_length );
			unsigned int L2 = l - L1;
			printf( "\nErstes Segment:  %d Samples\n", L1 );
			printf( "Zweites Segment: %d Samples\n", L2 );
			printf( "Anpasste Laeng.: %d Samples\n\n", L1 + L2 );

			// Filterkomponente erzeugen
			ITANUPC::CFilterComponent* pFC1 = pConv->createFilterComponent( 0, L1, 0, 0, 0, 0 );
			ITANUPC::CFilterComponent* pFC2 = pConv->createFilterComponent( 0, L2, 0, 0, 0, 0 );

			// Mess-Vorlauf zur Einspielung des Systems
			for( k = 0; k < 3; k++ )
			{
				sw.start( );
				pFC1->load( pfLeft, L1, pfRight, L1 );
				pFC2->load( pfLeft, L2, pfRight, L2 );
				sw.stop( );
			}

			// Eigentliche Messschleife:
			sw.reset( );
			for( k = 0; k < n; k++ )
			{
				sw.start( );
				pFC1->load( pfLeft, L1, pfRight, L1 );
				pFC2->load( pfLeft, L2, pfRight, L2 );
				sw.stop( );
			}

			// Speicher freigeben und Falter l�schen
			fm_free( pfLeft );
			fm_free( pfRight );
			delete pConv;

			// Infos ausgeben
			printf( "\nbs = %d, dur = %0.2f s = %d samples\nmin = %0.12f s\navg = %0.12f s\nmax = %0.12f s\ncyc = %d\n\n", b, d, l, sw.minimum( ), sw.mean( ),
			        sw.maximum( ), sw.cycles( ) );

			v.push_back( sw.mean( ) );
		}
		results.push_back( v );
	}

	// Ergebnisse ausgeben
	printf( "\n\n\nLaden von FCs:\n\n" );
	printf( "IRL / BS \t" );
	for( j = 0; j < vBuffersizes.size( ); j++ )
		printf( "%6d    \t", vBuffersizes[j] );
	printf( "\n" );

	for( i = 0; i < vIRDurations.size( ); i++ )
	{
		printf( "%0.3f s  \t", vIRDurations[i] );
		for( j = 0; j < vBuffersizes.size( ); j++ )
			printf( "%0.6f s  \t", results[j][i] );
		printf( "\n" );
	}
}