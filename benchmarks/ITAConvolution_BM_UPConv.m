[ bmrd, ~ ] = inifile( 'ITAConvolution_BM_UPConv.ini', 'readall' );

%%
bm_L256_1x = str2double( bmrd{ 204, 4 } );
bm_L256_2x = str2double( bmrd{ 217, 4 } );
bm_L256_3x = str2double( bmrd{ 230, 4 } );
bm_L256_4x = str2double( bmrd{ 243, 4 } );
bm_L256_5x = str2double( bmrd{ 243, 4 } );



bm_L256 = [ bm_L256_1x bm_L256_2x bm_L256_3x bm_L256_4x bm_L256_5x ];
plot( bm_L256 )


bm_L128_M2048 =  str2double( bmrd{ 191, 4 } );
bm_L256_M2048 =  bm_L256_4x;

bm_M2048 = [ bm_L128_M2048 bm_L256_M2048 ]


%% 
[ bm_data, ~ ] = inifile( 'ITAConvolution_BM_UPConv_ScalingBehavior.ini', 'readall' );

block_time = str2double( bm_data{ 6, 4 } );

bm_L256 = [];
bm_L256( 1 ) = str2double( bm_data{ 9 + 0 * 13, 4 } );
bm_L256( 2 ) = str2double( bm_data{ 9 + 1 * 13, 4 } );
bm_L256( 3 ) = str2double( bm_data{ 9 + 2 * 13, 4 } );
bm_L256( 4 ) = str2double( bm_data{ 9 + 3 * 13, 4 } );
bm_L256( 5 ) = str2double( bm_data{ 9 + 4 * 13, 4 } );
bm_L256( 6 ) = str2double( bm_data{ 9 + 5 * 13, 4 } );
bm_L256( 7 ) = str2double( bm_data{ 9 + 6 * 13, 4 } );
bm_L256( 8 ) = str2double( bm_data{ 9 + 7 * 13, 4 } );
bm_L256( 9 ) = str2double( bm_data{ 9 + 8 * 13, 4 } );
bm_L256( 10 ) = str2double( bm_data{ 9 + 9 * 13, 4 } );
bm_L256( 11 ) = str2double( bm_data{ 9 + 10 * 13, 4 } );
bm_L256( 12 ) = str2double( bm_data{ 9 + 11 * 13, 4 } );
bm_L256( 13 ) = str2double( bm_data{ 9 + 12 * 13, 4 } );

bm_irlength_samples = 256 * [ 1, 2, 4, 8, 16, 32, 64, 128, 512, 1024, 2048, 4096, 8182  ];
bm_irlength_seconds = bm_irlength_samples ./ 44100;
bm_L256_pph = bm_L256 / block_time * 100;

figure
hold on
plot( bm_irlength_seconds, bm_L256_pph )
plot( bm_irlength_seconds, bm_L256_pph * 4 ) % 2x binaural, 2x filter exchange
